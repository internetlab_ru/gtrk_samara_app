//если тест на ПК testFlag = 1, если в сборку = 0
var testFlag = 0;

var eventClick = 'tap';
var loc = '';

//фукцния проверка наличия интернета
function checkNetwork() {
	if (testFlag == 0) {
		var networkState = navigator.connection.type;   
	    var states = {};
	    states[Connection.UNKNOWN]  = 'UNKNOWN';
	    states[Connection.ETHERNET] = 'Ethernet connection';
	    states[Connection.WIFI]     = 'WiFi connection';
	    states[Connection.CELL_2G]  = 'Cell 2G connection';
	    states[Connection.CELL_3G]  = 'Cell 3G connection';
	    states[Connection.CELL_4G]  = 'Cell 4G connection';
	    states[Connection.CELL]     = 'Cell generic connection';
	    states[Connection.NONE]     = 'NONE';
	    //alert('Connection type: ' + states[networkState]);
	    //alert(states[networkState]);
	    if (states[networkState] == 'NONE' || states[networkState] == 'UNKNOWN') {
	    	return false;
	    } else {
	    	return true;
	    }
	} else {
		return true;
	}
}



//функция показа ошибок/отсутствия интернета
//на вход тоже самое что и в sendAjax
function showError(type) {

	switch (type) {
		case 'specProject':
			$object = $('#news_list .preloader, #spec .preloader');
			break;

		case 'news':
			$object = $('#news_list .preloader');
			break;

		case 'news_full':
			$object = $('#news_inner .preloader');
			break;

		case 'feedback':
			$object = $('#feedback_list .preloader');
			break;

		case 'feedback_full':
			$object = $('#feedback_inner .preloader');
			break;
	}

	$object.addClass('error').html('<p>Нет подключения к интернету</p>');
	
}

//функция отправк AJAX запроса
//на вход:
//type - название раздела/типа запроса для получения данных
//url - путь до API
//data - данные для запроса
//parsing - функция разбора JSON
function sendAjax(type, url, data, parsing) {

	//проверяем наличие интернета
	if (checkNetwork()) {
		// alert('url=https://tvsamara.ru/api/'+url);
		$.ajax({
            url      : 'https://tvsamara.ru/api/'+url,
            type     : 'POST', 
            dataType : 'json',
            data     : data,
            success: function(res){
            	//alert('type='+type+', url='+url+', res='+JSON.stringify(res));
                parsing(res);
            },
            error: function(res){
            	//alert('error res='+JSON.stringify(res));
            	/*setTimeout(function(){
            		sendAjax(type, url, data, parsing);
            	}, 2000);*/
            },
            timeout: 30000
        });

	} else {
		showError(type);
	}

}


//функция получения спецпроекта
function getSpecProject() {
	var data = {}
	sendAjax('specProject', 'special.get', data, parseSpecProject);
}
//парсер спецпроекта
function parseSpecProject(res) {
	//получаем данные
	var title   = res.name;
	var photo   = res.img;
	var content = res.text;
	//формируем HTML
	$('#spec_project_content').html('<div id="spec_project_main_img" class="img"><img src="'+photo+'"></div><p class="title caps">'+title+'</p>'+content);
	getNews();
	
}


//страница загрузки новостей
var newsPage = 0;
//массив с HTML новостей по страницам
//top - верхний отступ, height - высота блока, content - HTML контент
var newsContent = [];
//функция получения новостей
function getNews() {

	if (newsPage < 0) {
		newsPage = 0;
	}

	var data = {
		'page'   : newsPage
	}

	sendAjax('news', 'news.getList', data, parseNews);

	newsPage++;

}

//функция очистки контента для новостей
var timerClearNews;
function clearNewsContent() {
	clearTimeout(timerClearNews);
	timerClearNews = setTimeout(function(){
		//console.log(newsContent.length);
		var scrollTop = $('#news_list').scrollTop();
		//минимальное/максимальные значения для очистки блоков из viewport
		var minViewport = scrollTop - 4000;
		var maxViewport = scrollTop + 4000;
		//console.log(newsContent.length);
		for (var i=1; i < newsContent.length; i++) {
			var $page = $('#news_page'+i);
			var curTop = newsContent[i].top;
			if (curTop > maxViewport || curTop < minViewport) {
				$('#news_page'+i).html('');
			} else {
				if (!$page.find('.news').length) {
					$page.html(newsContent[i].content);
				} 
			}

		}
	}, 0);
	
		
}

//парсер новостей
function parseNews(res) {
	var specProjectImg = $('#spec_project_main_img img').attr('src');
	var specProjectHTML = '<a href="#" class="spec-project"><p class="title">Спецпроект<i class="ico"></i></p><div class="photo"><img src="'+specProjectImg+'"></div></a>';

	var newsHTML = '';

	if (res.news != undefined && res.news != '') {
		$.each(res.news, function(key, val){
			var addClass   = '';
			var statusHTML = '';
			if (res.special[val.id] != undefined && res.special[val.id] != '') {
				addClass   = 'main';
				statusHTML = '<p class="status">Важное</p>';
			}
			/*
			if (val.status == 'favorite') {
				addClass   = 'favorite';
				statusHTML = '<p class="status">Избранное</p>';
			} else if (val.status == 'main') {
				addClass   = 'main';
				statusHTML = '<p class="status">Важное</p>';
			} */


			var photoHTML = '';
			var photo     = '';
			if (val.img != undefined && val.img != '') {
				photoHTML = '<div class="photo" style="background-image: url('+res.img[val.img].url+');">'+statusHTML+'</div>';
				photo     = res.img[val.img].url;
			}

			var video = '';
			if (res.video[val.id] != undefined && res.video[val.id] != '') {
				video = res.video[val.id];
			}

			newsHTML += '<a href="#" class="news '+addClass+'" data-view="'+val.view+'" data-video="'+video+'" data-photo="'+photo+'" data-id="'+val.id+'">'+photoHTML+'<div class="info"><p class="date">'+val.date+'</p><p class="name">'+val.name+'</p></div></a>';

			//после 1й новости добавляем ссылку на спецпроект
			// if (key == 0 && newsPage == 1) {
			// 	newsHTML += specProjectHTML;
			// }

		});

		//setTimeout(function(){
			$('#news_list_content .preloader').remove();
			$('#news_list_content').append('<div class="news-page" id="news_page'+newsPage+'" data-page="'+newsPage+'">'+newsHTML+'</div>');

			var top    = $('#news_page'+newsPage).position().top;
			var height = $('#news_page'+newsPage).height();
			$('#news_page'+newsPage).css('height', height);
			
			newsContent[newsPage] = {
				'content' : newsHTML,
				'top'     : top,
				'height'  : height
			}

		//console.log(loc);
		if(loc == 'tv' && newsPage == 1){
			route('tv');
			loc = '';
		}

		$('#news .top').show();

		//}, 500);

	} else {
		$('#news_list_content .preloader').hide('fade');
	}	
}

//подгрузка полной
function getNewsFull(id) {

	$('#news_full_content').html('<div class="preloader"></div>');
	$('#news_inner').attr('data-id', id);
	$('#news_inner').attr('data-id', id);
	var data = {
		'id' : id
	}
	sendAjax('news_full', 'news.getFull', data, parseNewsFull);
}
//парсер полной новости
function parseNewsFull(res) {
	//setTimeout(function(){
		$('#news_full_content').html(res.news.text);
	//}, 500);
}


//фукцния получения обращений граждан
var feedbackPage = 0;
//массив с HTML новостей по страницам
//top - верхний отступ, height - высота блока, content - HTML контент
var feedbackContent = [];
function getFeedback() {
	var data = {
		'page'   : feedbackPage
	}

	sendAjax('feedback', 'guest-book.getList', data, parseFeedback);

	feedbackPage++;
}

//функция очистки контента для обращений граждан
var timerClearFeedback;
function clearFeedbackContent() {
	clearTimeout(timerClearFeedback);
	timerClearFeedback = setTimeout(function(){
		//console.log(newsContent.length);
		var scrollTop = $('#feedback_list').scrollTop();
		//минимальное/максимальные значения для очистки блоков из viewport
		var minViewport = scrollTop - 4000;
		var maxViewport = scrollTop + 4000;
		//console.log(newsContent.length);
		for (var i=1; i < feedbackContent.length; i++) {
			var $page = $('#feedback_page'+i);
			var curTop = feedbackContent[i].top;
			if (curTop > maxViewport || curTop < minViewport) {
				$('#feedback_page'+i).html('');
			} else {
				if (!$page.find('.item').length) {
					$page.html(feedbackContent[i].content);
				} 
			}

		}
	}, 0);
	
		
}

//парсер обращений граждан
function parseFeedback(res) {

	var feedbackHTML = '';
	var feedbackfullHTML = '';
	if (res.questions != undefined && res.questions != '') {

		$.each(res.questions, function(key, val){
			var typeText = '';

			if (val.type == 1) {
				typeText = 'Пожелание';

			} else if (val.type == 2) {
				typeText = 'Жалоба';
			
			} else if (val.type == 3) {
				typeText = 'Вопрос';
			}

			feedbackHTML += '<a href="#" class="item" data-id="'+val.id+'"><p class="status s'+val.type+'">Вопрос</p><p class="date">'+val.date+'</p><p class="name">'+val.name+'</p></a>';

			feedbackfullHTML += '<div class="item" data-id="'+val.id+'"><p class="desc">'+val.text+'</p></div>';


		});


		setTimeout(function(){
			$('#feedback_list_content .preloader').remove();
			$('#feedback_list_content').append('<div class="feedback-page" id="feedback_page'+feedbackPage+'" data-page="'+feedbackPage+'">'+feedbackHTML+'</div>');
			$('#feedback_full_html').append(feedbackfullHTML);

			var top    = $('#feedback_page'+feedbackPage).position().top;
			var height = $('#feedback_page'+feedbackPage).height();
			$('#feedback_page'+feedbackPage).css('height', height);
			
			feedbackContent[feedbackPage] = {
				'content' : feedbackHTML,
				'top'     : top,
				'height'  : height
			}
		}, 500);

	} else {
		$('#feedback_list_content .preloader').hide('fade');
	}	
}

//подгрузка внутренней обращения граждан
function getFeedbackFull(id) {

	$('#feedback_inner').scrollTop(0);
	var fullHTML = $('#feedback_full_html .item[data-id="'+id+'"] .desc').html();
	
	$('#feedback_full_content').html('<p>'+fullHTML+'<br><br></p>');

	/*$('#feedback_full_content').html('<div class="preloader"></div>');
	$('#feedback_inner').attr('data-id', id);
	var data = {
		'id'     : id
	}

	sendAjax('feedback_full', '', data, parseFeedbackFull);*/

}
//парсер внутренней обращения граждан
function parseFeedbackFull(res) {
	setTimeout(function(){
		$('#feedback_full_content').html(res.content);
	}, 500);
}


//показ меню
function showNav() {
	$('#main_nav').addClass('open');
	$('#main_nav_pxl').addClass('show');
	$('#nav_button').addClass('close');
}
//закрытие меню
function hideNav() {
	$('#main_nav').removeClass('open');
	$('#main_nav_pxl').removeClass('show');
	$('#nav_button').removeClass('close');
}

//навигация по разделам приложения
//на вход ID страницы
function route(page) {
	$('#nav .link[data-page="'+page+'"]').addClass('active').siblings().removeClass('active');
	$('#'+page).addClass('active').siblings().removeClass('active');
	hideNav();

	//если переход в обращения граждан и до этого они не загружались - грузим
	if (page == 'feedback' && !$('#feedback_full_content .item').length) {
		getFeedback();
	}

	//выкл аудио при переходе по разделам
	$('#radio_audio').html('');
	$('#radio_list').removeClass('load');
	$('#radio_list .radio').removeClass('play load');

}

//валидация формы обращения граждан!
function validateFeedback() {
	var error = 0;

	if ( !validateForm('required', 'feedback_fio') ) {
		error++;
	}

	if ( !validateForm('required', 'feedback_address') ) {
		error++;
	}

	if ( !validateForm('required', 'feedback_phone') ) {
		error++;
	}

	if ( !validateForm('email', 'feedback_email') ) {
		error++;
	}

	if ( !validateForm('required_select', 'feedback_type') ) {
		error++;
	}

	if ( !validateForm('required', 'feedback_message') ) {
		error++;
	}

	if ( !validateForm('checkbox_accept', 'feedback_accept_checkbox') ) {
		error++;
	}
	
	//если нет ошибок снимаем disable с кнопки
	if (error == 0) {
		$('#feedback_submit').removeClass('disable');
	} else {
		$('#feedback_submit').addClass('disable');
	}

}


//фукнция установки высоты для ТВ/радио каналов
function setTvRadioHeight() {
	var height = ($('#pages').height() - $('#tv .top').height() - 5) / 3;

	$('#tv_channels .channel, #radio_list .radio').css('height', height);

	var wVideo = $(window).width();
	var hVideo = wVideo * 0.9;
	$('.video-modal video').attr({
		'width' : wVideo,
		'height' : hVideo
	});
}


//показ модального окна
//на вход html код контента
function showModal(content) {
	$('#modal_content').html(content);
	$('#modal_wrap').addClass('show');
}

//закрытие модального окна
function closeModal() {
	$('#modal_wrap').removeClass('show white');
	$('#modal_content').html('');
}


//инициализация JS логики
function initApps() {
	
	//при загрузке приложения грузим сразу спецпроект
	getSpecProject();
	
	//парсим url, если есть hash=tv, скрываем title!
	loc = window.location.hash.replace('#','');
	if (loc == 'tv') {
		$('#news .top').hide();
	}

	var newsList = localStorage.getItem('news');
	//console.log(newsList);
	

	//показ/скрытие меню
	$('#nav_button').on(eventClick, function() {

		if ( $('#main_nav').hasClass('open') ) {
			hideNav();

		} else {
			showNav();
		}

		return false;
	});

	//закрытие меню по пикселю
	$('#main_nav .pxl, #main_nav_pxl').on(eventClick, function(){
		hideNav();

		return false;
	});

	//вытягиваемое меню
	var timeoutNav;
	$('#nav_swipe').swipe( {
        swipeStatus:function(event, phase, direction, distance, duration, fingerCount) {
        	if (timeoutNav != undefined) {
        		clearTimeout(timeoutNav);	
        	}
        	
			if (direction == 'right' && !$('#main_nav').hasClass('open')) {
	  			var tx = distance;
		  		var width = $(window).width();
		  		var pc = -100 + ((tx / width) * 100);

		  		if (pc > 0) {
		  			pc = 0;
		  		}
		  		$('#main_nav').addClass('swipe');
		  		var opacity = (100 - Math.abs(pc)) / 100;
		  		$('#main_nav_pxl').css('opacity', opacity).show();
		  		//console.log(phase);
		  		if (phase == 'move') {
		  			$('#main_nav').css({
		  				'transform': 'translateX('+pc+'%)'
		  			});
		  			/*if (pc >= -90) {
						showNav();
					} else {
						hideNav();
					}*/
		  		} else {

		  			$('#main_nav, #main_nav_pxl').removeClass('swipe').removeAttr('style');
					if (pc >= -90) {
						showNav();
					} else {
						hideNav();
					}
		  			//$('#main_nav').css('transform', 'translateX(0%)');
		  		}

		  		//console.log(direction+' | '+ tx +' |'+phase);

			} else if (direction == 'left' && $('#main_nav').hasClass('open')) {
				var tx = distance;
		  		var width = $(window).width() / 2;
		  		var pc = -((tx / width) * 100);

		  		if (pc < -100) {
		  			pc = -100;
		  		}
		  		$('#main_nav').addClass('swipe');
		  		var opacity = (100 - Math.abs(pc)) / 100;
		  		$('#main_nav_pxl').css('opacity', opacity).show();
		  		$('#main_nav').addClass('swipe');

		  		//console.log(phase);
		  		if (phase == 'move') {
		  			$('#main_nav').css({
		  				'transform': 'translateX('+pc+'%)'
		  			});
		  		} else {
		  			$('#main_nav, #main_nav_pxl').removeClass('swipe').removeAttr('style');
					if (pc >= -10) {
						showNav();
					} else {
						hideNav();
					}
		  			//$('#main_nav').css('transform', 'translateX(0%)');
		  		}

		  		console.log(direction+' | '+ pc+' |'+phase);

			}

			var tx = distance;
		  	var width = $(window).width();
		  	var pc = Math.round((tx / width) * 100);
			timeoutNav = setTimeout(function(){
				console.log(pc);
				if ($('#main_nav').hasClass('swipe')) {
					$('#main_nav, #main_nav_pxl').removeClass('swipe').removeAttr('style');
					if (pc < 50) {
						hideNav();
					} else {
						showNav();
					}
				}
				
			}, 500);

			
		  
		},
        
        threshold:0
    });

	//клик по ссылке в основном меню
	$('#nav a').on(eventClick, function(){
		if ( !$(this).hasClass('active') ) {
			route($(this).data('page'));
		} else {
			hideNav();
		}

		return false;
	});

	//переходы к просмотру новости и спецпроекту 
	$('#news_list_content')
		.on(eventClick, '.news', function(){
			//к новости
			var id    = $(this).data('id');
			var photo = $(this).data('photo');
			var date  = $(this).find('.date').text();
			var title  = $(this).find('.name').text();
			var video = $(this).data('video');
			var view  = $(this).data('view');

			//формируем HTML внутренней новости
			var videoHTML = '';
			if (video != '') {
				videoHTML = '<a target="_blank" href="'+video+'" class="video-play"></a>';
			}
			$('#news_inner_photo').css('backgroundImage', 'url('+photo+')').html(videoHTML);
			$('#news_inner_date').html(date);
			$('#news_inner_view').html(view+'<i class="ico"></i>');
			$('#news_inner_title').html(title);

			$('#news').addClass('full');
			//запрос на получение контента новости
			getNewsFull(id);

			return false;
		})
		.on(eventClick, '.spec-project', function(){
			//к спецпроекту
			route('spec');
			return false;
		});


	//запуск видео прикрепленного к новости
	$('#news_inner').on(eventClick, '.video-play', function(){
		//запуск видео прикрепленного к новости
		var url = $(this).attr('href');
		/*var w = $('#body').width();
		var h = $('#body').height();
		videoplayer.show(url, {
		  rect:[0, 0, w, h],
		  callback: function() {
		    //alert('finish!');
		  },
		});*/
		if (device.platform == 'iOS') {

		} else {
			VideoPlayer.play(url);
			//alert('запуск видео');
			return false;
		}
		
	});

	//возврат к просмотру новостей
	$('#news_back_button').on(eventClick, function(){
		$('#news').removeClass('full');

		return false;
	});

	//подгрузка новостей при скролле
	var timerNews;
	$('#news_list')
		.on('scroll', function(){
			if (!$('#news_list_content .preloader').length) {
				//высота обертки
				var heightWrap = $('#news_list').height();
				//знач.скролла
				var scrollTop  = $('#news_list').scrollTop();
				//высота контента новостей
				var heightNews = $('#news_list_content').height();
				//значение при достижении которого будет подгрузка
				var heightLoad = heightNews - heightWrap - 100;

				if (scrollTop >= heightLoad) {
					$('#news_list_content').append('<div class="preloader"></div>');
					getNews();
				} else {

					clearNewsContent();

				}

			} else {
				clearNewsContent();
			}

		});


	//обновление новостей
	$('#news_refresh').on(eventClick, function(){
		newsContent = [];
		newsPage = 0;
		$('#news_list_content').html('<div class="preloader"></div>');
		getNews();
		return false;
	});

	
	//устанавливаем высоту для каналов
	setTvRadioHeight();
	
	
	//videojs.registerComponent('FullscreenToggle', videojs.extends(videojs.getComponent('Button')));
	//tv1.FullscreenToggle();
    
	//запуск/пауза видео ТВ канала
	/*$('#tv_channels').on(eventClick, '.channel', function(){
		//alert('click');
		var channel = $(this).data('channel');
		$('#'+channel+'_modal').show();
		
		return false;
	});*/

	/*
	$('.close-tv-modal').on(eventClick, function(){
		$(this).parent().hide();

		//$('#tv1, #tv2, #tv3').pause();
		return false;
	});*/

	//запуск/пауза аудио радио
	//для iOS
	/*if (device.platform == 'iOS') {
		function playAudio(url) {
		    // Play the audio file at url
		    var my_media = new Media(url,
		        // success callback
		        function () { alert('success'); },
		        // error callback
		        function (err) { alert('error: ' + err); }
		    );

		    // Play audio
		    my_media.play();

		    // Pause after 10 seconds
		    setTimeout(function () {
		        my_media.pause();
		    }, 10000);
		}

	} else {*/

		$('#radio_list').on(eventClick, '.radio', function(){
			if (!$('#radio_list').hasClass('load')) {
				
				/*var $this = $(this);
				var num = $this.data('id');
				if ($this.hasClass('play')) {
					$this.removeClass('play');
					//$('#radio_audio').html('');
					$('#radio'+num).trigger('pause');

				} else {
					$('#radio_list').addClass('load');
					$('#radio_audio audio').trigger('pause');
					$('#radio'+num).trigger('play').on('loadeddata', function(){
						$('#radio_list').removeClass('load');
						
					});
					
					$this.addClass('play').siblings().removeClass('play');
				}*/
				var $this = $(this);
				if ($this.hasClass('play')) {
					$this.removeClass('play');
					$('#radio_audio').html('');
				} else {
					$('#radio_list').addClass('load');
					var radio = $this.data('radio');
					$('#radio_audio').html('<audio src="'+radio+'" autoplay></audio>');
					$('#radio_audio audio').on('loadeddata', function(){
						$('#radio_list').removeClass('load');
						
					});
					$this.addClass('play').siblings().removeClass('play');
				}
				
				//alert('click audio');
				/*var $this = $(this);
				if ($this.hasClass('play')) {
					$this.removeClass('play');
					window.plugins.streamingMedia.stopAudio();
				} else {
					$('#radio_list').addClass('load');
					var audioUrl = $this.data('radio');
					var audioImg = $this.data('img');
					$this.addClass('play').siblings().removeClass('play');
					//alert('play audio '+audioUrl);
					// Play an audio file with options (all options optional) 
					var options = {
					    bgColor: '#FFFFFF',
					    bgImage: audioImg,
					    bgImageScale: 'fit', // other valid values: "stretch" 
					    initFullscreen: false, // true(default)/false iOS only 
					    successCallback: function() {
					    	alert("Audio player closed without error.");
					    	$('#radio_list').removeClass('load').find('.play').removeClass('play');
					    },
					    errorCallback: function(errMsg) {
					    	alert("Error! " + errMsg);
					    	$('#radio_list').removeClass('load').find('.play').removeClass('play');

					    }
					};
					window.plugins.streamingMedia.playAudio(audioUrl, options);
				}*/

				//window.open($this.data('radio'), '_system');

				


			}
			
			return false;
		});
	/*}*/
	

	
	//переход к просмотру обращений граждан!
	$('#feedback_list').on(eventClick, '.item', function(){

		var id   = $(this).data('id');
		var date = $(this).find('.date').text();
		var name = $(this).find('.name').text();

		//формируем HTML внутренней обращения граждан
		$('#feedback_inner_name').html(name);
		$('#feedback_inner_date').html(date);

		//запрос на получение контента обращения
		getFeedbackFull(id);
		$('#feedback').addClass('full');

		return false;
	});

	//подгрузка обращений при скролле
	$('#feedback_list').on('scroll', function(){
		if (!$('#feedback_list_content .preloader').length) {
			//высота обертки
			var heightWrap = $('#feedback_list').height();
			//знач.скролла
			var scrollTop  = $('#feedback_list').scrollTop();
			//высота контента обращения граждан
			var heightFeedback = $('#feedback_list_content').height();
			//значение при достижении которого будет подгрузка
			var heightLoad = heightFeedback - heightWrap - 100;

			if (scrollTop >= heightLoad) {
				$('#feedback_list_content').append('<div class="preloader"></div>');
				getFeedback();
			} else {
				clearFeedbackContent();
			}
		} else {
			clearFeedbackContent();
		}
	});


	//возврат к просмотру обращений граждан
	$('#feedback_back_button').on(eventClick, function(){
		
		if ($('#feedback').hasClass('form')) {
			//возврат с формы
			$('#feedback').removeClass('form');
		} else {
			//возврат с просмотра обращения
			$('#feedback').removeClass('full');
		}
		

		return false;
	});

	//переход к форме Написать обращение
	$('#feedback_form_show').on(eventClick, function(){
		$('#feedback_form').show();
		$('#feedback_success').hide();
		$('#feedback').addClass('form');
		return false;
	});
	//стилиция селектов
	$('#feedback_type').styler({
		selectSmartPositioning: false,
		selectSearch: false
	});
	//textarea autoresize
	$('#feedback_message').autoResize({
	    extraSpace : 0
	});

	$('#feedback_fio')
		.on('keyup', function(){
			validateFeedback();
		})
		.on('focus', function(){
			$(this).removeClass('error');
		})
		.on('change', function(){
			if (!validateForm('required', 'feedback_fio')) {
				$(this).addClass('error');
			} 
		});

	$('#feedback_address')
		.on('keyup', function(){
			validateFeedback();
		})
		.on('focus', function(){
			$(this).removeClass('error');
		})
		.on('change', function(){
			if (!validateForm('required', 'feedback_address')) {
				$(this).addClass('error');
			}
		});

	$('#feedback_phone')
		.on('keyup', function(){
			validateFeedback();
		})
		.on('focus', function(){
			$(this).removeClass('error');
		})
		.on('change', function(){
			if (!validateForm('required', 'feedback_phone')) {
				$(this).addClass('error');
			}
		});

	$('#feedback_email')
		.on('keyup', function(){
			validateFeedback();
		})
		.on('change', function(){
			if (!validateForm('email', 'feedback_email')) {
				$(this).addClass('error');
			} else {
				$(this).removeClass('error');
			}
		});

	$('#feedback_message')
		.on('keyup', function(){
			validateFeedback();
		})
		.on('focus', function(){
			$(this).removeClass('error');
		})
		.on('change', function(){
			if (!validateForm('required', 'feedback_message')) {
				$(this).addClass('error');
			}
		});

	$('#feedback_type').on('change', function(){
		validateFeedback();
	});

	$('#feedback_accept')
		.on('change', function(){
			validateFeedback();

			if ($(this).prop('checked')) {
				$('#feedback_accept_checkbox').removeClass('error');
			} else {
				$('#feedback_accept_checkbox').addClass('error');
			}
		});



	//отправка формы обращения граждан
	$('#feedback_form').on('submit', function(){
		$('#feedback_submit').trigger(eventClick);
		return false;
	});
	$('#feedback_submit').on(eventClick, function(){
		//console.log(validateFeedback()+' ||| '+!$(this).hasClass('disable'));
		if ( !$(this).hasClass('disable') ) {
			//собираем данные для отправки
			var fio     = $('#feedback_fio').val();
			var address = $('#feedback_address').val();
			var phone   = $('#feedback_phone').val();
			var email   = $('#feedback_email').val();
			var type    = $('#feedback_type').val();
			var message = $('#feedback_message').val();
			$('#feedback_form').hide();
			$('#feedback_success').show('fade');
			//alert('Отправка данных с формы');
			var data = {
				'feedback_fio'     : fio,
				'feedback_address' : address,
				'feedback_phone'   : phone,
				'feedback_email'   : email,
				'feedback_type'    : type,
				'feedback_message' : message
			};
			$.ajax({
	            url      : 'http://tvsamara.ru/api/guest-book.add', 
	            type     : 'POST', 
	            dataType : 'json',
	            data     : data,
	            success: function(res){
	                //parsing(res);
	            },
	            error: function(res){
	            	/*setTimeout(function(){
	            		sendAjax(type, url, data, parsing);
	            	}, 2000);*/
	            },
	            timeout: 30000
	        });

	        //очищаем текст сообщения!
	        $('#feedback_message').val('');

		}
		
		return false;
	});



	//повторные запросы при отсутствии интернета
	//загрузка спецпроекта
	$('#spec').on(eventClick, '.preloader', function(){
		if ($(this).hasClass('error')) {
			$(this).removeClass('error').html('');
			setTimeout(function(){
				getSpecProject();
			}, 500);
			
		}
		return false;
	});

	//список новостей
	$('#news_list').on(eventClick, '.preloader', function(){
		if ($(this).hasClass('error')) {
			$(this).removeClass('error').html('');
			//если спецпроект не загружен - сначала грузим его!
			if ($('#spec .preloader.error').length) {
				$('#spec .preloader').trigger(eventClick);
			} else {
				newsPage--;
				
				setTimeout(function(){
					getNews();
				}, 500);
			}
			
			
		}
		return false;
	});

	//просмотр новости
	$('#news_inner').on(eventClick, '.preloader', function(){
		if ($(this).hasClass('error')) {
			$(this).removeClass('error').html('');
			setTimeout(function(){
				var id = $('#news_inner').data('id');
				getNewsFull(id);
			}, 500);
			
		}
		return false;
	});

	//список обращений
	$('#feedback_list').on(eventClick, '.preloader', function(){
		if ($(this).hasClass('error')) {
			feedbackPage--;
			$(this).removeClass('error').html('');
			setTimeout(function(){
				getFeedback();
			}, 500);
			
		}
		return false;
	});

	//проосмотр обращения
	$('#feedback_inner').on(eventClick, '.preloader', function(){
		if ($(this).hasClass('error')) {
			$(this).removeClass('error').html('');
			setTimeout(function(){
				var id = $('#feedback_inner').data('id');
				getFeedbackFull(id);
			}, 500);
			
		}
		return false;
	});




	$(window).on('resize', function(){

		//устанавливаем высоту для ТВ/РАДИО
		setTvRadioHeight();

	});

	//системная кнопка назад
	document.addEventListener('backbutton', backKeyDown, true); 

	function backKeyDown() { 

		if ($('#main_nav').hasClass('open')) {
			hideNav();

		} else {
			if ($('#news').hasClass('active') && $('#news').hasClass('full')) {
				//для акций
				$('#news').removeClass('full');

			} else if ($('#feedback').hasClass('active') && ($('#feedback').hasClass('full') || $('#feedback').hasClass('form')) ) {
				//для обращений граждан
				$('#feedback').removeClass('full form');

			} else if ($('#news').hasClass('active')) {
				showModal('<p class="title">Подтвердите действие</p><p>Вы действительно хотите выйти из приложения?</p><div class="buttons-wrap"><a id="close_modal" class="button w50 back" href="#">Отмена</a><a id="exit_app" class="button w50" href="#">Выйти</a><div class="clr"></div></div>');

			} else {
				//иначе на главную страницу приложения
				route('news');
			}
		}
		
		return false;
	}


	$('#modal_wrap')
		.on(eventClick, '#close_modal', function(){
			//закрытие мод.окна
			closeModal();
			return false;
		})
		.on(eventClick, '#exit_app', function(){
			//закрытие приложения
			navigator.app.exitApp();
			return false;
		});

}






document.addEventListener('deviceready', function(){
	//alert('deviceready');
	StatusBar.hide();

	initApps(); 
	
	//screen.lockOrientation('portrait');

	//alert(screen.orientation);

	/*
	window.screen.lockOrientationUniversal = window.screen.lockOrientation || window.screen.mozLockOrientation || window.screen.msLockOrientation;

	window.screen.lockOrientationUniversal('portrait-primary');*/
	
}, false);

