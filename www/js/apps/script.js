/*******************************
*******ИНИЦИАЛИЗАЦИЯ*******
*******************************/
document.addEventListener('deviceready', onDeviceReady, false);

document.addEventListener('deviceready', function(){

	// Инициализация 
	var fs = '';
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem){
		//alert("1");
		fs = fileSystem;		
		fileSystem.root.getDirectory("rfplfiles-v3", {create: true}, null, null);
		//alert("1");
		getDataJSON('clubs.json', 'clubs.php', {"action":"getClubs"}, getClubsLoad, "");
		//загрузка слайдов медиа для календаря
		setTimeout(function(){
			mediaSliderControl();
		}, 250);
			
	}, null);
	
	function loadNetwork(callbacksuccess) {
		//alert("1");
		if ($("#calendar").hasClass("current")) {
			$("#refresh-repeat").data("action", "calendar");
			$("#noconnect").show();

			$("#calendar-wrapper, #media-index, #calendar .banner-ea").removeClass("load");
			//alert("cal");
			//$("#title-page").append($mediaSlider.slides.length);

		} else if ($("#tournament-table").hasClass("current")) {
			$("#refresh-repeat").data("action", "tournament-table");
			$("#noconnect").show();
			$("#tournament-table").removeClass("load");
			//("table");

		} else if ($("#leaders").hasClass("current")) {
			$("#refresh-repeat").data("action", "leaders");
			$("#noconnect").show();
			$("#players-forward").removeClass("load");
			$("#players-foul").removeClass("load");
					
		} else if ($("#news-inner").hasClass("current")) {

			$("#refresh-repeat").data("action", "news-inner");
			$("#noconnect").show();
			$("#news-inner").removeClass("load");
			var idNews = $("#news-inner").data("id");
			//alert(idNews);
			fullNewsLoad(idNews);
		/*} else if ($("#news").hasClass("current")) {

			//$("#noconnect").hide();
				fileEntry.createWriter(function(writer){
					$.post("http://rfpl.org/api/v3/"+url, post, function(res){
						writer.write(JSON.stringify(res));
						callbacksuccess(res);
					}, "json");
				}, null);

				} else if ($("#video").hasClass("current")) {

					$("#refresh-repeat").data("action", "video");
					$("#noconnect").show(); 

				} else if ($("#photo").hasClass("current")) {

					$("#refresh-repeat").data("action", "photo");
					$("#noconnect").show(); 
				*/
		} else {
			//alert("!11");
			
			callbacksuccess("noconnect");
			
			//alert("111");
			
		}
	}

	//фукнция получение данных
	function getDataJSON(file, url, post, callbacksuccess, flag){
		//alert(checkConnection());

		fs.root.getFile("rfplfiles-v3/"+file, {create: true, exclusive: false}, function(fileEntry){
			//alert("1");
			fileEntry.file(function(file){
				//alert("1");
				var reader = new FileReader();
				reader.onloadend = function(evt){
					if(
						(evt.target.result != '' && evt.target.result != null &&  flag != 1 && checkConnection() == false) || 
						(evt.target.result != '' && evt.target.result != null  && flag != 1 && (post.action == "getNewsIndex" || post.action == "getVideoIndex" || post.action == "getPhotoIndex"))
					){
						//$("#title-page").html('файл');				
						if(device.platform != 'Win32NT'){
							callbacksuccess(JSON.parse(evt.target.result));
						}else{
							callbacksuccess(evt.target.result);
						};
					}else{
						//$("#title-page").html('запрос');
						if (checkConnection() == false) {
							setTimeout(function(){
								loadNetwork(callbacksuccess);
							}, 500);
						} else {
							
							//$("#noconnect").hide();
							fileEntry.createWriter(function(writer){
								/*
								$.post("http://rfpl.org/api/v3/"+url, post, function(res){
									writer.write(JSON.stringify(res));
									callbacksuccess(res);
								}, "json");*/
								$.ajax({
								  	type: "POST",
								  	url: "http://rfpl.org/api/v3/"+url,
								  	data: post,
								  	dataType: "json",
								  	timeout: 10000000,
								  	success: function(res){
								    	writer.write(JSON.stringify(res));
										callbacksuccess(res);
								  	},
								  	error: function(res){
								  		loadNetwork(callbacksuccess);
								  	}
								});
							}, null);
						}
					};
				};
				reader.readAsText(file);
			}, null);
		}, null);
	};

	$("#wrapper").on("click", "#refresh-repeat, .refresh-repeat", function(){

		var action = $(this).data("action");
		//$("#title-page").html(action);
		switch (action){

			case "calendar":
				$("#noconnect").hide();
				$("#calendar-wrapper, #media-index, #calendar .banner-ea").addClass("load");
				$("#supercup-match").addClass("load");
				//setTimeout(function(){
					//обновление клубов и календаря
					getDataJSON('calendar.json', 'matches.php', {"action":"getMatches"}, getCalendarRefresh, 1);
					//загрузка слайдов медиа для календаря
					mediaSliderControl("refresh");
				//}, 2000);
			break;

			case "tournament-table":
				$("#noconnect").hide();
				//setTimeout(function(){
					$("#tournament-table").addClass("load");
					getDataJSON('tournament-table.json', 'turnir.php', {"action":"getTurnirTable"}, parseTournamentTable, 1);
				//}, 2000);
			break;

			case "leaders":
				$("#noconnect").hide();
				//setTimeout(function(){
					$("#players-forward").addClass("load");
					getDataJSON('players-forward.json', 'players.php', {"action":"getPlayersForward"}, parsePlayersForward, 1);

					$("#players-foul").addClass("load");
					getDataJSON('players-foul.json', 'players.php', {"action":"getPlayersFoul"}, parsePlayersFoul, 1);
				//}, 2000);
			break;

			case "news": 
				$("#noconnect").hide();
				//setTimeout(function(){
					getNews("");
				//}, 2000);
			break;

			case "news-inner": 
				$("#noconnect").hide();
				//setTimeout(function(){
					getNews("");
				//}, 2000);
			break;

			case "newsscroll":

				$("#news-list .noconnect-small").remove();
				//обнуляем флаг загрузки
				newsLoadFlag = 0;
				//setTimeout(function(){
				getNews(newsPageLoad);
				//}, 2000);
				
			break;

			case "video":
				$("#noconnect").hide();
				//получаем тип видео
				var type = $("#tabs-video-wrapper a.active").attr("href").replace("#", "")*1;
				//alert(type);
				//в зависимости от типа вкладки получаем блок с контентом видео
				if (type == 1) {
					var $videoLoadBlock = $("#video-review").parent();
					videoReviewPageLoad = 1;
				} else if (type == 2) {
					var $videoLoadBlock = $("#video-moments").parent();
					videoMomentsPageLoad = 1;
				} else if (type == 3) {
					var $videoLoadBlock = $("#video-story").parent();
					videoStoryPageLoad = 1;
				} else {
					var $videoLoadBlock = $("#video-best").parent();
					videoBestPageLoad = 1; 
				}
				

				$videoLoadBlock.addClass("load");
				//setTimeout(function(){
					loadTabVideo(type, "");
				//}, 1000);
				

			break;

			case "videoscroll":

				//получаем активную вкладку
				var activeVideoTab = $("#tabs-video-wrapper a.active").attr("href").replace("#","")*1;
				//alert(activeVideoTab);
				//получаем блок в который будем добавлять прелоадер и видео
				if (activeVideoTab == 1) {
					var $videoLoadBlock = $("#video-review").parent();
					var videoPageLoad = videoBestPageLoad;
					var type = 1;
				}
				if (activeVideoTab == 2) {
					var $videoLoadBlock = $("#video-moments").parent();
					var videoPageLoad = videoReviewPageLoad;
					var type = 2;
				}
				if (activeVideoTab == 3) {
					var $videoLoadBlock = $("#video-story").parent();
					var videoPageLoad = videoMomentsPageLoad;
					var type = 3;
				}
				if (activeVideoTab == 0) {
					var $videoLoadBlock = $("#video-best").parent();
					var videoPageLoad = videoStoryPageLoad;
					var type = 0;
				}

				//$("#title-page").append("tab="+activeVideoTab);
				$videoLoadBlock.find(".noconnect-small").remove();
				videoLoadFlag = 1;
				//setTimeout(function(){
				loadTabVideo(type, videoPageLoad);	
				//}, 2000);
				

			break;

			case "photo":
				$("#noconnect").hide();
				//setTimeout(function(){
					getPhoto("");
				//}, 2000);
				
			break;

			case "photoscroll":

				$("#photo .noconnect-small").remove();
				//обнуляем флаг загрузки
				photoalbumLoadFlag = 0;
				//setTimeout(function(){
				getPhoto(photoalbumPageLoad);
				//}, 2000);
				
			break;
			case "photoinner": 
				var lastID = $("#photo-inner").data("id");
				photoLoad(lastID, "");
			break;
			case "photoinnerscroll":
				var lastID = $("#photo-inner").data("id");
				$("#photo-list .noconnect-small").remove();
				//обнуляем флаг загрузки
				photoalbumPhotoLoadFlag = 0;
				//setTimeout(function(){
				photoLoad(lastID, photoalbumPhotoPageLoad);
				//}, 2000);
				
			break;

			case "reportsquadsmatch":
				$("#noconnect").hide();
				var idMatch = $("#report-match").data("id");
				$("#tabs-report-wrapper .eventstabreport, #tabs-report-wrapper .squadstabreport").addClass("full");
				getDataJSON('temp.json', 'matches.php', {"action":"getMatchesInfo", "matches": idMatch}, getReportSquadsMatch, 1);
			break;

			case "reportvideomatch":
				$("#noconnect").hide();
				var idMatch = $("#report-match").data("id");
				$("#tabs-report-wrapper .videotabreport").addClass("full");
				getDataJSON('temp.json', 'video.php', {"action":"getVideoMatch", "matches": idMatch}, getVideoMatch, 1);
			break;

			case "reportphotomatch":
				$("#noconnect").hide();
				var idMatch = $("#report-match").data("id");
				$("#tabs-report-wrapper .phototabreport").addClass("full");
				getDataJSON('temp.json', 'photo.php', {"action":"getPhotoMatch", "matches": idMatch}, getPhotoMatch, 1);
			break;
			
			case "clubcalendar":
				var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
				getClubCalendar(idClub);
			break;

			case "clubnews": 
				
				var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
				//alert("news"+idClub);
				$("#noconnect").hide();
				getClubNews(idClub, "");	
			break;

			case "clubnewsscroll":
				var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
				$("#club-news .noconnect-small").remove();
				//обнуляем флаг загрузки
				clubNewsPageLoadFlag = 0;
				getClubNews(idClub, clubNewsPageLoad);	
			break;

			case "clubvideo": 
				
				var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
				//alert("video"+idClub);
				$("#noconnect").hide();
				getClubVideo(idClub, "");	
			break;

			case "clubvideoscroll":
				var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
				$("#club-video .noconnect-small").remove();
				//обнуляем флаг загрузки
				clubVideoPageLoadFlag = 0;
				getClubVideo(idClub, clubVideoPageLoad);	
			break;

			case "clubphoto": 
				
				var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
				//alert("photo"+idClub);
				$("#noconnect").hide();
				getClubPhoto(idClub, "");	
			break;

			case "clubphotoscroll":
				var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
				$("#club-photoalbum .noconnect-small").remove();
				//обнуляем флаг загрузки
				clubPhotoPageLoadFlag = 0;
				getClubPhoto(idClub, clubPhotoPageLoad);	
			break;

			case "clubplayers": 
				var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
				//alert("photo"+idClub);
				$("#noconnect").hide();
				getClubPlayers(idClub);	
			break;

			case "clubstaff": 				
				var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
				//alert("photo"+idClub);
				$("#noconnect").hide();
				getClubStaff(idClub);	
			break;
			
			case "clubawards": 
				var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
				//alert("photo"+idClub);
				$("#noconnect").hide();
				getClubAwards(idClub);	
			break;
			

			case "playersinfo": 
				$("#noconnect").hide();
				//setTimeout(function(){
				//ID игрока
				var id = $("#players-inner").data("id");
				getPlayersInfo(id);
				//}, 2000);
			break;
			
			case "newsplayersscroll":

				$("#news-list-players .noconnect-small").remove();
				//обнуляем флаг загрузки
				playersNewsAllLoadFlag = 0;
				//setTimeout(function(){
				getPlayersNewsAll(playersNewsAllPageLoad);
				//}, 2000);
				
			break;

			case "videoplayersscroll":

				$("#video-list-players .noconnect-small").remove();
				//обнуляем флаг загрузки
				playersVideoAllLoadFlag = 0;
				//setTimeout(function(){
				getPlayersVideoAll(playersVideoAllPageLoad);
				//}, 2000);
				
			break;

			case "photoplayersscroll":

				$("#photo-list-players .noconnect-small").remove();
				//обнуляем флаг загрузки
				playersPhotoAllLoadFlag = 0;
				//setTimeout(function(){
				getPlayersPhotoAll(playersPhotoAllPageLoad);
				//}, 2000);
				
			break;

			/*лига ставок повторная загрузка!*/
			case "lsline":
				$("#noconnect").hide();
				getLSline();
			break;

			case "lsnews": 
				$("#noconnect").hide();
				getLSNews("");	
			break;

			case "lsnewsscroll":
				$("#ls-news .noconnect-small").remove();
				//обнуляем флаг загрузки
				lsNewsPageLoadFlag = 0;
				getLSNews(lsNewsPageLoad);	
			break;

			case "lsvideo": 
				$("#noconnect").hide();
				getLSVideo("");	
			break;

			case "lsvideoscroll":
				$("#ls-video .noconnect-small").remove();
				//обнуляем флаг загрузки
				lsVideoPageLoadFlag = 0;
				lsClubVideo(lsVideoPageLoad);	
				
			break;

			case "lsofficial":
				$("#noconnect").hide();
				getLSofficial();
			break;

		};
		
		return false;
	});

	//ОБНОВЛЕНИЕ СТРАНИЦ
	$("#refresh").on("tap", function(){

		if (!$(this).hasClass("disable")) {
			
			//календарь!
			if ( $("#calendar").hasClass("current") ) {
				$("#noconnect").hide();
				//если загрузки в данный момент нет - подгружаем данные
				if( !$("#calendar-wrapper").hasClass("load") && !$("#media-index").hasClass("load") ) {
					$("#calendar-wrapper, #media-index, #calendar .banner-ea").addClass("load");

					$("#supercup-match").addClass("load").html("");
					//обновление клубов и календаря
					//getDataJSON('clubs.json', 'clubs.php', {"action":"getClubs"}, getClubsRefresh, 1);
					getDataJSON('calendar.json', 'matches.php', {"action":"getMatches"}, getCalendarRefresh, 1);
					//загрузка слайдов медиа для календаря
					mediaSliderControl("refresh");
				}
			}

			//турнирная таблица
			if ( $("#tournament-table").hasClass("current") ) {
				$("#noconnect").hide();
				//если загрузки в данный момент нет - подгружаем данные
				if ( !$("#tournament-table").hasClass("load") ) {
					$("#tournament-table").addClass("load");
					getDataJSON('tournament-table.json', 'turnir.php', {"action":"getTurnirTable"}, parseTournamentTable, 1);
				}	
			}

			//лидеры!
			if ( $("#leaders").hasClass("current") ) {
				$("#noconnect").hide();
				if (!$("#players-forward").hasClass("load") && !$("#players-forward").hasClass("load")) {
					
					$("#players-forward").addClass("load");
					getDataJSON('players-forward.json', 'players.php', {"action":"getPlayersForward"}, parsePlayersForward, 1);

					$("#players-foul").addClass("load");
					getDataJSON('players-foul.json', 'players.php', {"action":"getPlayersFoul"}, parsePlayersFoul, 1);

				}
			}
				
			//новости
			if ( $("#news").hasClass("current") ) {
				$("#noconnect").hide();
				if (!$("#news").hasClass("load")) {
					
					getNews("");

				}	
			}
			//видео
			if ( $("#video").hasClass("current") ) {
					
				//получаем тип видео
				var type = $("#tabs-video-wrapper a.active").attr("href").replace("#", "")*1;
				//alert(type);
				//в зависимости от типа вкладки получаем блок с контентом видео
				if (type == 1) {
					var $videoLoadBlock = $("#video-review").parent();
				} else if (type == 2) {
					var $videoLoadBlock = $("#video-moments").parent();
				} else if (type == 3) {
					var $videoLoadBlock = $("#video-story").parent();
				} else {
					var $videoLoadBlock = $("#video-best").parent();
				}
					
				if ( !$videoLoadBlock.hasClass("load") || $("#noconnect:visible").length ) {
					$("#noconnect").hide();
					//в зависимости от типа вкладки обнуляем счетчик
					if (type == 1) {
						videoReviewPageLoad = 1;
					} else if (type == 2) {
						videoMomentsPageLoad = 1;
					} else if (type == 3) {
						videoStoryPageLoad = 1;
					} else {
						videoBestPageLoad = 1; 
					}
					//alert(type);
					$videoLoadBlock.addClass("load");

					setTimeout(function(){
						loadTabVideo(type, "");
					}, 1000);
					//loadTabVideo(type, "");

				}
		
			}

			//фото
			if ( $("#photo").hasClass("current") ) {
				if (!$("#photo").hasClass("load")) {
					getPhoto("");
				}
					
			}
		}
		
		return false;
	});


	//пустая фукцния заглушка для callback получения данных
	function callbackEmpty(res) {

	}

	var calendarJSON;
	//функции первичной инициализации клубов и календаря!
	function getCalendarLoad(res) {

		//после загрузки календаря снимаем прелоадер с приложения!!!
		$("body").removeClass("load");

	    calendarJSON = res;

		var currentTour = res.current;

		var allCountTour = res.count;
		//alert("Всего туров = "+allCountTour);
		var calendarSlideHTML = '';
		for (var i=1;i<=allCountTour;i++) {
			calendarSlideHTML += '<div class="swiper-slide tour-wrapper load"></div>';
		}
		$("#calendar-slider .swiper-wrapper").html(calendarSlideHTML);
		//alert(currentTour);
		//alert(currentTour);
		//alert(res);
		$("#calendar-wrapper").removeClass("load");
		$("#calendar .banner-ea").removeClass("load");
		calendarInit(currentTour-1);
	}
	//функции обновления календаря!
	function getCalendarRefresh(res) {
	    calendarJSON = res;
	    var currentTour = $("#name-tour").html().replace("-й тур", "")*1;
	    //alert(currentTour);

	    getMatchSupercup();

	    //очищаем все слайды и добавляем к ним прелоадеры
		//$("#calendar-wrapper").addClass("load");
		$("#calendar-slider .tour-wrapper").addClass("load").html("");
		//получаем данные из JSON массива для текущего тура
		//а потом для -2+2 от текущего
		getMatchTour(currentTour, $calendarSlider);
		//getMatchTour(currentTour-1, $calendarSlider);
		//getMatchTour(currentTour-2, $calendarSlider);
		//getMatchTour(currentTour+1, $calendarSlider);
		//getMatchTour(currentTour+2, $calendarSlider);
		//var currentTour = res.current;
		//alert(currentTour);
		//alert(currentTour);
		//alert(res);

		$("#calendar-wrapper, #calendar .banner-ea").removeClass("load");

		//calendarInit(currentTour-1);
	}

	function getClubsLoad(res) {
	    //alert(res.clubs.clubs2.name);
	    //alert(res.clubs.clubs2.name);
		if(res.clubs != null){
		 	var i = 0;
		 	var clubsHtml = "";
		 	clubsData = res;


		  	$.each(res.clubs, function(key, val){
		  		if (val.curturnir != "0") {
					clubsHtml += '<a href="#'+val.id+'" data-year="'+val.year+'" data-town="'+val.town+'" data-stadium="'+val.stadium+'" class="club"><img src="./img/logos/big/'+val.id+'.png"><p class="name">'+val.name+'</p></a>';
					i++;
					if (i%4 == 0) {
						clubsHtml += '<div class="clr"></div>';
					}
					if (i == 16) {
						$("#clubs-nav .clubs").html(clubsHtml);
						//alert(clubsHtml);
						getDataJSON('calendar.json', 'matches.php', {"action":"getMatches"}, getCalendarLoad, "");

					}
				}
			});
		}
	}

	//обновление клубов!
	function getClubsRefresh(res) {
	    //alert(res.clubs.clubs2.name);
		if(res.clubs != null){
		 	var i = 0;
		 	var clubsHtml = "";
		 	clubsData = res;


		  	$.each(res.clubs, function(key, val){
				clubsHtml += '<a href="#'+val.id+'" data-year="'+val.year+'" data-town="'+val.town+'" data-stadium="'+val.stadium+'" class="club"><img src="./img/logos/big/'+val.id+'.png"><p class="name">'+val.name+'</p></a>';
				i++;
				if (i%4 == 0) {
					clubsHtml += '<div class="clr"></div>';
				}
				if (i == 16) {
					$("#clubs-nav .clubs").html(clubsHtml);

					getDataJSON('calendar.json', 'matches.php', {"action":"getMatches"}, getCalendarRefresh, 1);

				}
			});
		}
	}

	
	var $mediaSlider,
		$calendarSlider,
		$leadersSlider,
		$videoSlider,
		$clubTabsSlider,
		$reportMatchSlider,
		$lsSlider;

	//фукнция удаления слайдов из слайдера медиа
	function mediaSliderClear() {
		if ($mediaSlider.slides.length > 0) {
			$mediaSlider.removeAllSlides();
			$mediaSlider.reInit();
		}
	}


	function parseIndexNews(res) {
		//alert(res);
		if(res.news != null){
			$.each(res.news, function(key, val){
				//alert(val.name);
				$mediaSlider.appendSlide('<img src="'+val.img_big+'"><span data-id="'+val.id+'" class="desc">'+val.name+'</span>', 'swiper-slide media news');	
			});
			$mediaSlider.swipeTo(0);
			$("#media-index").removeClass("load");
			
			$mediaSlider.reInit();
			$mediaSlider.startAutoplay();
		}

	}
	function parseIndexVideo(res) {
		if(res.video != null){
			$.each(res.video, function(key, val){
				$mediaSlider.appendSlide('<img src="'+val.bigimg+'"><span data-link="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" class="desc">'+val.name+'</span><i></i>', 'swiper-slide media video');	
			});
			$mediaSlider.swipeTo(0);
			$("#media-index").removeClass("load");
			
			$mediaSlider.reInit();		
			$mediaSlider.startAutoplay();
		}
				
	}
	function parseIndexPhoto(res) {
		if(res.photo != null){
			$.each(res.photo, function(key, val){
				$mediaSlider.appendSlide('<img src="'+val.bigimg+'"><span data-id="'+val.id+'" class="desc">'+val.name+'</span><i></i>', 'swiper-slide media photo');				
			});
			$mediaSlider.swipeTo(0);
			$("#media-index").removeClass("load");
			
			$mediaSlider.reInit();		
			$mediaSlider.startAutoplay();
		}
				
	}
	//оставнока/запуск медиа слайдеров 
	function mediaSliderControl(type) {
		//alert("1");
		if (type != "stop" && type != "start" && type != "refresh") {
			type = $("#tabs-media-slider .active").attr("href");
			//если слайды существуют тогда очищаем их
			mediaSliderClear();
		}
		//alert(type);
		switch (type) {

			case "news":
				if ($("#tabs-media-slider .news").data("load") == "0") {
					getDataJSON('news-index.json', 'news.php', {"action":"getNewsIndex"}, parseIndexNews, 1);
					$("#tabs-media-slider .news").data("load", "1");
				} else {
					getDataJSON('news-index.json', 'news.php', {"action":"getNewsIndex"}, parseIndexNews, "");
				}
				
			break;

			case "video":
				if ($("#tabs-media-slider .video").data("load") == "0") {
					getDataJSON('video-index.json', 'video.php', {"action":"getVideoIndex"}, parseIndexVideo, 1);
					$("#tabs-media-slider .video").data("load", "1");	
				} else {
					getDataJSON('video-index.json', 'video.php', {"action":"getVideoIndex"}, parseIndexVideo, "");
				}	
			break;

			case "photo":
				if ($("#tabs-media-slider .photo").data("load") == "0") {
					getDataJSON('photo-index.json', 'photo.php', {"action":"getPhotoIndex"}, parseIndexPhoto, 1);
					$("#tabs-media-slider .photo").data("load", "1");	
				} else {
					getDataJSON('photo-index.json', 'photo.php', {"action":"getPhotoIndex"}, parseIndexPhoto, "");
				}
				
			break;

			case "start":
				$mediaSlider.startAutoplay();
				//$mediaSlider.reInit();
			break;

			case "stop":
				$mediaSlider.stopAutoplay();
			break;

			case "refresh": 
				mediaSliderClear();
				var activeTab = $("#tabs-media-slider .active").attr("href");
				if (activeTab == "news") {
					getDataJSON('news-index.json', 'news.php', {"action":"getNewsIndex"}, parseIndexNews, 1);
					getDataJSON('video-index.json', 'video.php', {"action":"getVideoIndex"}, callbackEmpty, 1);
					getDataJSON('photo-index.json', 'photo.php', {"action":"getPhotoIndex"}, callbackEmpty, 1);	
				} else if (activeTab == "video") {
					getDataJSON('news-index.json', 'news.php', {"action":"getNewsIndex"}, callbackEmpty, 1);
					getDataJSON('video-index.json', 'video.php', {"action":"getVideoIndex"}, parseIndexVideo, 1);
					getDataJSON('photo-index.json', 'photo.php', {"action":"getPhotoIndex"}, callbackEmpty, 1);	
				} else if (activeTab == "photo") {
					getDataJSON('news-index.json', 'news.php', {"action":"getNewsIndex"}, callbackEmpty, 1);
					getDataJSON('video-index.json', 'video.php', {"action":"getVideoIndex"}, callbackEmpty, 1);
					getDataJSON('photo-index.json', 'photo.php', {"action":"getPhotoIndex"}, parseIndexPhoto, 1);	
				}
				
			break;
		}

	}

	/*******************************
	*******END ИНИЦИАЛИЗАЦИЯ*******
	*******************************/
	

	//массив содержащий ссылки назад
	//для переходов на внутреннии страницы
	var backArray = [];

	//глобальный массив всех переходов
	//для кнопки назад на android и windows phone
	var backArrayGlobal = [];

	//массив позиций игроков
	var ampluaData = [
		{
			"name":"Вратарь",
			"short_name": "В"
		}, 
		{
			
			"name":"Защитник",
			"short_name": "З"
			
		},
		{
			"name":"Полузащитник",
			"short_name": "П/3"
		},
		{
			"name":"Нападающий",
			"short_name": "Н"
		}
	];

	//основные данные о клубах
	var clubsData = [];//JSON.parse(localStorage.getItem("clubsData"));

	//лидеры и самые грубые!

	//фукнции формирования html кода лидеров и самых грубых
	function parsePlayersForward(res) {
		var playersForwardHTML = '';
		var i = 1;
		$.each(res.players, function(key, val){
			var color = "";
			if (i%2 == 0) {
				color = "color";
			}
			i++
			var pseudonymHTML = '';
			/*if (val.pseudonym != '') {
				pseudonymHTML = "<span>/ "+val.pseudonym+" /</span>";
			}*/
			//clubsData["clubs"]["clubs"+val.club1]["name"]
			playersForwardHTML += '<div data-id="'+val.id+'" class="player '+color+' "><figure><img src="'+val.img+'"></figure><div class="info"><div class="fio">'+val.name+' '+pseudonymHTML+'</div><div class="add"><div class="club"><img src="./img/logos/small/'+val.club+'.png"><p class="name">'+clubsData["clubs"]["clubs"+val.club]["name"]+'</p><p class="city">'+clubsData["clubs"]["clubs"+val.club]["town"]+'</p></div><div class="position"><p>'+val.amplua+'</p></div><div class="clr"></div></div><div class="res"><span class="goals">'+val.goal+'</span> голов<span class="matches">'+val.count+'</span> матчей</div></div></div>';	
		});

		$("#players-forward").html(playersForwardHTML+'<div class="banner-ea" data-url="http://rfpl.org/beautifully/"><img src="./img/banner-ipad2.png" class="phone"></div>').removeClass("load");
		$("#leaders").removeClass("load");
	}

	function parsePlayersFoul(res) {
		var playersFoulHTML = '';
		var i = 1;
		$.each(res.players, function(key, val){
			var color = "";
			if (i%2 == 0) {
				color = "color";
			}
			i++
			
			var pseudonymHTML = '';
			/*if (val.pseudonym != '') {
				pseudonymHTML = "<span>/ "+val.pseudonym+" /</span>";
			}*/
			playersFoulHTML += '<div data-id="'+val.id+'" class="player '+color+' "><figure><img src="'+val.img+'"></figure><div class="info"><div class="fio">'+val.name+' '+pseudonymHTML+'</span></div><div class="add"><div class="club"><img src="./img/logos/small/'+val.club+'.png"><p class="name">'+clubsData["clubs"]["clubs"+val.club]["name"]+'</p><p class="city">'+clubsData["clubs"]["clubs"+val.club]["town"]+'</p></div><div class="position"><p>'+val.amplua+'</p></div><div class="clr"></div></div><div class="res"><span class="cards">'+val.yellow+'</span><span class="card yellow"></span><span class="cards">'+val.red+'</span><span class="card red"></span><span class="matches">'+val.count+'</span> матчей</div></div></div>';
		});
		$("#players-foul").html(playersFoulHTML+'<div class="banner-ea" data-url="http://rfpl.org/beautifully/"><img src="./img/banner-ipad2.png" class="phone"></div>').removeClass("load");
	}
	//фукнции получения лидеров и самых грубых
	//если type=load обновление списка лидеров
	//вне зависимости от наличия в local storage
	function getPlayersForward() {
		//добавляем прелоадер
		$("#players-forward").addClass("load");
		getDataJSON('players-forward.json', 'players.php', {"action":"getPlayersForward"}, parsePlayersForward, "");
	}

	function getPlayersFoul() {
		//добавляем прелоадер
		$("#players-foul").addClass("load");
		getDataJSON('players-foul.json', 'players.php', {"action":"getPlayersFoul"}, parsePlayersFoul, "");
	}

	//ПРОФИЛЬ ИГРОКА
	//разбор основной информации о игроке при загрузке
	function parsePlayersInfo(res) {

		if (res != "noconnect") {
			//ID игрока
			var id = $("#players-inner").data("id");

			//сбрасываем флаг кол-ва загрузки страниц для страниц все новости, видео и фото игрока
			//и очищаем блоки со старым контентом 
		    playersNewsAllPageLoad = 1;
		    $("#news-list-players").html("");
		    playersVideoAllPageLoad = 1;
		    $("#video-list-players").html("");
		    playersPhotoAllPageLoad = 1;
		    $("#photo-list-players").html("");

			var val = res.player;
			var pseudonymHTML = '';
			if (val.pseudonym != '') {
				pseudonymHTML = "("+val.pseudonym+")";
			}
			var topHTML = '<div class="top"><figure><img src="'+val.img+'"></figure><div class="info"><p class="name">'+val.name+' '+pseudonymHTML+'</p><p>'+clubsData["clubs"]["clubs"+val.club]["name"]+' <span>'+clubsData["clubs"]["clubs"+val.club]["town"]+'</span></p><p>'+val.nomer+' | '+val.amplua+'</p><div class="line"></div><p><span class="label">Дата рождения</span> '+val.birthdate+' г.</p><p><span class="label">Гражданство</span> '+val.country+' <img src="./img/flag/'+val.citizen+'.gif" alt=""></p><p><span class="label">Рост</span> '+val.growth+' СМ</p><p><span class="label">Вес</span> '+val.weight+' КГ</p></div></div><div class="stats"><div class="item"><p class="label">матчей</p><p>'+val.matches+'</p></div><div class="item"><p class="label">минут</p><p>'+val.time+'</p></div><div class="item"><p class="label">голы</p><p>'+val.goal+'</p></div><div class="item"><p class="label">пенальти</p><p>'+val.penalty+'</p></div><div class="item"><p class="label"><img src="./img/event3.png"></p><p>'+val.yellow+'</p></div><div class="item"><p class="label"><img src="./img/event4.png"></p><p>'+val.red+'</p></div></div>';		
			//alert(res.player.name);
			$("#profile-top").html(topHTML);
			$("#players-inner").removeClass("load").data("id", id);
				
			getPlayersNews(id, 2);
			getPlayersVideo(id, 2);
			getPlayersPhoto(id, 2);

		} else {
			//если интернета нет
			$("#noconnect").show();
			$("#players-inner").removeClass("load");
			$("#refresh-repeat").data("action", "playersinfo");
		}
	}
	//получение основной информации о игроке по ID
	function getPlayersInfo(id) {
		//добавляем ID игрока и прелоадеры
		$("#players-inner").data("id", id);
		$("#players-inner, #profile-news, #profile-video, #profile-photo").addClass("load");
		getDataJSON('temp.json', 'players.php', {"action":"getPlayersInfo", "id" : id}, parsePlayersInfo, 1);
	}

	//получение новостей, видео и фото игрока
	//указываем ID и кол-во записей
	var playersProfileNewsData = "";
	function getPlayersNews(id, max) {
		var i = 0;
		var newsHTML = '';
		$.post("http://rfpl.org/api/v3/news.php",{
			"action" : "getNewsPlayer",
			"player" : id
		},function (res) {

	    	playersProfileNewsData = res;
	    	
			if(res.news != null){
				var countNews = Object.keys(res.news).length;
				$.each(res.news, function(key, val){
					var descHtml = "";
					if (val.short_s != "") {
						descHtml = '<p class="desc">'+val.short_s+'</p>';
					}
					var rfplHtml = "";
					if (val.type == "18") {
						rfplHtml = '<span class="rfpl">РФПЛ</span>';
					}

					newsHTML += '<a href="#" class="news" data-id="'+val.id+'"><figure><img src="'+val.img+'"></figure><p><span class="date">'+val.date+'</span>'+rfplHtml+'</p><p class="name">'+val.name+'</p>'+descHtml+'</a>';

					i++;
					//выход из цикла!
					if (i == max) {
						return false;
					}

				});
				$("#profile-news .news-wrapper").html(newsHTML);
				$("#profile-news").removeClass("load");
				if (countNews <= 2) {
					$("#all-news-player-link").hide();
				} else {
					$("#all-news-player-link").show();
				}
				
			} else {
				//новостей нет!
				$("#all-news-player-link").hide();
				$("#profile-news").removeClass("load");
				$("#profile-news .news-wrapper").html('<div class="link">Новости не найдены</div>');
			}

		});
	}

	var playersProfileVideoData = "";
	function getPlayersVideo(id, max) {
		var i = 0;
		var videoHTML = '';
		$.post("http://rfpl.org/api/v3/video.php",{
			"action" : "getVideoPlayer",
			"player" : id
		},function (res) {
	    	
	    	playersProfileVideoData = res;
	    	

			if(res.video != null){
				var countVideo = Object.keys(res.video).length;
				$.each(res.video, function(key, val){
					videoHTML += '<a class="video" href="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" target="_blank"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.time+'<i></i> </span></span><span class="bg"></span></a>';

					i++;
					//выход из цикла!
					if (i == max) {
						return false;
					}
				});

				$("#profile-video .video-wrapper .video-list").html(videoHTML);
				$("#profile-video").removeClass("load");

				if (countVideo <= 2) {
					$("#all-video-player-link").hide();
				} else {
					$("#all-video-player-link").show();
				}
				
			} else {
				//видео нет!
				$("#profile-video").removeClass("load");
				$("#all-video-player-link").hide();
				$("#profile-video .video-wrapper .video-list").html('<div class="link">Видео не найдено</div>');
			}

		});
	}

	var playersProfilePhotoData = "";
	function getPlayersPhoto(id, max) {
		var i = 0;
		var photoHTML = '';
		$.post("http://rfpl.org/api/v3/photo.php",{
			"action" : "getPhotoPlayer",
			"player" : id
		},function (res) {
	    	//alert(res.news);
	    	playersProfilePhotoData = res;
	    	
			if(res.photo != null){
				var countPhoto = Object.keys(res.photo).length;
				$.each(res.photo, function(key, val){
					photoHTML += '<a href="#" class="photoimg"><img src="'+val.img+'"></a>';
					i++;
					//выход из цикла!
					if (i == max) {
						return false;
					}
				});

				$("#profile-photo .photo-wrapper .photo-list").html(photoHTML);
				$("#profile-photo").removeClass("load");
				if (countPhoto <= 2) {
					$("#all-photo-player-link").hide();
				} else {
					$("#all-photo-player-link").show();
				}
			} else {
				//фото нет!
				$("#profile-photo").removeClass("load");
				$("#all-photo-player-link").hide();
				$("#profile-photo .photo-wrapper .photo-list").html('<div class="link">Фото не найдено</div>');
			}

		});
	}

	//для новостей игрока переменная страницы загрузки и флаг загрузки!
	var playersNewsAllPageLoad = 1,
		playersNewsAllLoadFlag = 0;
	//получение всех новостей, видео и фото игрока
	function getPlayersNewsAll(page) {
		
		var idPlayer = $("#players-inner").data("id");

		var newsHTML = '';
		//если страница не указана - значит новости у нас уже есть и это начальная загрузка полученных новостей из массива
		if (page == "") {
			playersNewsAllPageLoad = 1;
			$.each(playersProfileNewsData.news, function(key, val){
				var descHtml = "";
				if (val.short_s != "") {
					descHtml = '<p class="desc">'+val.short_s+'</p>';
				}
				var rfplHtml = "";
				if (val.type == "18") {
					fplHtml = '<span class="rfpl">РФПЛ</span>';
				}
				newsHTML += '<a href="#" class="news" data-id="'+val.id+'"><figure><img src="'+val.img+'"></figure><p><span class="date">'+val.date+'</span>'+rfplHtml+'</p><p class="name">'+val.name+'</p>'+descHtml+'</a>';
			});

			$("#news-list-players").html(newsHTML);
			$("#news-players").animate({scrollTop:0}, '250');
			$("#news-players, #news-list-players").removeClass("load").scrollTop(0);
		} else {

			if (checkConnection() == true) {
				//если страница указана - подгружаем новости!
				//указываем флаг загрузки данных!
				playersNewsAllLoadFlag = 1;
				//добавляем блок загрузки данных
				$("#news-list-players").find(".noconnect-small").remove();
				$("#news-list-players").append('<div class="preloader"></div>');
				$.post("http://rfpl.org/api/v3/news.php", {
					"action" : "getNewsPlayer",
					"player" : idPlayer,
					"page" : playersNewsAllPageLoad
				}, function (res) {
					if(res.news != null){
						$.each(res.news, function(key, val){
							var descHtml = "";
							if (val.short_s != "") {
								descHtml = '<p class="desc">'+val.short_s+'</p>';
							}
							var rfplHtml = "";
							if (val.type == "18") {
								rfplHtml = '<span class="rfpl">РФПЛ</span>';
							}

							newsHTML += '<a href="#" class="news" data-id="'+val.id+'"><figure><img src="'+val.img+'"></figure><p><span class="date">'+val.date+'</span>'+rfplHtml+'</p><p class="name">'+val.name+'</p>'+descHtml+'</a>';
						});

						$("#news-list-players").find(".preloader").hide("fade", 250, function(){
							$(this).remove();
						});
						$("#news-list-players").append(newsHTML);	

						playersNewsAllPageLoad++;
						playersNewsAllLoadFlag = 0;
					} else {
						setTimeout(function(){
							$("#news-list-players").find(".preloader").remove();
							playersNewsAllLoadFlag = 0;
						}, 250);
						
					}
					

				});

			} else {
				//если интернета нет
				//удаляем прелоадер
				$("#news-list-players").find(".preloader").remove();
				//добавляем сообщение об отсутствии интернета
				$("#news-list-players").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="newsplayersscroll" href="#"><i></i> Повторить попытку</a></div>');		

			}
			
		}

	}

	//для видео игрока переменная страницы загрузки и флаг загрузки!
	var playersVideoAllPageLoad = 1,
		playersVideoAllLoadFlag = 0;
	function getPlayersVideoAll(page) {
		var idPlayer = $("#players-inner").data("id");

		var videoHTML = '';
		//если страница не указана - значит видео у нас уже есть и это начальная загрузка полученных видео из массива
		if (page == "") {
			playersVideoAllPageLoad = 1;
			$.each(playersProfileVideoData.video, function(key, val){
				videoHTML += '<a class="video" href="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" target="_blank"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.time+'<i></i> </span></span><span class="bg"></span></a>';
			});

			$("#video-list-players").html(videoHTML);
			$("#video-players").removeClass("load");
			$("#video-players").animate({scrollTop:0}, '250');
		} else {

			if (checkConnection() == true) {
				//если страница указана - подгружаем новости!
				//указываем флаг загрузки данных!
				playersVideoAllLoadFlag = 1;
				//alert(playersNewsAllLoadFlag);
				//добавляем блок загрузки данных
				$("#video-list-players").find(".noconnect-small").remove();
				$("#video-list-players").append('<div class="preloader"></div>');
				//alert(idPlayer+"|||"+playersVideoAllPageLoad);
				$.post("http://rfpl.org/api/v3/video.php",{
					"action" : "getVideoPlayer",
					"player" : idPlayer,
					"page" : playersVideoAllPageLoad
				},function (res) {
			    	//alert(res.news);
					if(res.video != null){
						$.each(res.video, function(key, val){
							videoHTML += '<a class="video" href="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" target="_blank"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.time+'<i></i> </span></span><span class="bg"></span></a>';
						});

						$("#video-list-players").find(".preloader").hide("fade", 250, function(){
							$(this).remove();
						});
						$("#video-list-players").append(videoHTML);	

						playersVideoAllPageLoad++;
						playersVideoAllLoadFlag = 0;
					} else {
						setTimeout(function(){
							$("#video-list-players").find(".preloader").remove();
							playersVideoAllLoadFlag = 0;
						}, 250);
						
					}
					//alert(playersVideoAllPageLoad+" ||| "+$("#video-list-players .video").length);
				});

			} else {
				//если интернета нет
				//удаляем прелоадер
				$("#video-list-players").find(".preloader").remove();
				//добавляем сообщение об отсутствии интернета
				$("#video-list-players").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="videoplayersscroll" href="#"><i></i> Повторить попытку</a></div>');		

			}
			
		}
	}

	//для фото игрока переменная страницы загрузки и флаг загрузки!
	var playersPhotoAllPageLoad = 1,
		playersPhotoAllLoadFlag = 0;

	function getPlayersPhotoAll(page) {
		var idPlayer = $("#players-inner").data("id");

		var photoHTML = '';
		//если страница не указана - значит фото у нас уже есть и это начальная загрузка полученных фото из массива
		if (page == "") {

			playersPhotoAllPageLoad = 1;

			$.each(playersProfilePhotoData.photo, function(key, val){
				photoHTML += '<a href="#" class="photoimg"><img src="'+val.img+'"></a>';
			});

			$("#photo-list-players").html(photoHTML);
			$("#photo-players").removeClass("load");
			$("#photo-players").animate({scrollTop:0}, '250');
		} else {
			if (checkConnection() == true) {
				//если страница указана - подгружаем новости!
				//указываем флаг загрузки данных!
				playersPhotoAllLoadFlag = 1;
				//alert(playersNewsAllLoadFlag);
				//добавляем блок загрузки данных
				$("#photo-list-players").find(".noconnect-small").remove();
				$("#photo-list-players").append('<div class="preloader"></div>');
				//alert(idPlayer+"|||"+playersVideoAllPageLoad);
				$.post("http://rfpl.org/api/v3/photo.php",{
					"action" : "getPhotoPlayer",
					"player" : idPlayer,
					"page" : playersPhotoAllPageLoad
				},function (res) {
			    	//alert(res.news);
					if(res.photo != null){
						$.each(res.photo, function(key, val){
							photoHTML += '<a href="#" class="photoimg"><img src="'+val.img+'"></a>';
						});

						$("#photo-list-players").find(".preloader").hide("fade", 250, function(){
							$(this).remove();
						});
						$("#photo-list-players").append(photoHTML);	

						playersPhotoAllPageLoad++;
						playersPhotoAllLoadFlag = 0;
					} else {
						setTimeout(function(){
							$("#photo-list-players").find(".preloader").remove();
							playersPhotoAllLoadFlag = 0;
						}, 250);
						
					}
					//alert(playersVideoAllPageLoad+" ||| "+$("#video-list-players .video").length);
				});

			} else {
				//если интернета нет
				//удаляем прелоадер
				$("#photo-list-players").find(".preloader").remove();
				//добавляем сообщение об отсутствии интернета
				$("#photo-list-players").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="photoplayersscroll" href="#"><i></i> Повторить попытку</a></div>');	
			}

		}
	}

	//функция формирования HTML кода турнирной таблицы
	function parseTournamentTable(res) {
		var i = 1;
		var tableHTML = '';
		tableHTML += '<table><tr><th class="line"></th><th class="w10"><p>&nbsp;</p></th><th><p>Команда</p></th><th class="w10"><p>Игры</p></th><th class="w10"><p>В</p></th><th class="w10"><p>Н</p></th><th class="w10"><p>П</p></th><th class="w10"><p>Очки</p></th></tr>';
		$.each(res.table, function(key, val){
			//цвета строк
			var addClassTR = '';
			if (i%2 == 0) {
				addClassTR = "color";
			}
			//выделение 1х и последних мест таблицы
			var addClassTD = '';
			if (i == 1) {
				addClassTD = 'yellow';
			}
			if (i == 2 || i == 3) {
				addClassTD = 'green';
			}
			if (i== 13 || i == 14) {
				addClassTD = 'red';
			}
			if (i == 15 || i == 16) {
				addClassTD = 'dark-red';
			}

			tableHTML += '<tr class="'+addClassTR+'" ><td class="line '+addClassTD+' "></td><td class="w10"><p>'+i+'</p></td><td><p>'+val.name+'</p></td><td class="w10"><p>'+val.g+'</p></td><td class="w10"><p>'+val.w+'</p></td><td class="w10"><p>'+val.d+'</p></td><td class="w10"><p>'+val.l+'</p></td><td class="w10"><p><b>'+val.p+'</b></p></td></tr>';
			i++;

		});

		tableHTML += '</table>';
		$("#tournament-table .tournament-table").html(tableHTML+'<div class="banner-ea" data-url="http://rfpl.org/beautifully/"><img src="./img/banner-ipad2.png" class="phone"></div>');

		//получаем высоту ячейки
		var headerHeight = $("header").height()*1;
		var windowHeight = $(window).height()*1;
		//alert(headerHeight+" ||| "+windowHeight);
		var rowHeight = (windowHeight-headerHeight)/17;
		$("#tournament-table .tournament-table td, #tournament-table .tournament-table th").css({
			"height": rowHeight
		});


		$("#tournament-table").removeClass("load");
	}
	//турнирная таблица
	//var tournamentTableJSON = null;//JSON.parse(localStorage.getItem("tournamentTableJSON"));
	//фукнция получения турнирной таблицы
	//если type=load загрузка внезависимости от наличия данных в local storage
	function getTournamentTable(type) {
		getDataJSON('tournament-table.json', 'turnir.php', {"action":"getTurnirTable"}, parseTournamentTable, "");
	}


	//фукнции формирования HTML кода новостей при загрузке и скроле
	function parseNewsList(res) {

		//если есть подключение к интернету
		if (res != "noconnect") {
			var newsHTML = '';
			var i = 0;
			$.each(res.news, function(key, val){

				if (i == 0) {
					newsHTML += '<a href="#" class="main-news" data-id="'+val.id+'"><figure><img src="'+val.img_big+'"></figure><div class="desc"><p class="name">'+val.name+'</p><p class="date">'+val.date+'<i></i></p></div></a>';
				} else {
					var descHtml = "";
					if (val.short_s != "") {
						descHtml = '<p class="desc">'+val.short_s+'</p>';
					}
					var rfplHtml = "";
					if (val.type == "18") {
						rfplHtml = '<span class="rfpl">РФПЛ</span>';
					}
					newsHTML += '<a href="#" class="news" data-id="'+val.id+'"><figure><img src="'+val.img+'"></figure><p><span class="date">'+val.date+'</span>'+rfplHtml+'</p><p class="name">'+val.name+'</p>'+descHtml+'</a>';
				}

				i++;
			});
			$("#news-list").html(newsHTML);
			$("#news").removeClass("load");

		} else {
			//если интернета нет
			$("#noconnect").show();
			$("#news").removeClass("load");
			$("#refresh-repeat").data("action", "news");
		}
	}

	function parseNewsScrollList(res) {

		//если есть подключение к интернету
		if (res != "noconnect") {
			var newsHTML = '';
			//подгрузка новостей при скроле!
			//первоначальная загрузка полсдених новостей в local storage	
			$.each(res.news, function(key, val){
						
				var descHtml = "";
				if (val.short_s != "") {
					descHtml = '<p class="desc">'+val.short_s+'</p>';
				}
				var rfplHtml = "";
				if (val.type == "18") {
					rfplHtml = '<span class="rfpl">РФПЛ</span>';
				}
				newsHTML += '<a href="#" class="news" data-id="'+val.id+'"><figure><img src="'+val.img+'"></figure><p><span class="date">'+val.date+'</span>'+rfplHtml+'</p><p class="name">'+val.name+'</p>'+descHtml+'</a>';

						
			});

			$("#news-list").find(".preloader").hide("fade", 250, function(){
				$(this).remove();
			});
			$("#news-list").append(newsHTML);
			newsPageLoad++;
			newsLoadFlag = 0;

		} else {
			//если интернета нет
			//удаляем прелоадер
			$("#news-list").find(".preloader").remove();
			//добавляем сообщение об отсутствии интернета
			$("#news-list").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="newsscroll" href="#"><i></i> Повторить попытку</a></div>');		

		}
	}

	//для списка новостей переменная страницы загрузки и флаг загрузки!
	var newsPageLoad = 1,
		newsLoadFlag = 0;
	//список новостей из раздела новости
	//var newsJSON = null;//JSON.parse(localStorage.getItem("newsJSON"));
	function getNews(page) {
		var i = 0;
		var newsHTML = '';
		if (page == "") {
			//обнуляем страницу загрузки при получении списка новостей
			newsPageLoad = 1;
			$("#news").addClass("load");
			getDataJSON('temp.json', 'news.php', {"action":"getNews"}, parseNewsList, 1);
		} else {
			$("#news-list").append('<div class="preloader"></div>');
			newsLoadFlag = 1;
			getDataJSON('temp.json', 'news.php', {"action":"getNews", "page":page}, parseNewsScrollList, 1);
		}
		
	}

	//формирование html страницы полной новости
	function parseNewsFull(res) {
		var rfplHtml = "";
		if (res["type"] == "18") {
			rfplHtml = '<span class="rfpl">РФПЛ</span>';
		}
		var fullNewsHtml = '<figure><img src="'+res["img_big"]+'"></figure><div class="news-content"><div class="top"><span class="date">'+res["date"]+'</span>'+rfplHtml+'</div><p class="name">'+res["name"]+'</p> '+res["text"]+'</div>'; 
		$("#news-full").html(fullNewsHtml+'<div class="banner-ea" data-url="http://rfpl.org/beautifully/"><img src="./img/banner-ipad2.png" class="phone"></div>');
		$("#news-inner").removeClass("load");
	}
	//функция получения и показа полной новости
	function fullNewsLoad(id) {
		$("#news-inner").addClass("load");
		$("#news-full").html("");
		getDataJSON('temp.json', 'news.php', {"action":"getNewsInfo", "id": id}, parseNewsFull, 1);
	}

	//функция появления/скрытия главного меню
	function viewMainNav(type) {

		//показ и скрытие меню с анимацией
		if (type == "show") {
			$("#fade").show();
			$("#main-nav").addClass("open");
			if ($("#calendar").hasClass("current")) {
				mediaSliderControl("stop");
			}
			
			$("#refresh").addClass("disable");
		}

		if (type == "hide") {
			$("#fade").hide();
			$("#main-nav").removeClass("open");

			if ($("#calendar").hasClass("current")) {
				mediaSliderControl("start");
			}
			$("#refresh").removeClass("disable");
		}

		//моментальное скрытие меню
		if (type == "disable") {
			$("#fade").hide();
			$("#main-nav").removeClass("open");
			$("#refresh").removeClass("disable");
		}

		//alert($("#refresh").attr("class"));

	}

	//видео согласно вкладкам раздела
	var videoBestJSON = "";//JSON.parse(localStorage.getItem("videoBestJSON"));
	var videoReviewJSON = "";//JSON.parse(localStorage.getItem("videoReviewJSON"));
	var videoMomentsJSON = "";//JSON.parse(localStorage.getItem("videoMomentsJSON"));
	var videoStoryJSON = "";//JSON.parse(localStorage.getItem("videoStoryJSON"));


	//для видео флаг загрузки и номера страниц загрузки по вкладкам соответственно!
	var videoLoadFlag = 0,
		videoBestPageLoad = 1, 
		videoMomentsPageLoad = 1,
		videoReviewPageLoad = 1,
		videoStoryPageLoad = 1;

	//функция получения первоначального контента для вкладок раздела видео
	function getVideo() {
		
		//обнуляем данные по страницам загрузки вкладок видео
		videoBestPageLoad = 1; 
		videoMomentsPageLoad = 1;
		videoReviewPageLoad = 1;
		videoStoryPageLoad = 1;
		$videoSlider.reInit();
		//запускаем фукнции загрузки вкладов с видео!
		loadTabVideo(0, "");
		//loadTabVideo(1, "");
		//loadTabVideo(2, "");
		//loadTabVideo(3, "");

	}

	//фукнции формирования HTML кода при загрузке и подгрузке видео
	
	function parseTabVideo(res) {
		var type = res.type;
		if (type == 1) {
			var $videoLoadBlock = $("#video-review");
		} else if (type == 2) {
			var $videoLoadBlock = $("#video-moments");
		} else if (type == 3) {
			var $videoLoadBlock = $("#video-story");
		} else {
			var $videoLoadBlock = $("#video-best");
		}

		if (res != "noconnect") {

			var videoHTML = '';
						
			$.each(res.video, function(key,val){
				videoHTML += '<a class="video" href="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" target="_blank"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.time+'<i></i> </span></span><span class="bg"></span></a>';
			});
						
			$videoLoadBlock.html(videoHTML);
			$videoLoadBlock.parent().removeClass("load");
			$("#video").removeClass("load");
			$videoSlider.reInit();	
		} else {
			//alert("1");
			//если интернета нет
			$("#noconnect").show();
			$("#video").removeClass("load");
			$videoLoadBlock.parent().removeClass("load");
			$("#refresh-repeat").data("action", "video");
			$videoSlider.reInit();

		}
	}
	function parseTabScrollVideo(res) {

		if (res != "noconnect") {
			var type = res.type;
			//в зависимости от типа вкладки увеличиваем номер страницы для загрузки
			if (type == 1) {
				var $videoLoadBlock = $("#video-review");
				videoReviewPageLoad = videoReviewPageLoad+1;
			} else if (type == 2) {
				var $videoLoadBlock = $("#video-moments");
				videoMomentsPageLoad = videoMomentsPageLoad+1;
			} else if (type == 3) {
				var $videoLoadBlock = $("#video-story");
				videoStoryPageLoad = videoStoryPageLoad+1;
			} else {
				var $videoLoadBlock = $("#video-best");
				videoBestPageLoad = videoBestPageLoad+1;
			}
			//alert(type);
			var videoHTML = '';

			$.each(res.video, function(key,val){
				videoHTML += '<a class="video" href="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" target="_blank"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.time+'<i></i> </span></span><span class="bg"></span></a>';
			});
			//alert(videoHTML);
			$videoLoadBlock.append(videoHTML);
			$videoLoadBlock.find(".preloader").hide("fade", 250, function(){
				$(this).remove();
			});
			videoLoadFlag = 0;

		} else {
			var type = $("#tabs-video-wrapper a.active").attr("href").replace("#", "")*1;
			//если интернета нет
			//в зависимости от типа вкладки увеличиваем номер страницы для загрузки
			if (type == 1) {
				var $videoLoadBlock = $("#video-review");
			} else if (type == 2) {
				var $videoLoadBlock = $("#video-moments");
			} else if (type == 3) {
				var $videoLoadBlock = $("#video-story");
			} else {
				var $videoLoadBlock = $("#video-best");
			}

			$videoLoadBlock.find(".noconnect-small").remove();
			//удаляем прелоадер
			//alert(type+" | "+$videoLoadBlock.attr("id")+" | "+$videoLoadBlock.find(".preloader").length);
			$videoLoadBlock.find(".preloader").remove();
			//добавляем сообщение об отсутствии интернета
			$videoLoadBlock.append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="videoscroll" href="#"><i></i> Повторить попытку</a></div>');	

			//$videoLoadBlock.parent().scrollTop(100000);

			$videoSlider.reInit();	

		}	

	}

	//фукнция загрузки видео: передаем тип видео и номер страницы!
	//1 — обзоры, 2 — моменты, 3 — сюжеты, остальное - лучшее!
	function loadTabVideo(type, page) {
		//http://rfpl.org/api/v3/video.php?action=getVideo&page=1&type=1
		if (page == "") {
			getDataJSON('temp.json', 'video.php', {"action":"getVideo", "type" : type}, parseTabVideo, 1);
			
		} else {
			//в зависимости от типа вкладки увеличиваем номер страницы для загрузки
			if (type == 1) {
				var $videoLoadBlock = $("#video-review");
			} else if (type == 2) {
				var $videoLoadBlock = $("#video-moments");
			} else if (type == 3) {
				var $videoLoadBlock = $("#video-story");
			} else {
				var $videoLoadBlock = $("#video-best");
			}
			//подгрузка видео при скроле!
			//добавляем флаг загрузки и прелоадер
			videoLoadFlag = 1;
			$videoLoadBlock.append('<div class="preloader"></div>');
			getDataJSON('temp.json', 'video.php', {"action":"getVideo", "type" : type, "page" : page}, parseTabScrollVideo, 1);
		}
	}

	//фукнции формирования HTML кода альбомов при загрузке и подгрузке
	function parsePhotoalbumList(res) {
		var photoHTML = "";

		if (res != "noconnect") {

			$.each(res.photo, function(key, val){

				photoHTML += '<a data-id="'+val.id+'" href="#" class="photo"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.count+' фото<i></i> </span></span><span class="bg"></span></a>';
					
			});

			$("#photoalbum-list").html(photoHTML);
			$("#photo").removeClass("load");

		} else {
			//если интернета нет
			$("#noconnect").show();
			$("#photo").removeClass("load");
			$("#refresh-repeat").data("action", "photo");

		}	

	}

	function parsePhotoalbumScrollList(res) {
		var photoHTML = "";

		if (res != "noconnect") {

			$.each(res.photo, function(key, val){

				photoHTML += '<a data-id="'+val.id+'" href="#" class="photo"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.count+' фото<i></i> </span></span><span class="bg"></span></a>';
						
			});

			$("#photoalbum-list").append(photoHTML);
			$("#photoalbum-list").find(".preloader").hide("fade", 250, function(){
				$(this).remove();
			});
			photoalbumLoadFlag = 0;
			photoalbumPageLoad++;

		} else {

			//удаляем прелоадер
			$("#photoalbum-list").find(".preloader").remove();
			//добавляем сообщение об отсутствии интернета
			$("#photoalbum-list").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="photoscroll" href="#"><i></i> Повторить попытку</a></div>');		

		}

	}
	//для фотоальбомов флаг загрузки и номер страницы загрузки
	var photoalbumLoadFlag = 0,
		photoalbumPageLoad = 1;
	//функция получения HTML кода фотоальбомов для раздела ФОТО
	//и подгрузки при скроле если указан page
	function getPhoto(page) {
		//http://rfpl.org/api/v3/photo.php?action=getPhoto&page=1
		var photoHTML = "";
		if (page == "") {
			//обнуляем переменную загрузки страниц
			photoalbumPageLoad = 1;
			//первоначальная загрузка фотоальбомов
			$("#photo").addClass("load");
			getDataJSON('temp.json', 'photo.php', {"action":"getPhoto"}, parsePhotoalbumList, 1);

		} else {
			//подгрузка фотоальбомов при скроле
			photoalbumLoadFlag = 1;
			$("#photoalbum-list").append('<div class="preloader"></div>');
			getDataJSON('temp.json', 'photo.php', { "action":"getPhoto", "page" : page }, parsePhotoalbumScrollList, 1);
		
		}
		
	}

	//фукнции формирования html кода фотоальбомов
	function parsePhotoList(res) {

		if (res != "noconnect") {

			if (res != null) {
				var photoHtml = "";
				$.each(res.photo, function(key, val){
					  photoHtml += '<a href="#" class="photoimg"><img src="'+val.img+'"></a>';
				});	
				$("#photo-inner").removeClass("load");
				$("#photo-list").append(photoHtml);

				photoalbumPhotoLoadFlag = 0;
			}
		} else {
			//если интернета нет
			$("#noconnect").show();
			$("#photo-inner").removeClass("load");
			$("#refresh-repeat").data("action", "photoinner");

		}
	}
	function parsePhotoScrollList(res) {
		if (res != "noconnect") {
			if (res != null) {
				var photoHtml = "";
				$.each(res.photo, function(key, val){
					  photoHtml += '<a href="#" class="photoimg"><img src="'+val.img+'"></a>';
				});	
				$("#photo-inner").removeClass("load");
				$("#photo-list").append(photoHtml);

				$("#photo-list .preloader").hide("fade", 250, function(){
					$(this).remove();
				});	
				photoalbumPhotoPageLoad++;
				photoalbumPhotoLoadFlag = 0;
			}
		} else {
			//если интернета нет
			//удаляем прелоадер
			$("#photo-list").find(".preloader").remove();
			//добавляем сообщение об отсутствии интернета
			$("#photo-list").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="photoinnerscroll" href="#"><i></i> Повторить попытку</a></div>');		

		}
	}
	//для фотографий альбома переменная страницы загрузки и флаг загрузки!
	var photoalbumPhotoPageLoad = 1,
		photoalbumPhotoLoadFlag = 0;
	//получение и формирование фотографий фотоальбома
	//если page не указан значит загрузка первых
	function photoLoad(id, page) {
		var lastID = $("#photo-inner").data("id");
		//если заходим в последний загруженный альбом
		//просто показываем его
		//иначе загрузка или подгрузка фотографий альбома
		if (id == lastID && page == "") {} else {
			$("#photo-inner").data("id", id);

			if (page == "") {
				//сбрасываем счетчик страниц подгрузки при скроле!
				photoalbumPhotoPageLoad = 1;
				$("#photo-list").html("");
				$("#photo-inner").addClass("load");
				
				getDataJSON('temp.json', 'photo.php', {"action":"getPhotoInfo", "id": id}, parsePhotoList, 1);
			} else {
				photoalbumPhotoLoadFlag = 1;
				$("#photo-list").append('<div class="preloader"></div>');
				getDataJSON('temp.json', 'photo.php', {"action":"getPhotoInfo", "id": id, "page" : page}, parsePhotoScrollList, 1);
			}
		}
		
	}

	/*******************************
	*******ОСНОВНАЯ НАВИГАЦИЯ*******
	*******************************/

	//фукнция перехода в подраздел(страницы полного просмотра)
	function pageInner(toPage, fromPage) {

		var $toPage = $(toPage);
		var $fromPage = $(fromPage);
		var $pages = $("#pages");

		if ($pages.hasClass("load")) {
			return;
		} else {

			$("#nav-button").hide();
			$("#back").show("fade", 300);
			$("#swipe-nav").removeClass("hide");
			animateInner("next", $toPage, $fromPage);
				
		}

	}
	//фукнция анимации перехода на внутреннюю или возврата назад
	//передаем действие(next, back) и объекты куда переходим и откуда
	function animateInner(action, $toPage, $fromPage) {

		//$("#title-page").html($toPage.data("title")+"<i></i>");
		$("#pages").attr("style", "");

		if ($toPage.data("refresh") == true) {
	   		$("#refresh").show();
	   	} else {
	   		$("#refresh").hide();
	   	}

	   	$("#title-page").html($toPage.data("title")+"<i></i>");

		//возврат назад
		if (action == "back") {

			$fromPage.addClass("transitionback right");
			//если возврат со страницы Событияа добавляем эффект хедеру
			if ($fromPage.attr("id") == "report-match") {
				$("#report-match-header").addClass("transitionback right");
				setTimeout(function(){
					$("#report-match-header").removeClass("show transitionback right");
				}, 300);
			}

			//если возврат со страницы Лиги Ставок
			if ($fromPage.attr("id") == "ls") {
				$("#ls-header").addClass("transitionback right");
				setTimeout(function(){
					$("#ls-header").removeClass("show transitionback right");
				}, 300);
			}

			setTimeout(function(){
			   	$toPage.removeClass("transitionback").addClass("current");
			    $fromPage.removeClass("current transitionback right");
			    $("#noconnect").hide();

			    if ($toPage.attr("id") == "news") {
			    	//newsScroll.refresh();
			    	var top = $("#news").data("scroll")*1;
			    	//alert(top);
			    	//$("#title-page").html(top);

			    	$("#news").scrollTop(top);
			    }

			    if ($toPage.attr("id") == "photo") {
			    	//photoScroll.refresh();
			    	var top = $("#photo").data("scroll");
			    	//$("#title-page").html(top);
			    	//$("#photoalbum-list .lastalbum").removeClass("lastalbum");
			    	$("#photo").scrollTop(top);
			    }

			    //$("#tabs-clubs-wrapper .tabs a
			    if ($toPage.attr("id") == "clubs") {
			    	var activeTab = $("#tabs-clubs-wrapper .tabs a.active").attr("href").replace("#", "")*1;
			    	//$("#title-page").append(activeTab);
			    	if (activeTab == 1) {
			    		var top = $("#club-news").parent().data("scroll");
			    		$("#club-news").parent().scrollTop(top);
			    	}

			    	if (activeTab == 3) {
			    		var top = $("#club-photoalbum").parent().data("scroll");
			    		$("#club-photoalbum").parent().scrollTop(top);
			    	}
			    }

			    if ($toPage.attr("id") == "ls") {
			    	var activeTab = $("#ls-tabs a.active").attr("href").replace("#", "")*1;
			    	
			    	if (activeTab == 2) {
			    		var top = $("#ls-news-scroll").data("scroll");
			    		//lsNewsScroll.refresh();
			    		$("#ls-news-scroll").scrollTop(top);
			    	}

			    }

			}, 300);

			//удаляем полученный ID блока из массива
			backArray.pop();
			backArrayGlobal.pop();
			if (backArray.length > 0) {

			} else {
				$("#back").hide();
				$("#swipe-nav").removeClass("hide");
				$("#nav-button").show("fade", 300);
			}
			

			
			//если возврат к разделу календаря запускаем медиа слайдер
			
			if ($toPage.attr("id") == "calendar") {
				mediaSliderControl("start"); 
				if ($mediaSlider.slides.length == 0) {
					mediaSliderControl();
				}
				//$calendarSlider.reInit();
			} 
			//alert($toPage.attr("id"));
			//если возврат к разделу лидеры переинициализация раздела лидеры
			

			if ($toPage.attr("id") == "leaders") {
				$("#tabs-leaders-wrap").addClass("show");
			} else {
				$("#leaders").addClass("load");
				$("#tabs-leaders-wrap").removeClass("show");
			}

			//если переход на страницу видео
			if ($toPage.attr("id") == "video") {
			   	$("#tabs-video-wrapper").addClass("show");
			    //если слайды с видео пустые
			    //загружаем видео из массива
			    if (!$("#video-best .video").length) {
			    	getVideo();
			    }

			     //получаем активную вкладку
				var type = $("#tabs-video-wrapper a.active").attr("href").replace("#","")*1;
				//получаем блок в который будем добавлять прелоадер и видео
				if (type == 1) {
					var $videoLoadBlock = $("#video-review").parent();
					videoReviewPageLoad = 1;
					if (!$videoLoadBlock.find(".video").length) {
						loadTabVideo(type, "");	
					}
				} else if (type == 2) {
					var $videoLoadBlock = $("#video-moments").parent();
					videoMomentsPageLoad = 1;
					if (!$videoLoadBlock.find(".video").length) {
						loadTabVideo(type, "");	
					}
				} else if (type == 3) {
					var $videoLoadBlock = $("#video-story").parent();
					videoStoryPageLoad = 1;
					if (!$videoLoadBlock.find(".video").length) {
						loadTabVideo(type, "");	
					}
				} 
			   
			} else {
			    $("#tabs-video-wrapper").removeClass("show");
			}

			//если переход на страницу клубов показываем меню
			if ($toPage.attr("id") == "clubs") {
			   	setTimeout(function(){
			   		if ($("#clubs-nav .clubs a.active").length) {
			   			$("#club-header").addClass("show");
			   			$("#title-page").addClass("clubs");
			   		} else {
			   			$("#clubs-nav").show();
			   			$("#club-header").removeClass("show");

			   			$("#clubs, #clubs .clubs-slide, #tabs-clubs-wrapper").addClass("load");
			   		}
			   	}, 300);
			   	
			} else {
				$("#club-header").removeClass("show");
				$("#title-page").removeClass("clubs");
			}

			//если переход на страницу протокола матча
			if ($toPage.attr("id") == "report-match") {
			   	setTimeout(function(){
			   		$("#report-match-header").addClass("show");
			   	}, 300);
			}
			
			//если переход на страницу РФПЛ
			if ($toPage.attr("id") == "rfpl") {
				//если форма отправлялась - показываем ее заново при переходе на раздел
				if ( $("#rfpl .success:visible").length) {
					$("#rfpl .success").hide();
					$("#rfpl .form").show();
				}
			}

			//если переход на страницу Лиги Ставок
			if ($toPage.attr("id") == "ls") {
			   	setTimeout(function(){
			   		$("#ls-header").addClass("show");
			   	}, 300);
			}


		}

		//переход на внутреннюю
		if (action == "next") {

			$("#nav-button").hide();
			$("#back").show("fade", 300);
			$("#swipe-nav").addClass("hide");
			$fromPage.addClass("transition left");
			//если переход на внутреннюю с матча добавляем эффект к хедеру
			if ($fromPage.attr("id") == "report-match") {
				$("#report-match-header").addClass("transition left");
				setTimeout(function(){
					$("#report-match-header").removeClass("show transition left");
				}, 300);
			}

			//если переход на внутреннюю с лиги ставок
			if ($fromPage.attr("id") == "ls") {
				$("#ls-header").addClass("transition left");
				setTimeout(function(){
					$("#ls-header").removeClass("show transition left");
				}, 300);
			}

			//если переход на внутреннюю с матча добавляем эффект к хедеру
			if ($fromPage.attr("id") == "leaders") {
				$("#tabs-leaders-wrap").addClass("transition left");
				setTimeout(function(){
					$("#tabs-leaders-wrap").removeClass("show transition left");
				}, 300);
			}
			//если переход на внутреннюю с матча добавляем эффект к хедеру
			if ($fromPage.attr("id") == "clubs") {

				$("#club-header").addClass("transition left");
				setTimeout(function(){
					$("#club-header").removeClass("show transition left");
				}, 300);
			}

			setTimeout(function(){
			   	$toPage.removeClass("transition left").addClass("current");
			   	$fromPage.removeClass("current transition left");
			   	$("#noconnect").hide();
			}, 300);
			


			//переход к лидерам
			if ($toPage.attr("id") == "leaders") {
				$("#tabs-leaders-wrap").addClass("show");
			}

			backArray.push($fromPage.attr("id"));

			backArrayGlobal.push($fromPage.attr("id"));
			
			//если переход с календаря останавливаем медиа слайдер
			if ($fromPage.attr("id") == "calendar") {
			   	mediaSliderControl("stop");
			} 
			//alert($toPage.attr("id"));
			//если переход на страницу видео
			if ($toPage.attr("id") == "video") {
			   	$("#tabs-video-wrapper").addClass("show");
			    //если слайды с видео пустые
			    //добавляем видео из массива
			    if (!$("#video-best .video").length) {
			    	getVideo();
			    }

			     //получаем активную вкладку
				var type = $("#tabs-video-wrapper a.active").attr("href").replace("#","")*1;
				//получаем блок в который будем добавлять прелоадер и видео
				if (type == 1) {
					var $videoLoadBlock = $("#video-review").parent();
					videoReviewPageLoad = 1;
					if (!$videoLoadBlock.find(".video").length) {
						loadTabVideo(type, "");	
					}
				} else if (type == 2) {
					var $videoLoadBlock = $("#video-moments").parent();
					videoMomentsPageLoad = 1;
					if (!$videoLoadBlock.find(".video").length) {
						loadTabVideo(type, "");	
					}
				} else if (type == 3) {
					var $videoLoadBlock = $("#video-story").parent();
					videoStoryPageLoad = 1;
					if (!$videoLoadBlock.find(".video").length) {
						loadTabVideo(type, "");	
					}
				} 
			   
			} else {
			    $("#tabs-video-wrapper").removeClass("show");
			}

			//если переход на страницу клубов показываем меню
			if ($toPage.attr("id") == "clubs") {
			   	$("#clubs-nav").show();
			   	$("#club-header").removeClass("show");

			   	$("#clubs, #clubs .clubs-slide, #tabs-clubs-wrapper").addClass("load");
			} else {
				$("#clubs-nav").hide();
				$("#title-page").removeClass("clubs");
				$("#clubs, #tabs-clubs-wrapper").removeClass("load");
			}

			//если переход на страницу протокола матча
			//$("#debug").html($("#report-match-header").attr("class"));
			//alert($toPage.attr("id"));
			if ($toPage.attr("id") == "report-match") {
			   	$("#report-match-header").addClass("show");
			   	//alert($("#report-match-header").attr("class"));
			   	$("#tabs-report-wrapper, #report-match").addClass("load");
			}

			//если переход на страницу Лиги ставок
			if ($toPage.attr("id") == "ls") {
			   	$("#ls-header").addClass("show");
			}

			//если переход на страницу РФПЛ
			if ($toPage.attr("id") == "rfpl") {
				//если форма отправлялась - показываем ее заново при переходе на раздел
				if ( $("#rfpl .success:visible").length) {
					$("#rfpl .success").hide();
					$("#rfpl .form").show();
				}

			}
			

		}		

	}

	//фукнция перехода между разделами
	function page(toPage) {	
		var $toPage = $(toPage);
		var $fromPage = $("#pages .current");
		var $pages = $("#pages");
	   	$pages.attr("style", "");

	   	if ($toPage.data("refresh") == true) {
	   		$("#refresh").show();
	   	} else {
	   		$("#refresh").hide();
	   	}

		if ($pages.hasClass("load")) {
			return;
		} else {
			//скрываем главное меню
			   
			$fromPage.removeClass("current");
			$toPage.addClass("current");

			$("#noconnect").hide();

			backArrayGlobal.push($fromPage.attr("id"));
			//добавляем title страницы
			$("#title-page").html($toPage.data("title")+"<i></i>");
			//если переход на главную страницу запускаем активный медиа слайдер
			if (toPage == "#calendar") {
			   	mediaSliderControl("start");
				$calendarSlider.resizeFix();
				if ($mediaSlider.slides.length == 0) {
					mediaSliderControl();
				}
			} else {
			    mediaSliderControl("stop");
			} 

			//если переход на страницу лидеры
			if (toPage == "#leaders") {
				$("#leaders").addClass("load");
			   	$leadersSlider.reInit(); 
			   	$("#tabs-leaders-wrap").addClass("show");
			   	getPlayersForward();
			   	getPlayersFoul();

			} else {
				$("#tabs-leaders-wrap").removeClass("show");
			}

			//если переход на страницу новостей и блок с новостями пустой 
			if (toPage == "#news" && !$("#news-list .main-news").length) {
			   	getNews("");
			}

			//если переход на страницу видео
			if (toPage == "#video") {
			   	$("#tabs-video-wrapper").addClass("show");
			    //если слайды с видео пустые
			    if (!$("#video-best .video").length) {
			    	getVideo();
			    }

			    //получаем активную вкладку
				var type = $("#tabs-video-wrapper a.active").attr("href").replace("#","")*1;
				//получаем блок в который будем добавлять прелоадер и видео
				if (type == 1) {
					var $videoLoadBlock = $("#video-review").parent();
					videoReviewPageLoad = 1;
					if (!$videoLoadBlock.find(".video").length) {
						loadTabVideo(type, "");	
					}
				} else if (type == 2) {
					var $videoLoadBlock = $("#video-moments").parent();
					videoMomentsPageLoad = 1;
					if (!$videoLoadBlock.find(".video").length) {
						loadTabVideo(type, "");	
					}
				} else if (type == 3) {
					var $videoLoadBlock = $("#video-story").parent();
					videoStoryPageLoad = 1;
					if (!$videoLoadBlock.find(".video").length) {
						loadTabVideo(type, "");	
					}
				} 

			} else {
			    $("#tabs-video-wrapper").removeClass("show");
			}
			
			//если переход на страницу фото и блок с фотоальбомами пустой
			if (toPage == "#photo" && !$("#photoalbum-list .photo").length) {
			   	getPhoto("");
			}

			//если переход на страницу клубов показываем меню
			if (toPage == "#clubs") {
			   	$("#clubs-nav").show();
			   	$("#club-header").removeClass("show");
			   	//добавляем прелоадер ко всем вкладкам клуба
			   	$("#clubs .clubs-slide, #tabs-clubs-wrapper").addClass("load");
			   	//$("#title-page").html($(window).width());
			} else {
				$("#clubs-nav").hide();
				$("#club-header").removeClass("show");
				$("#title-page").removeClass("clubs");
			}

			//если переход на страницу протокола матча
			if (toPage == "#report-match") {
			   	$("#report-match-header").addClass("show");
			   	$("#tabs-report-wrapper, #report-match").addClass("load");
			} else {
			   	$("#report-match-header").removeClass("show");
			}

			//переход на турнирную таблицу
			if (toPage == "#tournament-table") {
				getTournamentTable("");
			}

			//если переход на страницу РФПЛ
			if (toPage == "#rfpl") {
				//если форма отправлялась - показываем ее заново при переходе на раздел
				if ( $("#rfpl .success:visible").length) {
					$("#rfpl .success").hide();
					$("#rfpl .form").show();
				}

			}

			//если переход на страницу лиги ставок
			if (toPage == "#ls") {
			   	$("#ls-header").addClass("show");
			   	$lsSlider.reInit(); 
			   	getLSline();
			   	/*
			   	setTimeout(function(){
					lsLineScroll.refresh();
			    	lsVideoScroll.refresh();
			    	lsNewsScroll.refresh();
			    	lsOfficialScroll.refresh();
			   	}, 300);
				*/
			   	
			} else {
			   	$("#ls-header").removeClass("show");
			}

			
		}
	}

	//хак для андроид при повторной подгрузке контента
	function checkScrollAndroid() {
		//var heightContent = $("#pages > .current > div").height();
		//$("#pages .current > d").css("height", heightContent);
		//$("#pages").css("height", heightContent);
		
	}

	//флаги и номера страниц для подгрузки при скроле вкладок клубов
	var clubPhotoPageLoad = 1,
		clubPhotoPageLoadFlag = 0,
		clubVideoPageLoad = 1,
		clubVideoPageLoadFlag = 0,
		clubNewsPageLoad = 1,
		clubNewsPageLoadFlag = 0;


	//функция формирования HTML календаря клубов
	function parseClubCalendar(res) {

		if (res != "noconnect") {
			//var countNews = Object.keys(res.matches).length;
			var clubCalendarHTML = '';
			$.each(res.matches, function(key, val){
				var status, statusName;
				var goalsHTML = '';
				var goal1 = val.goal1;
				var goal2 = val.goal2;

				if (val.status == 0) {
					status = "preview";
					statusName = "не начался";
					goal1 = "-";
					goal2 = "-";
				} else if (val.status == 1) {
					status = "online";
					statusName = "онлайн";
				} else if (val.status == 2) {
					status = "final";
					statusName = "завершен";

					//голы для зевершенных матчей только
					if (val.gols1 != "" || val.gols2 != "") {
						if (val.gols1 == null) {
							val.gols1 = "";
						}
						if (val.gols2 == null) {
							val.gols2 = "";
						}
						goalsHTML = '<div class="goals-wrap"><div class="goals"><div class="home">'+val.gols1+'</div><div class="away">'+val.gols2+'</div><div class="border"></div><div class="clr"></div></div></div>';
					}
				}

				var clubHomeID = val.club1;
				var clubAwayID = val.club2;
				//формируем доп.блок для матчей превью и онлайн
				var previewHTML = '';
				if (val.status == 0 || val.status == 1) {
					if (val.stName != "" || val.wT != "") {
						previewHTML += '<div class="preview-info"><table cellspacing="1"><tr><td>';
						if (val.stName != "") {
							previewHTML+= '<p class="stadium">'+val.stName+'</p><p class="city">'+val.stCity+'</p>';
						}
						previewHTML += '</td><td>';

						previewHTML += '';

						if (val.wT != "") {
							var textWeather = "";
							if (val.wN != "") {
								textWeather = val.wN+" | ";
							}
							previewHTML += '<p class="weather">'+val.wT+'&deg; <img src="./img/weather/'+val.wI+'.png"></p><p class="humidity">'+textWeather+' <img src="./img/ico-humidity.png"> '+val.wH+'%</p>';
						}
						previewHTML += '</td></tr></table></div>'
							
					}
				}

				var linksHTML = '';
				if (val.sostav > 0 || val.protocol > 0 || val.video > 0 || val.photo > 0) {
					linksHTML = '<div class="link-wrap"><div data-squads="'+val.sostav+'" data-report="'+val.protocol+'" data-video="'+val.video+'" data-photo="'+val.photo+'" data-id="'+val.id+'" class="links">';

					if (val.sostav > 0) {
						linksHTML += '<span data-tab="squads">Составы<i></i></span>';
					}
					if (val.protocol > 0) {
						linksHTML += '<span data-tab="report">События<i></i></span>';
					}
					if (val.video > 0) {
						linksHTML += '<span data-tab="video">Видео | '+val.video+'<i></i></span>';
					}
					if (val.photo > 0) {
						linksHTML += '<span data-tab="photo">Фото | '+val.photo+'<i></i></span>';
					}

					linksHTML += '</div></div>';		

				}

				//коэффициэнты лиги ставок для превью матча
				var lsHTML = "";
				var k1 = val.outcome_1;
				var k2 = val.outcome_2;
				var k3 = val.outcome_3;
				if (status == "preview" && k1 != null) {
					//lsHTML = '<div class="ls"><div class="kof p1"><p class="eventname">победа</p><p class="round"></p><p class="value">'+k1+'</p></div><div class="kof x"><p class="eventname">ничья</p><p class="round"></p><p class="value">'+k2+'</p></div><div class="kof p2"><p class="eventname">победа</p><p class="round"></p><p class="value">'+k3+'</p></div></div>';
					lsHTML = '<div class="ls"><p class="ls-top">Кто победит, как Вы считаете?</p><img src="./img/ls-logo-match.png" alt="" class="logo"><div class="kof-wrapper"><div class="kof p1"><p class="round"></p><p class="value">'+k1+'</p></div><div class="kof x"><p class="eventname">ничья</p><p class="round"></p><p class="value">'+k2+'</p></div><div class="kof p2"><p class="round"></p><p class="value">'+k3+'</p></div><i class="left-line"></i><i class="right-line"></i></div><p class="ls-bottom">Считайте и побеждайте!</p></div>';
				}

				clubCalendarHTML += '<a href="#" class="match '+status+' "><div class="main"><p class="text">'+val.date+' <span>'+statusName+'</span></p><div class="info"><div class="club home"><img src="./img/logos/small/'+val.club1+'.png"><p>'+clubsData["clubs"]["clubs"+val.club1]["name"]+'</p></div><div class="score"><p class="home">'+goal1+'</p><p class="away">'+goal2+'</p><i></i></div><div class="club away"><img src="./img/logos/small/'+val.club2+'.png"><p>'+clubsData["clubs"]["clubs"+val.club2]["name"]+'</p></div></div></div><div class="add-info"> '+goalsHTML+' '+previewHTML+' '+lsHTML+' '+linksHTML+'</div></a>';

			});

			$("#club-match-calendar").html(clubCalendarHTML);

			$("#club-match-calendar").removeClass("load");
			$("#clubs").removeClass("load");
			$("#tabs-clubs-wrapper").removeClass("load");
			$clubTabsSlider.swipeTo(0, 0, false);
			$clubTabsSlider.reInit();
			//$("#debug").html($("#club-match-calendar .match").length);
			var scrollMatch = $("#club-match-calendar .preview").first().position().top;
			if (scrollMatch > $("#club-match-calendar").height()) {
				scrollMatch = $("#club-match-calendar").height();
			}
			$("#club-match-calendar").parent().scrollTop(scrollMatch);
			//вызываем фукнции получения данных для остальных вкладок
			//по ID клуба
			/*
			var id = $("#clubs-nav .clubs a.active").attr("href").replace("#","");
			getClubNews(id, "");
			getClubVideo(id, "");
			getClubPhoto(id, "");
			getClubPlayers(id);
			getClubStaff(id);
			getClubAwards(id);
			*/
		} else {
			//если интернета нет
			$("#noconnect").show();
			$("#club-match-calendar").removeClass("load");
			$("#clubs").removeClass("load");
			$("#tabs-clubs-wrapper").removeClass("load");

			$clubTabsSlider.swipeTo(0, 0, false);
			$clubTabsSlider.reInit();
			$("#tabs-clubs-wrapper").scrollLeft(0);
			$("#refresh-repeat").data("action", "clubcalendar");
		}
	}
	//фукнция получения календаря клубов
	function getClubCalendar(id) {
		//http://rfpl.org/api/v3/matches.php?action=getMatchesClubs&club=1
		getDataJSON('temp.json', 'matches.php', {"action":"getMatchesClubs", "club": id}, parseClubCalendar, 1);
	}
	
	//фукцнии разбора новостей клуба при загрузке и подгрузке
	function parseClubNews(res) {

		if (res != "noconnect") {
			var newsHTML = '';
			//alert(res.news);
			if (res.news != null) {
				$.each(res.news, function(key, val){
					var descHtml = "";
					if (val.short_s != "") {
						descHtml = '<p class="desc">'+val.short_s+'</p>';
					}
					var rfplHtml = "";
					if (val.type == "18") {
						rfplHtml = '<span class="rfpl">РФПЛ</span>';
					}
					newsHTML += '<a href="#" class="news" data-id="'+val.id+'"><figure><img src="'+val.img+'"></figure><p><span class="date">'+val.date+'</span>'+rfplHtml+'</p><p class="name">'+val.name+'</p>'+descHtml+'</a>';
				});
				$("#club-news").html(newsHTML).removeClass("load");
			} else {
				//$("#club-news").html(newsHTML).removeClass("load");
			}

		} else {
			//если интернета нет
			$("#noconnect").show();
			//$("#club-news").removeClass("load");
			$("#refresh-repeat").data("action", "clubnews");
		}

	}
	function parseClubScrollNews(res) {
		if (res != "noconnect") {
			var newsHTML = '';
			if (res.news != null) {
				$.each(res.news, function(key, val){
					var descHtml = "";
					if (val.short_s != "") {
						descHtml = '<p class="desc">'+val.short_s+'</p>';
					}
					var rfplHtml = "";
					if (val.type == "18") {
						rfplHtml = '<span class="rfpl">РФПЛ</span>';
					}
					newsHTML += '<a href="#" class="news" data-id="'+val.id+'"><figure><img src="'+val.img+'"></figure><p><span class="date">'+val.date+'</span>'+rfplHtml+'</p><p class="name">'+val.name+'</p>'+descHtml+'</a>';
				});
				$("#club-news").find(".preloader").hide("fade", 250, function(){ $(this).remove(); });
				$("#club-news").append(newsHTML);
				clubNewsPageLoad++;
				clubNewsPageLoadFlag = 0;
			} else {
				setTimeout(function(){
					$("#club-news").find(".preloader").remove();
					clubNewsPageLoadFlag = 0;
				}, 250);
			}
		} else {
			//если интернета нет
			//удаляем прелоадер
			$("#club-news").find(".preloader").remove();
			//добавляем сообщение об отсутствии интернета
			$("#club-news").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="clubnewsscroll" href="#"><i></i> Повторить попытку</a></div>');		

		}
	}
	//Фукнции получения данных по вкладкам клубов
	function getClubNews(id, page) {
		//http://rfpl.org/api/v3/news.php?action=getNewsClubs&club=1&page=1
		if (page != "") { 
			clubNewsPageLoadFlag = 1;
			$("#club-news").append('<div class="preloader"></div>');
			
			getDataJSON('temp.json', 'news.php', {"action":"getNewsClubs", "club": id, "page" : page}, parseClubScrollNews, 1);
		} else {
			//обнуляем страницу загрузки
			clubNewsPageLoad = 1;
			getDataJSON('temp.json', 'news.php', {"action":"getNewsClubs", "club": id}, parseClubNews, 1);
		}
	}

	//фукцнии разбора видео клуба при загрузке и подгрузке
	function parseClubVideo(res) {

		if (res != "noconnect") {
			var videoHTML = '';
			if (res.video != null) {
				$.each(res.video, function(key, val){
					videoHTML += '<a class="video" href="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" target="_blank"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.time+'<i></i> </span></span><span class="bg"></span></a>';
				});

				$("#club-video .video-list").html(videoHTML);
				$("#club-video").removeClass("load");
			
			} else {

			}	

		} else {
			//если интернета нет
			$("#noconnect").show();
			//$("#club-video").removeClass("load");
			$("#refresh-repeat").data("action", "clubvideo");
		}	
					
	}
	function parseClubScrollVideo(res) {
		if (res != "noconnect") {
			var videoHTML = '';
			if (res.video != null) {
				$.each(res.video, function(key, val){
					videoHTML += '<a class="video" href="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" target="_blank"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.time+'<i></i> </span></span><span class="bg"></span></a>';
				});

				$("#club-video .video-list").find(".preloader").hide("fade", 250, function(){ $(this).remove(); });
				$("#club-video .video-list").append(videoHTML);
				clubVideoPageLoad++;
				clubVideoPageLoadFlag = 0;
					
			} else {	
				setTimeout(function(){
					$("#club-video .video-list").find(".preloader").remove();
					clubVideoPageLoadFlag = 0;
				}, 250);
			}
		} else {
			//если интернета нет
			//удаляем прелоадер
			$("#club-video .video-list").find(".preloader").remove();
			//добавляем сообщение об отсутствии интернета
			$("#club-video .video-list").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="clubvideoscroll" href="#"><i></i> Повторить попытку</a></div>');		

		}
	}
	function getClubVideo(id, page) {
		//http://rfpl.org/api/v3/video.php?action=getVideoClubs&club=1&page=0		
		if (page != "") { 
			clubVideoPageLoadFlag = 1;
			$("#club-video .video-list").append('<div class="preloader"></div>');
			getDataJSON('temp.json', 'video.php', {"action":"getVideoClubs", "club": id, "page" : page}, parseClubScrollVideo, 1);
		} else {
			//обнуляем страницу загрузки
			clubVideoPageLoad = 1;
			getDataJSON('temp.json', 'video.php', {"action":"getVideoClubs", "club": id}, parseClubVideo, 1);
		}	
	}

	//фукцнии разбора фото клуба при загрузке и подгрузке
	function parseClubPhoto(res) {
		if (res != "noconnect") {
			var photoHTML = '';
			if (res.photo != null) {
				$.each(res.photo, function(key, val){
					photoHTML += '<a data-id="'+val.id+'" href="#" class="photo"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.count+' фото<i></i> </span></span><span class="bg"></span></a>';
				});
					
				$("#club-photoalbum .photo-list").html(photoHTML);
				$("#club-photoalbum").removeClass("load");
						
			} else {
					
			}
		} else {
			//если интернета нет
			$("#noconnect").show();
			//$("#club-photoalbum").removeClass("load");
			$("#refresh-repeat").data("action", "clubphoto");
		}
	}
	function parseClubScrollPhoto(res) {
		if (res != "noconnect") {
			var photoHTML = '';
			if (res.photo != null) {
				$.each(res.photo, function(key, val){
					photoHTML += '<a data-id="'+val.id+'" href="#" class="photo"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.count+' фото<i></i> </span></span><span class="bg"></span></a>';
				});
		
				$("#club-photoalbum .photo-list").find(".preloader").hide("fade", 250, function(){ $(this).remove(); });
				$("#club-photoalbum .photo-list").append(photoHTML);
				clubPhotoPageLoad++;
				clubPhotoPageLoadFlag = 0;
				
			} else {
					
				setTimeout(function(){
					$("#club-photoalbum .photo-list").find(".preloader").remove();
					clubPhotoPageLoadFlag = 0;
				}, 250);
			}

		} else {
			//если интернета нет
			//удаляем прелоадер
			$("#club-photoalbum .photo-list").find(".preloader").remove();
			//добавляем сообщение об отсутствии интернета
			$("#club-photoalbum .photo-list").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="clubphotoscroll" href="#"><i></i> Повторить попытку</a></div>');		

		}

	}

	function getClubPhoto(id, page) {
		//http://rfpl.org/api/v3/photo.php?action=getPhotoClubs&club=1&page=0
		var photoHTML = '';
		if (page != "") { 
			clubPhotoPageLoadFlag = 1;
			$("#club-photoalbum .photo-list").append('<div class="preloader"></div>');
			getDataJSON('temp.json', 'photo.php', {"action":"getPhotoClubs", "club": id, "page" : page}, parseClubScrollPhoto, 1);
		} else {
			//обнуляем страницу загрузки
			clubPhotoPageLoad = 1;
			getDataJSON('temp.json', 'photo.php', {"action":"getPhotoClubs", "club": id}, parseClubPhoto, 1);
		}			
	}

	//фукцния разбора игроков клуба при загрузке
	function parseClubPlayers(res) {
		if (res != "noconnect") { 
			var clubForwardsHTML = '',
				clubMiddfieldersHTML = '',
				clubDefendersHTML = '',
				clubGoalkeepersHTML = '',
				playerHTML = '';
			if (res.players != null) {
				$.each(res.players, function(key, val){
						
					playerHTML = '<a href="#'+val.id+'" class="player"><img src="'+val.img+'"><div class="num">'+val.nomer+'</div><div class="flag"><img src="./img/flag/'+val.citizen+'.gif"></div><div class="text"><div class="info"><p class="name">'+val.name+'</p><p class="country">'+val.country+'</p></div></div></a>';
						
					if (val.amplua == 0) {
						clubGoalkeepersHTML += playerHTML;
					}
					if (val.amplua == 1) {
						clubDefendersHTML += playerHTML;
					}
					if (val.amplua == 2) {
						clubMiddfieldersHTML += playerHTML;
					}
					if (val.amplua == 3) {
						clubForwardsHTML += playerHTML;
					}
				});

				$("#club-forwards").html(clubForwardsHTML);
				$("#club-middfielders").html(clubMiddfieldersHTML);
				$("#club-defenders").html(clubDefendersHTML);
				$("#club-goalkeepers").html(clubGoalkeepersHTML);

				$("#club-players").removeClass("load");
			}	
		} else {
			//если интернета нет
			$("#noconnect").show();
			//$("#club-players").removeClass("load");
			$("#refresh-repeat").data("action", "clubplayers");
		}
	}
	function getClubPlayers(id) {
		//http://rfpl.org/api/v3/players.php?action=getPlayersClubs&club=1
		getDataJSON('temp.json', 'players.php', {"action":"getPlayersClubs", "club": id}, parseClubPlayers, 1);
	}

	//фукцния разбора сотрудников клуба при загрузке
	function parseClubStaff(res) {
		if (res != "noconnect") {
			var clubDirectionStaffHTML = '',
				clubMainStaffHTML = '',
				clubYouthStaffHTML = '',
				peopleHTML = '';
			if (res.stuff != null) {
				$.each(res.stuff, function(key, val){
					peopleHTML = '<div class="people"><p class="status">'+val.doljnost+'</p><p class="name">'+val.name+'</p></div>';
					if (val.sostav == 1) {
						clubDirectionStaffHTML += peopleHTML;
					}	
					if (val.sostav == 2) {
						clubMainStaffHTML += peopleHTML;
					}
					if (val.sostav == 3) {
						clubYouthStaffHTML += peopleHTML;
					}
				});

				$("#direction-staff").html(clubDirectionStaffHTML);
				$("#main-staff").html(clubMainStaffHTML);
				$("#youth-staff").html(clubYouthStaffHTML);
							
				$("#club-staff").removeClass("load");

			}
		} else {
			//если интернета нет
			$("#noconnect").show();
			//$("#club-staff").removeClass("load");
			$("#refresh-repeat").data("action", "clubstaff");
		}
	}
	function getClubStaff(id) {
		//http://rfpl.org/api/v3/stuff.php?action=getStuffClubs&clubs=1
		getDataJSON('temp.json', 'stuff.php', {"action":"getStuffClubs", "clubs": id}, parseClubStaff, 1);
	}

	//фукцния разбора достижений клуба при загрузке
	function parseClubAwards(res) {
		if (res != "noconnect") {
			//alert(res.records);
			if (res.records != null) {
				$("#club-awards").html(res.records).removeClass("load");	
			} else {
				$("#club-awards").html('<p class="awards">Достижения отсутствуют</p>').removeClass("load");		
			}
		} else {
			//если интернета нет
			$("#noconnect").show();
			//$("#club-awards").removeClass("load");
			$("#refresh-repeat").data("action", "clubawards");
		}
	}
	function getClubAwards(id) {
		//http://rfpl.org/api/v3/clubs.php?action=getClubsRecords&clubs=1
		getDataJSON('temp.json', 'clubs.php', {"action":"getClubsRecords", "clubs": id}, parseClubAwards, 1);
	}

	//фукнция инициализации календаря игр
	//передаем текущий тур
	var baseSlideHeight; //базовая высота слайдера для восстановления при переключении слайдов
	//var check = false;
	function calendarInit(tour) {
		var type = "";
		$calendarSlider = new Swiper('#calendar-slider',{
		    slidesPerView: 1,
		    createPagination: false,
		    loop: false,
		    calculateHeight: true,
		    autoplay: false,
		    initialSlide: tour,

		   	speed: 300,
		   	followFinger: false,
		   	noSwiping: true,
		   	simulateTouch: false,
		   	resizeReInit: false,

		   	longSwipesRatio: 200,

		   	onInit: function(slider) {
		   		var activeSlide = $("#tabs-video-wrapper a.active").attr("href").replace("#", "");
		    	var $activeSlide = $(slider.slides[activeSlide]);
		    	var heightContent = $(slider.activeSlide()).height();
		    	$("#calendar-slider, #calendar-slider .swiper-wrapper, #calendar-slider .swiper-slide").css("height", heightContent);
		   	},
		    onSwiperCreated: function(slider){
		    	
		    	if (slider.activeIndex+1 == slider.slides.length) {
		      		$("#next-tour").hide();
		      	} else {
		      		$("#next-tour").show();
		      	}
		      	if (slider.activeIndex == 0) {
		      		$("#prev-tour").hide();
		      	} else {
		      		$("#prev-tour").show();
		      	}
		      	var numTour = slider.activeIndex+1;
		      	$("#name-tour").html(numTour+"-й тур");

		      	var heightContent = $(slider.activeSlide()).height();
		      	baseSlideHeight = heightContent;
		      	$("#calendar-slider, #calendar-slider .swiper-wrapper, #calendar-slider .swiper-slide").css("height", heightContent);



		      	getMatchSupercup();

		      	//получаем матчи для туров 
		      	//при инициализации слайдера или переинициализации слайдера
		   		//очищаем все слайды и формируем матчи для текущего тура и -2 +2 туров
		      	getMatchTourInit(slider.activeIndex+1, slider);

		    },
		    onSlideChangeStart: function(slider){
		    	
		    	$(".match.active").removeClass("active");
		      	if (slider.activeIndex+1 == slider.slides.length) {
		      		$("#next-tour").hide();
		      	} else {
		      		$("#next-tour").show();
		      	}
		      	if (slider.activeIndex == 0) {
		      		$("#prev-tour").hide();
		      	} else {
		      		$("#prev-tour").show();
		      	}
		      	var numTour = slider.activeIndex+1;
		      	$("#name-tour").html(numTour+"-й тур");
		    }, 
		    onSlideChangeEnd: function(slider){
		    	/*
		    	if (type == "next") {
		    		//при переключении на следующий слайд подгружаем слайд+2 от текущего и очищаем слайд -3 от текущего
			    	if (slider.activeIndex+1 <= slider.slides.length-1) {
			    		getMatchTour(slider.activeIndex+2, slider);
			    	}	
		    	} 
		    	if (type == "prev") {
		    		//при переключении на предыдущий слайд подгружаем слайд-2 от текущего и очищаем слайд +3 от текущего
			    	if (slider.activeIndex-1 >= 0) {
			    		var newSlideIndex = slider.slides[slider.activeIndex-2];
			    		getMatchTour(slider.activeIndex-1, slider);
			    	}
		    	}*/
		    	$("#calendar-slider .swiper-slide").addClass("load").html("");
		    	
		    	//проверка если текущий слайд пустой
		    	//то подгружаем в него матчи
		    	var $currentSlide = $(slider.slides[slider.activeIndex]);
		    	
		    	if (!$currentSlide.find(".match").length) {
		    		getMatchTour(slider.activeIndex+1, slider);
		    	}
		    	//slider.reInit();
		    },
		    onSlideNext: function(slider) {
		    	type = "next";
		    	if (slider.activeIndex-2 >= 0) {
			    	var clearSlideIndex = slider.slides[slider.activeIndex-2];
			    	var $clearSlide = $(clearSlideIndex);
			    	$clearSlide.addClass("load").html("");
			    }
		    		
		    },
		    onSlidePrev: function(slider) {
		    	type="prev";
		    	if (slider.activeIndex+2 <= slider.slides.length-1) {
			    	var clearSlideIndex = slider.slides[slider.activeIndex+2];
			    	var $clearSlide = $(clearSlideIndex);
			    	$clearSlide.addClass("load").html("");
			    }
		    }
		});
		
	}
	//фукнция формирования матчей текущего тура и -2 +2 от текущего тура
	function getMatchTourInit(tour, slider) {
		//очищаем все слайды и добавляем к ним прелоадеры
		$("#calendar-wrapper").addClass("load");
		$("#calendar-slider .tour-wrapper").addClass("load").html("");
		//получаем данные из JSON массива для текущего тура
		//а потом для -2+2 от текущего
		var curTourObj = calendarJSON["matches"]["tur"+tour];
		
		var i = 0;
		getMatchTour(tour, slider);
		//getMatchTour(tour-1, slider);
		//getMatchTour(tour-2, slider);
		//getMatchTour(tour+1, slider);
		//getMatchTour(tour+2, slider);

	}
	//получение данных только по 1 туру
	function getMatchTour(tour, slider) {
		var curTourObj = calendarJSON["matches"]["tur"+tour];

		var i = 0;
		var curTourHTML = "";
		$.each(curTourObj, function(key, val){
			var status, statusName;
			var goalsHTML = '';
			var goal1 = val.goal1;
			var goal2 = val.goal2;
			if (val.status == 0) {
				status = "preview";
				statusName = "не начался";
				goal1 = "-";
				goal2 = "-";
			} else if (val.status == 1) {
				status = "online";
				statusName = "онлайн";
			} else if (val.status == 2) {
				status = "final";
				statusName = "завершен";

				//голы для зевершенных матчей только
				if (val.gols1 != "" || val.gols2 != "") {
					if (val.gols1 == null) {
						val.gols1 = "";
					}
					if (val.gols2 == null) {
						val.gols2 = "";
					}
					goalsHTML = '<div class="goals-wrap"><div class="goals"><div class="home">'+val.gols1+'</div><div class="away">'+val.gols2+'</div><div class="border"></div><div class="clr"></div></div></div>';
				}

			}

			var clubHomeID = val.club1;
			var clubAwayID = val.club2;

			//формируем доп.блок для матчей превью и онлайн
			var previewHTML = '';
			if (val.status == 0 || val.status == 1) {
				if (val.stName != "" || val.wT != "") {
					previewHTML += '<div class="preview-info"><table cellspacing="1"><tr><td>';
					if (val.stName != "") {
						
						previewHTML+= '<p class="stadium">'+val.stName+'</p><p class="city">'+val.stCity+'</p>';
					}
					previewHTML += '</td><td>';

					previewHTML += '';

					if (val.wT != "") {
						var textWeather = "";
						if (val.wN != "") {
							textWeather = val.wN+" | ";
						}
						previewHTML += '<p class="weather">'+val.wT+'&deg; <img src="./img/weather/'+val.wI+'.png"></p><p class="humidity">'+textWeather+' <img src="./img/ico-humidity.png"> '+val.wH+'%</p>';
					}
					previewHTML += '</td></tr></table></div>'
					
				}
			}

			var linksHTML = '';
			if (val.sostav > 0 || val.protocol > 0 || val.video > 0 || val.photo > 0) {
				linksHTML = '<div class="link-wrap"><div data-squads="'+val.sostav+'" data-report="'+val.protocol+'" data-video="'+val.video+'" data-photo="'+val.photo+'" data-id="'+val.id+'" class="links">';

				if (val.sostav > 0) {
					linksHTML += '<span data-tab="squads">Составы<i></i></span>';
				}
				if (val.protocol > 0) {
					linksHTML += '<span data-tab="report">События<i></i></span>';
				}
				if (val.video > 0) {
					linksHTML += '<span data-tab="video">Видео | '+val.video+'<i></i></span>';
				}
				if (val.photo > 0) {
					linksHTML += '<span data-tab="photo">Фото | '+val.photo+'<i></i></span>';
				}

				linksHTML += '</div></div>';		

			}

			var addClass = "";
			if (i == 7) {
				//если последний добавляем класс матчу
				addClass = "last";
			}
			//alert("Перед КОФ ЛС");
			//коэффициэнты лиги ставок для превью матча
			var lsHTML = "";
			var k1 = val.outcome_1;
			var k2 = val.outcome_2;
			var k3 = val.outcome_3;
			if (status == "preview" && k1 != null) {
				//lsHTML = '<div class="ls"><div class="kof p1"><p class="eventname">победа</p><p class="round"></p><p class="value">'+k1+'</p></div><div class="kof x"><p class="eventname">ничья</p><p class="round"></p><p class="value">'+k2+'</p></div><div class="kof p2"><p class="eventname">победа</p><p class="round"></p><p class="value">'+k3+'</p></div></div>';
				lsHTML = '<div class="ls"><p class="ls-top">Кто победит, как Вы считаете?</p><img src="./img/ls-logo-match.png" alt="" class="logo"><div class="kof-wrapper"><div class="kof p1"><p class="round"></p><p class="value">'+k1+'</p></div><div class="kof x"><p class="eventname">ничья</p><p class="round"></p><p class="value">'+k2+'</p></div><div class="kof p2"><p class="round"></p><p class="value">'+k3+'</p></div><i class="left-line"></i><i class="right-line"></i></div><p class="ls-bottom">Считайте и побеждайте!</p></div>';
			}

			curTourHTML += '<a href="#" class="match '+addClass+' '+status+' "><div class="main"><p class="text">'+val.date+' <span>'+statusName+'</span></p><div class="info"><div class="club home"><img src="./img/logos/small/'+val.club1+'.png"><p>'+clubsData["clubs"]["clubs"+val.club1]["name"]+'</p></div><div class="score"><p class="home">'+goal1+'</p><p class="away">'+goal2+'</p><i></i></div><div class="club away"><img src="./img/logos/small/'+val.club2+'.png"><p>'+clubsData["clubs"]["clubs"+val.club2]["name"]+'</p></div></div></div><div class="add-info">'+goalsHTML+' '+previewHTML+' '+lsHTML+' '+linksHTML+'</div></a>';

			i++;

			if (i==8) {
		    	var $curSlide = $(slider.slides[tour-1]);
		    	
		    	$curSlide.html(curTourHTML).removeClass("load");

		    	$("#calendar-wrapper").removeClass("load");
		    	slider.reInit();
		    	//alert(curTourHTML);
			}

			
		});
	}

	function getMatchSupercup() {
		var statusSupercup = calendarJSON["supercup"]["status"];
		if (statusSupercup == 1) {
			$("#supercup-name").html(calendarJSON["supercup"]["text"]);
			$("#supercup-match").addClass("load").html("");
			var curTourObj = calendarJSON["supercup"]["matches"];
		
			var i = 0;
			var curTourHTML = "";

			var status, statusName;
			var goalsHTML = '';
			var goal1 = curTourObj.goal1;
			var goal2 = curTourObj.goal2;
			if (curTourObj.status == 0) {
				status = "preview";
				statusName = "не начался";
				goal1 = "-";
				goal2 = "-";
			} else if (curTourObj.status == 1) {
				status = "online";
				statusName = "онлайн";
			} else if (curTourObj.status == 2) {
				status = "final";
				statusName = "завершен";

				//голы для зевершенных матчей только
				if (curTourObj.gols1 != "" || curTourObj.gols2 != "") {
					if (curTourObj.gols1 == null) {
						curTourObj.gols1 = "";
					}
					if (curTourObj.gols2 == null) {
						curTourObj.gols2 = "";
					}
					goalsHTML = '<div class="goals-wrap"><div class="goals"><div class="home">'+curTourObj.gols1+'</div><div class="away">'+curTourObj.gols2+'</div><div class="border"></div><div class="clr"></div></div></div>';
				}

			}

			var clubHomeID = curTourObj.club1;
			var clubAwayID = curTourObj.club2;

			//формируем доп.блок для матчей превью и онлайн
			var previewHTML = '';
			if (curTourObj.status == 0 || curTourObj.status == 1) {
				if (curTourObj.stName != "" || curTourObj.wT != "") {
					previewHTML += '<div class="preview-info"><table cellspacing="1"><tr><td>';
					if (curTourObj.stName != "") {
						
						previewHTML+= '<p class="stadium">'+curTourObj.stName+'</p><p class="city">'+curTourObj.stCity+'</p>';
					}
					previewHTML += '</td><td>';

					previewHTML += '';

					if (curTourObj.wT != "") {
						var textWeather = "";
						if (curTourObj.wN != "") {
							textWeather = curTourObj.wN+" | ";
						}
						previewHTML += '<p class="weather">'+curTourObj.wT+'&deg; <img src="./img/weather/'+curTourObj.wI+'.png"></p><p class="humidity">'+textWeather+' <img src="./img/ico-humidity.png"> '+curTourObj.wH+'%</p>';
					}
					previewHTML += '</td></tr></table></div>'
					
				}
			}

			var linksHTML = '';
			if (curTourObj.sostav > 0 || curTourObj.protocol > 0 || curTourObj.video > 0 || curTourObj.photo > 0) {
				linksHTML = '<div class="link-wrap"><div data-squads="'+curTourObj.sostav+'" data-report="'+curTourObj.protocol+'" data-video="'+curTourObj.video+'" data-photo="'+curTourObj.photo+'" data-id="'+curTourObj.id+'" class="links">';

				if (curTourObj.sostav > 0) {
					linksHTML += '<span data-tab="squads">Составы<i></i></span>';
				}
				if (curTourObj.protocol > 0) {
					linksHTML += '<span data-tab="report">События<i></i></span>';
				}
				if (curTourObj.video > 0) {
					linksHTML += '<span data-tab="video">Видео | '+curTourObj.video+'<i></i></span>';
				}
				if (curTourObj.photo > 0) {
					linksHTML += '<span data-tab="photo">Фото | '+curTourObj.photo+'<i></i></span>';
				}

				linksHTML += '</div></div>';		

			}

			//коэффициэнты лиги ставок для превью матча
			var lsHTML = "";
			var k1 = curTourObj.outcome_1;
			var k2 = curTourObj.outcome_2;
			var k3 = curTourObj.outcome_3;
			if (status == "preview" && k1 != null) {
				//lsHTML = '<div class="ls"><div class="kof p1"><p class="eventname">победа</p><p class="round"></p><p class="value">'+k1+'</p></div><div class="kof x"><p class="eventname">ничья</p><p class="round"></p><p class="value">'+k2+'</p></div><div class="kof p2"><p class="eventname">победа</p><p class="round"></p><p class="value">'+k3+'</p></div></div>';
				lsHTML = '<div class="ls"><p class="ls-top">Кто победит, как Вы считаете?</p><img src="./img/ls-logo-match.png" alt="" class="logo"><div class="kof-wrapper"><div class="kof p1"><p class="round"></p><p class="value">'+k1+'</p></div><div class="kof x"><p class="eventname">ничья</p><p class="round"></p><p class="value">'+k2+'</p></div><div class="kof p2"><p class="round"></p><p class="value">'+k3+'</p></div><i class="left-line"></i><i class="right-line"></i></div><p class="ls-bottom">Считайте и побеждайте!</p></div>';
			}

			curTourHTML += '<a href="#" class="match '+status+' "><div class="main"><p class="text">'+curTourObj.date+' <span>'+statusName+'</span></p><div class="info"><div class="club home"><img src="./img/logos/small/'+curTourObj.club1+'.png"><p>'+clubsData["clubs"]["clubs"+curTourObj.club1]["name"]+'</p></div><div class="score"><p class="home">'+goal1+'</p><p class="away">'+goal2+'</p><i></i></div><div class="club away"><img src="./img/logos/small/'+curTourObj.club2+'.png"><p>'+clubsData["clubs"]["clubs"+curTourObj.club2]["name"]+'</p></div></div></div><div class="add-info">'+goalsHTML+' '+previewHTML+' '+lsHTML+' '+linksHTML+'</div></a>';

			//$("#title-page").prepend("-1-");
	    	$("#supercup-match").html(curTourHTML).removeClass("load");
	    	$("#supercup-wrapper").show();
	    	//calendarScroll.refresh();
		} else {
			$("#supercup-wrapper").hide();
		}
		

	}

	$("#supercup-match")
		.on("tap", ".match", function(){

			if ($(this).hasClass("active")) {
				
				$(this).removeClass("active");

			} else {
				
				$("#calendar-slider .match.active").removeClass("active");
				$(this).addClass("active");
				
			}
			//calendarScroll.refresh();
			return false;
		})
		.on("tap", ".match .links span", function(){

			//при переходе на страницу матча получаем 
			//ID матча, вкладки, активную вкладку и html код матча для подставновки в header
			var activeTab = $(this).data("tab");
			var $linksBlock = $(this).parent();
			//alert(activeTab);
			var idMatch = $linksBlock.data("id");
			//проверочные переменные для вкладок
			var checkSquads = $linksBlock.data("squads")*1;
			var checkReport = $linksBlock.data("report")*1;
			var checkVideo = $linksBlock.data("video")*1;
			var checkPhoto = $linksBlock.data("photo")*1;

			//html код протокола в матче
			var $activeMatch = $("#supercup-match .match.active");
			var classMatch = $activeMatch.attr("class").replace("active", "");
			var matchHtml = $activeMatch.find(".main").html();

			$("#report-match-header .tour-wrapper").html('<div class="'+classMatch+'"><div class="main">'+matchHtml+'</div></div>');

			//запускаем фукнцию получения данных по матчу
			//и формирования html кода слайдов
			//передается ID матча и активная вкладка
			//и проверочные переменные для формирования html кода вкладов
			$("#tabs-report-wrapper").addClass("load");
			
			
			pageInner("#report-match", "#calendar");
			getInfoMatch(idMatch, activeTab, checkSquads, checkReport, checkVideo, checkPhoto);

			return false;
		});


	//получение протокола матча
	function getInfoMatch(id, activeTab, checkSquads, checkReport, checkVideo, checkPhoto) {
		$("#report-match").data("id", id);

		//предварительная очистка вкладов страницы клуба
		reportMatchSliderClear();


		var $tabsReport = $("#tabs-report-wrapper .tabs");
		$tabsReport.html("");
		var i=0;
		var activeSlide = 0;
		if (checkSquads > 0) {
			var classTab1 = "";
			if (activeTab == "squads") {
				classTab1 = "active";
				activeSlide = i;
			}
			$tabsReport.append('<a href="#'+i+'" class="squadstabreport '+classTab1+'">Составы<span class="line"></span></a>');
			$reportMatchSlider.appendSlide('<div id="report-squads" class="report-slide"></div>', 'swiper-slide');

			i++;
		}
		if (checkReport > 0) {
			var classTab2 = "";
			if (activeTab == "report") {
				classTab2 = "active";
				activeSlide = i;
			}
			$tabsReport.append('<a href="#'+i+'" class="eventstabreport '+classTab2+'">События<span class="line"></span></a>');
			$reportMatchSlider.appendSlide('<div id="report-chronology" class="report-slide"></div>', 'swiper-slide');
			i++;
		}
		if (checkVideo > 0) {
			var classTab3 = "";
			if (activeTab == "video") {
				classTab3 = "active";
				activeSlide = i;

			}
			$tabsReport.append('<a href="#'+i+'" class="videotabreport '+classTab3+'">Видео | '+checkVideo+'<span class="line"></span></a>');
			$reportMatchSlider.appendSlide('<div id="report-video" class="report-slide video-wrapper"><div class="video-list"></div></div>', 'swiper-slide dark');
			i++;
		}
		if (checkPhoto > 0) {
			var classTab3 = "";
			if (activeTab == "photo") {
				classTab3 = "active ";
				activeSlide = i;
			}
			$tabsReport.append('<a href="#'+i+'" class="phototabreport '+classTab3+'">Фото | '+checkPhoto+'<span class="line"></span></a>');
			//добавляем слайд
			$reportMatchSlider.appendSlide('<div id="report-photo" class="report-slide photo-wrapper"><div class="photo-list"></div></div>', 'swiper-slide dark');
			
			i++;
		}

		//добавляем прелоадеры
		$("#tabs-report-wrapper, #report-match, #report-chronology, #report-squads, #report-video, #report-photo").addClass("load");
		//в зависимости от того какую вкладку выбрали подгружаем сначала ее информацию
		if (activeTab == "squads" || activeTab == "report") {
			$("#tabs-report-wrapper .eventstabreport, #tabs-report-wrapper .squadstabreport").addClass("full");
			getDataJSON('temp.json', 'matches.php', {"action":"getMatchesInfo", "matches": id}, getReportSquadsMatch, 1);
		}
		if (activeTab == "photo") {
			$("#tabs-report-wrapper .phototabreport").addClass("full");
			getDataJSON('temp.json', 'photo.php', {"action":"getPhotoMatch", "matches": id}, getPhotoMatch, 1);
		}
		if (activeTab == "video") {
			$("#tabs-report-wrapper .videotabreport").addClass("full");
			getDataJSON('temp.json', 'video.php', {"action":"getVideoMatch", "matches": id}, getVideoMatch, 1);
		}	
	}

	//фукнция удаления слайдов из слайдера протокола матча
	function reportMatchSliderClear() {
		if ($reportMatchSlider.slides.length > 0) {
			$reportMatchSlider.removeAllSlides();
			//$reportMatchSlider.reInit();
		}
	}

	//фунция получения вкладок Протокол и Составы для протокала матча по ID
	function getReportSquadsMatch(res) {
		if (res != "noconnect") {
			//ID домашней и выездной команды
			var homeClub = res.home;
			var awayClub = res.away;

			var playersHome = [];
			var playersAway = [];
			var playersReservHome = [];
			var playersReservAway = [];
			var i = 0;
			//заполняем 4 массива по игрокам
				
			$.each(res.players, function(key, val){
					
					
				if (val.club == homeClub) {
					if (val.sostav == 1) {
						playersHome.push({
							"sostav": val.sostav,
		            		"name": val.name,
		            		"event": val.event,
		            		"club": val.club,
		            		"time": val.time,
		            		"amplua": val.amplua
						});
					} else {
						playersReservHome.push({
							"sostav": val.sostav,
		            		"name": val.name,
		            		"event": val.event,
		            		"club": val.club,
		            		"time": val.time,
		            		"amplua": val.amplua
						});
					}
				} else {
					if (val.sostav == 1) {
						playersAway.push({
							"sostav": val.sostav,
		            		"name": val.name,
		            		"event": val.event,
		            		"club": val.club,
		            		"time": val.time,
		            		"amplua": val.amplua
						});
					} else {
						playersReservAway.push({
							"sostav": val.sostav,
		            		"name": val.name,
		            		"event": val.event,
		            		"club": val.club,
		            		"time": val.time,
		            		"amplua": val.amplua
						});
					}
				}


			});	
		

			//формируем HTML основного состава
			var maxPlayers = playersAway.length;
			if (playersHome.length > playersAway.length) {
				maxPlayers = playersHome.length;
			}
			var playersHTML = "";
			if (maxPlayers > 0) {
				var i = 0;
				var playersHTML = '<div class="head">Основной состав</div><table class="squads-table" cellpadding="0" cellspacing="0">';
				while(i < maxPlayers){ 
					playersHTML += "<tr><td>"
					
					if (playersHome[i] != undefined) {
						var imgEvent = "";
						if (playersHome[i]["event"] != "" && playersHome[i]["event"] != undefined) {
							imgEvent = '<img src="./img/event'+playersHome[i]["event"]+'.png" />';
						}

						//HTML амплуа
						var posHome = "";
						if (playersHome[i]["amplua"] != null) {
							posHome = ampluaData[playersHome[i]["amplua"]]["short_name"];
						}

						playersHTML += '<span class="event">'+imgEvent+'</span><p class="player">'+playersHome[i]["name"]+'</p><span class="pos">'+posHome+'</span>';
					} else {
						playersHTML += '<span class="event"></span><p class="player">&nbsp;</p><span class="pos"></span>';
					}
					playersHTML += "</td><td>";
						
					if (playersAway[i] != undefined) {
						var imgEventAway = "";
						if (playersAway[i]["event"] != "" && playersAway[i]["event"] != undefined) {
							imgEventAway = '<img src="./img/event'+playersAway[i]["event"]+'.png" />';
						}
						//HTML амплуа
						var posAway = "";
						if (playersAway[i]["amplua"] != null) {
							posAway = ampluaData[playersAway[i]["amplua"]]["short_name"];
						}
						playersHTML += '<span class="event">'+imgEventAway+'</span><p class="player">'+playersAway[i]["name"]+'</p><span class="pos">'+ampluaData[playersAway[i]["amplua"]]["short_name"]+'</span>';
					} else {
						playersHTML += '<span class="event"></span><p class="player">&nbsp;</p><span class="pos"></span>';
					}
					playersHTML += '<div class="border"></div></td></tr>';

		           	i++;
			    }
			    playersHTML += "</table>";
			}
				
			//alert(maxPlayers);
			//формируем HTML запасного состава
			var maxPlayersReserv = playersReservAway.length;
			if (playersReservHome.length > playersReservAway.length) {
				maxPlayersReserv = playersReservHome.length;
			}
			//alert(maxPlayersReserv);
			var playersReservHTML = "";
			if (maxPlayersReserv > 0) {

				var j = 0;
				var playersReservHTML = '<div class="head">Запасной состав</div><table class="squads-table" cellpadding="0" cellspacing="0">';

				while(j < maxPlayersReserv){ 
					playersReservHTML += "<tr><td>"
					//alert(playersReservHome[j]+"|||"+playersReservAway[j]);
					if (playersReservHome[j] != undefined) {
						var imgEventHomeReserv = "";
						//alert("1: "+playersReservHome[j]["event"]);
						//alert(playersReservHome[j]["name"]);
						if (playersReservHome[j]["event"] != "" && playersReservHome[j]["event"] != undefined) {
							imgEventHomeReserv = '<img src="./img/event'+playersReservHome[j]["event"]+'.png" />';
							//alert("2: "+playersReservHome[j]["event"]);
						}
						
						//HTML амплуа
						var posHomeReserv = "";
						if (playersReservHome[j]["amplua"] != null) {
							posHomeReserv = ampluaData[playersReservHome[j]["amplua"]]["short_name"];
						}

						playersReservHTML += '<span class="event">'+imgEventHomeReserv+'</span><p class="player">'+playersReservHome[j]["name"]+'</p><span class="pos">'+posHomeReserv+'</span>';
						//alert("3: "+playersReservHome[j]["event"]);
					} else {
						playersReservHTML += '<span class="event"></span><p class="player">&nbsp;</p><span class="pos"></span>';
						//alert("4: "+playersReservHome[j]["event"]);
					}
					playersReservHTML += "</td><td>";
					
					if (playersReservAway[j] != undefined) {
						var imgEventAwayReserv = "";
						if (playersReservAway[j]["event"] != "" && playersReservAway[j]["event"] != undefined) {
							imgEventAwayReserv = '<img src="./img/event'+playersReservAway[j]["event"]+'.png" />';
						}
						//HTML амплуа
						var posAwayReserv = "";
						if (playersReservAway[j]["amplua"] != null) {
							posAwayReserv = ampluaData[playersReservAway[j]["amplua"]]["short_name"];
						}
						playersReservHTML += '<span class="event">'+imgEventAwayReserv+'</span><p class="player">'+playersReservAway[j]["name"]+'</p><span class="pos">'+posAwayReserv+'</span>';
					} else {
						playersReservHTML += '<span class="event"></span><p class="player">&nbsp;</p><span class="pos"></span>';
					}
					playersReservHTML += '<div class="border"></div></td></tr>';
					
			        j++;
				}
				playersReservHTML += "</table>";
			}
			//alert(maxPlayersReserv);

			//формируем html код тренеров
			var coachHTML = '<div class="head">Тренеры</div><table class="squads-table" cellpadding="0" cellspacing="0"><tr><td><p class="text">'+res.coach.home+'</p></td><td><p class="text">'+res.coach.away+'</p><span class="border"></span></td></tr></table>';

			var arbitrHTML = '';
			if (res.arbitr != undefined) {
				arbitrHTML = '<div class="head">Официальные лица матча</div><table class="squads-table" cellpadding="0" cellspacing="0">';
				$.each(res.arbitr, function(key, val){
					arbitrHTML += '<tr><td><p class="text"><span>'+val.amplua+'</span> <br>'+val.name+' <span><nobr>\ '+val.city+'</nobr></span></p></td></tr>';
				});
				arbitrHTML += '</table>'
			}
				
			//формируем голы матча
			var goalsHTML = '';
				
			if (res.gols != undefined) {
				var goalHomeHTML = '';
				var goalAwayHTML = '';
				if (res.gols[homeClub] != undefined) {
					goalHomeHTML = res.gols[homeClub];
				}
				if (res.gols[awayClub] != undefined) {
					goalAwayHTML = res.gols[awayClub];
				}
				goalsHTML = '<div class="goals-wrap"><div class="goals"><div class="home">'+goalHomeHTML+'</div><div class="away">'+goalAwayHTML+'</div><div class="border"></div><div class="clr"></div></div></div>';
			}
			//формируем события матча
			var chronologyEventsHTML = '';
			if (res.events != undefined) {
				chronologyEventsHTML = '<div id="events-list"><div class="head">Хронология событий</div>';
				$.each(res.events, function(key, val){

					if (val.event == 7) {
						chronologyEventsHTML += '<div class="chronology end"><p class="text">'+val.name+'</p></div>';
					} else {
						var eventImg = '';
						if (val.event == "1" || val.event == '1-10' || val.event == '1-11' || val.event == '2' || val.event == '3' || val.event == '4' || val.event == '4-3' || val.event == '5' || val.event == '8-81' || val.event == '8-82' ) {
							eventImg = '<img src="./img/event'+val.event+'.png">';
						}
						var minHTML = '';
						if (val.time != "" && val.time != undefined) {
							minHTML = val.time;
						}
						var noteHTML = '';
						if (val.note != "" && val.note != undefined) {
							noteHTML = val.note;
						}
						var clubImg = '';
						if (val.club != "" && val.club != undefined) {
							clubImg = '<img src="./img/logos/small/'+val.club+'.png" >';
						}
						chronologyEventsHTML += '<div class="chronology"><p class="event">'+eventImg+'</p><p class="min">'+minHTML+'</p><p class="text">'+val.name+' <span class="note">'+noteHTML+'</span></p><div class="logo">'+clubImg+'</div></div>';
					}
				});
					
				chronologyEventsHTML += '</div>'
			}

			//HTML код стадиона
			var stadiumHTML = '';
			if (res.stName != "" && res.stName != undefined) {
				stadiumHTML = '<div class="head">Стадион</div><table class="squads-table" cellpadding="0" cellspacing="0"><tr><td><p class="text">'+res.stName+' <span><nobr>'+res.stCity+'</nobr></span></p></td></tr></table>';
			}

			if ($("#report-squads").length) {
				$("#report-squads").html(playersHTML+playersReservHTML+coachHTML+arbitrHTML+'<div class="banner-ea" data-url="http://rfpl.org/beautifully/"><img src="./img/banner-ipad2.png" class="phone"></div>');
			}
			if ($("#report-chronology").length) {
				$("#report-chronology").html(goalsHTML+chronologyEventsHTML+stadiumHTML+'<div class="banner-ea" data-url="http://rfpl.org/beautifully/"><img src="./img/banner-ipad2.png" class="phone"></div>');
			}

			setTimeout(function(){
				$("#tabs-report-wrapper").removeClass("load");
				$("#report-match, #report-chronology, #report-squads").removeClass("load");
				
				$("#report-match-slider, #report-match-slider .swiper-wrapper, #report-match-slider .swiper-slide, #report-match-slider .report-slide").css("height", $("#report-match").height());	
				$("#report-match-slider .swiper-slide, #report-match-slider .report-slide").css("width", $(window).width());

				$reportMatchSlider.reInit();
			}, 250);

		} else {
			
			//если интернета нет
			$("#noconnect").show();
			$("#refresh-repeat").data("action", "reportsquadsmatch");

			$("#tabs-report-wrapper .eventstabreport, #tabs-report-wrapper .squadstabreport").removeClass("full");

			setTimeout(function(){
				$("#report-match-slider, #report-match-slider .swiper-wrapper, #report-match-slider .swiper-slide, #report-match-slider .report-slide").css("height", $("#report-match").height());	
				$("#report-match-slider .swiper-slide, #report-match-slider .report-slide").css("width", $(window).width());
				$("#report-match, #report-chronology, #report-squads").removeClass("load");
				$reportMatchSlider.reInit();
				var num = $("#tabs-report-wrapper a.active").attr("href").replace("#","");
				//$reportMatchSlider.swipeTo(num, 0, true);

			}, 250);

		}
	}

	//фукнция получения Видео матча
	function getVideoMatch(res) {

		if (res != "noconnect") {

			var videoMatchHTML = '';
			$.each(res.video, function(key, val){
				videoMatchHTML += '<a class="video" href="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" target="_blank"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.time+'<i></i> </span></span><span class="bg"></span></a>';
			});
			$("#report-video .video-list").html(videoMatchHTML);

			
			setTimeout(function(){
				$("#tabs-report-wrapper").removeClass("load");
				$("#report-match, #report-video").removeClass("load");
				$reportMatchSlider.reInit();
			}, 250);

		} else {
			//если интернета нет
			$("#noconnect").show();
			$("#refresh-repeat").data("action", "reportvideomatch");
			$("#tabs-report-wrapper .videotabreport").removeClass("full");
			setTimeout(function(){
				$("#report-match-slider, #report-match-slider .swiper-wrapper, #report-match-slider .swiper-slide, #report-match-slider .report-slide").css("height", $("#report-match").height());	
				$("#report-match-slider .swiper-slide, #report-match-slider .report-slide").css("width", $(window).width());
				$("#report-match, #report-video").removeClass("load");
				$reportMatchSlider.reInit();
				var num = $("#tabs-report-wrapper a.active").attr("href").replace("#","");
				//$reportMatchSlider.swipeTo(num, 0, true);
			}, 250);
		}
	}
	//фукнция получения Видео матча
	function getPhotoMatch(res) {
		if (res != "noconnect") {
			var photoMatchHTML = '';
			$.each(res.photo, function(key, val){
				photoMatchHTML += '<a class="photo" href="#" data-id="'+val.id+'"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.count+' фото<i></i> </span></span><span class="bg"></span></a>';
			});
			$("#report-photo .photo-list").html(photoMatchHTML);


			setTimeout(function(){
				$("#tabs-report-wrapper").removeClass("load");
				$("#report-match, #report-photo").removeClass("load");
				$reportMatchSlider.reInit();
			}, 250);
			
			var id = $("#report-match").data("id");

		} else {
			//если интернета нет
			$("#noconnect").show();
			$("#refresh-repeat").data("action", "reportphotomatch");
			$("#tabs-report-wrapper .phototabreport").removeClass("full");
			setTimeout(function(){
				$("#report-match, #report-photo").removeClass("load");
				$("#report-match-slider, #report-match-slider .swiper-wrapper, #report-match-slider .swiper-slide, #report-match-slider .report-slide").css("height", $("#report-match").height());	
				$("#report-match-slider .swiper-slide, #report-match-slider .report-slide").css("width", $(window).width());
				$reportMatchSlider.reInit();
				var num = $("#tabs-report-wrapper a.active").attr("href").replace("#","");
				//$reportMatchSlider.swipeTo(num, 0, true);
			}, 250);
		}
	
	}

    StatusBar.hide();
    switch(device.platform){
        case "WinCE":
            $("body").addClass("wp").addClass("wp7");
        break;
        case "Win32NT":
            $("body").addClass("wp").addClass("wp8");
        break;
        case "iOS":
            $("body").addClass("iOS");
        break;
        default:
            $("body").addClass("Android");
    };
    //класс для древного андроид
    //где проблема со скроллом при повторном добавлении контента
    if (device.version == "4.2.1") {
    	$("body").addClass("a421");
    }
    //$("#debug").html(device.model+" ||| "+device.version);
    //для ANDROID touchmove для появления/скрытия главного меню
    
    /*
    if ( $("body").hasClass("Android") && !$("body").hasClass("a421") ) {

        $("#swipe-nav").on("touchmove", function(e){
        	if (!$("#back:visible").length && !$("#main-nav").hasClass("open")) {
	            viewMainNav("show");
	        }
           

            if ($("#back:visible").length) {
                var $fromPage = $("#pages > .current");
                //достаем ID блока к которому возврат из массива
                var toPage = backArray[backArray.length-1];
                var $toPage = $("#"+toPage);
            
                animateInner("back", $toPage, $fromPage);
            }
        });

        $("#fade").on("touchmove", function(){
            viewMainNav("hide");
        });

    } else {

        $("#swipe-nav").on("swiperight", function(){
            if (!$("#back:visible").length && !$("#main-nav").hasClass("open") ) {
                viewMainNav("show");
            }
            //$("#debug").html($("#back:visible").length);
            if ($("#back:visible").length) {
                var $fromPage = $("#pages > .current");
                //достаем ID блока к которому возврат из массива
                var toPage = backArray[backArray.length-1];
                var $toPage = $("#"+toPage);
            
                animateInner("back", $toPage, $fromPage);
            }

        }).on("swipeleft");
        $("#fade").on("swipe", function(){
            viewMainNav("hide");
        });

    }*/
    //возврат назад
	$("#swipe-nav").swipe({
		swipeStatus:function(event, phase, direction, distance, duration, fingers){
			//$("#title-page").html(phase+"---"+direction);
			//&& distance > 50
		    if (phase=="move" && direction =="right" ) {
		       	if (!$("#back:visible").length && !$("#main-nav").hasClass("open")) {
		            viewMainNav("show");
		        }
	           

	            if ($("#back:visible").length) {
	                var $fromPage = $("#pages > .current");
	                //достаем ID блока к которому возврат из массива
	                var toPage = backArray[backArray.length-1];
	                var $toPage = $("#"+toPage);
	            
	                animateInner("back", $toPage, $fromPage);
	            }
            	return false;
		    }
		    // && distance > 50
		    if (phase=="move" && direction =="left") {
		       	viewMainNav("hide");
            	return false;
		    }

		    //return false;
		}
	});

	$("#fade").swipe({
		swipeStatus:function(event, phase, direction, distance, duration, fingers){
			//$("#title-page").html(phase+"---"+direction);
			// && distance > 50
		    if (phase=="move" && direction =="left") {
		       	viewMainNav("hide");
            	return false;
		    }

		    //return false;
		}
	});
	$("#main-nav").swipe({
		swipeStatus:function(event, phase, direction, distance, duration, fingers){
			//$("#title-page").html(phase+"---"+direction);
		    if (phase=="move" && direction =="left") {
		       	viewMainNav("hide");
            	return false;
		    }

		    //return false;
		}
	});
 	
    
    //скрипты из ready
    	//переход между разделами
	$("#main-nav a").on("tap ", function(){
	    if (!$(this).hasClass("active")) {
	    	$("#main-nav a").removeClass("active");
	    	viewMainNav("disable");
	    	$(this).addClass("active");
	    	var nextPage = $(this).attr("href");
	    	page(nextPage);
	    } else {
	    	viewMainNav("disable");
	    	//для календаря запускаем слайдер медиа и переинициализация слайдера туров
			if ($("#calendar").hasClass("current")) {
				mediaSliderControl("start");
				$calendarSlider.reInit();
			}
			if ($("#clubs").hasClass("current")) {
	    		$("#clubs-nav").show();
				$("#club-header").removeClass("show");
				//добавляем прелоадер ко всем вкладкам клуба
				//$("#clubs .clubs-slide, #tabs-clubs-wrapper").addClass("load");
	    	}
	    }
	    	   
	    return false;
	});

	//показ-скрытие главного меню
	$("#logo, #nav-button, #fade").on("tap ", function(){

		//либо переход назад либо вызов главного меню
		if ($("#back:visible").length) {
			var $fromPage = $("#pages > .current");
			//достаем ID блока к которому возврат из массива
			var toPage = backArray[backArray.length-1];
			var $toPage = $("#"+toPage);
			
			animateInner("back", $toPage, $fromPage);

		} else {

			if ( $("#main-nav").hasClass("open") ) {
				viewMainNav("hide");
				//для календаря запускаем слайдер медиа
				if ($("#calendar").hasClass("current")) {	
					mediaSliderControl("start");
				}
				
			} else {
				
				viewMainNav("show");
				//для календаря останавливаем слайдер медиа
				if ($("#calendar").hasClass("current")) {	
					mediaSliderControl("stop");
				}
			}

		}

		return false;
	});
	
	//скрытие внутренних страниц
	$("#back").on("tap", function(){

		var $fromPage = $("#pages .current");
		//достаем ID блока к которому возврат из массива
		var toPage = backArray[backArray.length-1];

		var $toPage = $("#"+toPage);
		animateInner("back", $toPage, $fromPage);

		return false;
	});

	
/*******************************
*******СТРАНИЦА КАЛЕНДАРЯ*******
*******************************/
	$mediaSlider = new Swiper('#media-slider',{
	    pagination: '.pagination',
	    paginationClickable: true,
	    slidesPerView: 1,
	    loop: true,
	    calculateHeight: true,
	    createPagination: true,
	    //autoplay: 5000,
	    //autoplayDisableOnInteraction: true,
	    followFinger: false,
	    touchRatio: 1,
	    onSlideClick: function(slider) {
	    	var $clickSlide = $(slider.clickedSlide);
	    	if ($clickSlide.hasClass("news")) {
	    		//получение новости по ID
				var id = $clickSlide.find(".desc").data("id");
				//фукнция загрузки и показа новости по ID
				fullNewsLoad(id);
				pageInner("#news-inner", "#calendar");
	    	}
	    	if ($clickSlide.hasClass("photo")) {
	    		var id = $clickSlide.find(".desc").data("id");
				pageInner("#photo-inner", "#calendar");
				photoLoad(id, "");
	    	}
	    	if ($clickSlide.hasClass("video")) {
	    		var urlVideo = $clickSlide.find(".desc").data("link");
	    		var idVideo = $clickSlide.find(".desc").data("idfile");
	    		//YoutubeVideoPlayer.openVideo(idVideo);
	    		VideoPlayer.play(urlVideo);
	    		//window.open(urlVideo, "_system");
	    	}
	    	
	    }
	});

	

	$("#tabs-media-slider a").on("click", function(){
		if (!$(this).hasClass("active") && !$("#media-index").hasClass("load") ) {
			$(this).addClass("active").siblings().removeClass("active");
			$("#media-index").addClass("load");
			mediaSliderControl();

		}
		return false;
	});

	$("#calendar-slider")
		.on("tap", ".match", function(){
			if ($(this).hasClass("active")) {
				$(this).removeClass("active");
				$calendarSlider.reInit();
				//$(this).find(".add-info").hide();
			} else {
				
				$("#supercup-match .match").removeClass("active");
				
				//$(".match .add-info").hide();
				//$(".match").removeClass("active");
				//var $this = $(this);
				//$(this).addClass("active").find(".add-info").slideDown(250, function(){
				//	$calendarSlider.reInit();
				//});
				$(this).addClass("active").siblings().removeClass("active");
				if ($(this).hasClass("last")) {
					$("#calendar").animate({scrollTop:1000}, '250');
				}
				$calendarSlider.reInit();
			}
			

			return false;
		})
		.on("tap", ".match .links span", function(){
			//при переходе на страницу матча получаем 
			//ID матча, вкладки, активную вкладку и html код матча для подставновки в header
			var activeTab = $(this).data("tab");
			var $linksBlock = $(this).parent();
			
			var idMatch = $linksBlock.data("id");
			//проверочные переменные для вкладок
			var checkSquads = $linksBlock.data("squads")*1;
			var checkReport = $linksBlock.data("report")*1;
			var checkVideo = $linksBlock.data("video")*1;
			var checkPhoto = $linksBlock.data("photo")*1;

			//html код протокола в матче
			var $activeMatch = $("#calendar-slider .match.active");
			var classMatch = $activeMatch.attr("class").replace("active", "");
			var matchHtml = $activeMatch.find(".main").html();

			$("#report-match-header .tour-wrapper").html('<div class="'+classMatch+'"><div class="main">'+matchHtml+'</div></div>');

			//запускаем фукнцию получения данных по матчу
			//и формирования html кода слайдов
			//передается ID матча и активная вкладка
			//и проверочные переменные для формирования html кода вкладов
			$("#tabs-report-wrapper").addClass("load");
			getInfoMatch(idMatch, activeTab, checkSquads, checkReport, checkVideo, checkPhoto);
			
			pageInner("#report-match", "#calendar");

			return false;
		});
	
	$("#prev-tour").on("tap", function(){
	    $calendarSlider.swipePrev();
	    
	    return false;
	});
	$("#next-tour").on("tap", function(){
	    $calendarSlider.swipeNext();
	    return false;
	});



	

/*******************************
*******СТРАНИЦА ЛИДЕРЫ*******
*******************************/
	//слайдер лидеров
  	$leadersSlider = new Swiper('#leaders-slider',{
	    slidesPerView: 1,
	    createPagination: false,
	    loop: false,
	    calculateHeight: true,
	    autoplay: false,
	   	speed: 300,
	   	touchRatio: 1,
	   	noSwiping: true,
	   	simulateTouch: false,
	   	resizeReInit: true,
	   	followFinger: false,
	    onSwiperCreated: function(slider){
	    	var heightContent = $("#leaders").height();
		    $("#leaders-slider, #leaders-slider .swiper-wrapper, #leaders-slider .swiper-slide").css("height", heightContent);
	    },
	    onInit: function(slider) {
	    	var heightContent = $("#leaders").height();
		    $("#leaders-slider, #leaders-slider .swiper-wrapper, #leaders-slider .swiper-slide").css("height", heightContent);
	    },
	    onSlideChangeStart: function(slider){
	    	$("#leaders-tabs a").removeClass("active");
	    	$("#leaders-tabs a[href=#"+slider.activeIndex+"]").addClass("active"); 
	    }
	});

  	$("#leaders-tabs a").on("tap", function(){
  		if (!$(this).hasClass("active")) {
  			var num = $(this).attr("href").replace("#", "");
  			$leadersSlider.swipeTo(num);
  		}
  		return false;
  	});

  	//переход на страницу просмотра игрока
  	$("#players-forward, #players-foul").on("tap", ".player", function(){
  		pageInner("#players-inner", "#leaders");
  		//получение информации об игроке по ID
  		getPlayersInfo($(this).data("id"));
  		return false;
  	});

/*******************************
*******СТРАНИЦА НОВОСТИ*******
*******************************/

	//переход к просмотру полной новости
	$("#news-list").on("tap", ".news, .main-news", function(){
		$("#news").data("scroll", $("#news").scrollTop());
		//переход на внутреннюю страницу новости
		pageInner("#news-inner", "#news");
		//получение новости по ID
		var id = $(this).data("id");
		//фукнция загрузки и показа новости по ID
		fullNewsLoad(id);

		return false;
	});

	//открытие всех ссылок в контенте новости в стандартном браузере
	$("#news-full").on("tap", "a", function(){
		var url = $(this).attr("href");
		window.open(url, "_system");

		return false;
	});

	//подгрузка новостей при скроле!
	$("#news").scroll(function() {
		if ($(this).scrollTop() + $(this).height() >= $("#news-list").height()-100 && newsLoadFlag == 0 ) {
			getNews(newsPageLoad);
		}
	});
	
	
/*******************************
*******СТРАНИЦА ВИДЕО*******
*******************************/
	$("#video-tabs a").on("tap", function(){
		if (!$(this).hasClass("active")) {
			$(this).addClass("active").siblings().removeClass("active");
			var num = $(this).attr("href").replace("#", "");
  			$videoSlider.swipeTo(num);
		}
		return false;
	});

	//слайдер видео категорий
  	$videoSlider = new Swiper('#video-slider',{
	    slidesPerView: 1,
	    createPagination: false,
	    loop: false,
	    calculateHeight: false,
	    autoplay: false,
	   	speed: 300,
	   	touchRatio: 1,
	   	noSwiping: true,
	   	simulateTouch: false,
	   	resizeReInit: true,
	   	followFinger: false,
	    onSwiperCreated: function(slider){
	    	var activeSlide = $("#tabs-video-wrapper a.active").attr("href").replace("#", "");
	    	var $activeSlide = $(slider.slides[activeSlide]);

	    	var heightContent = $("#video").height();
		    $("#video-slider, #video-slider .swiper-wrapper, #video-slider .swiper-slide").css("height", heightContent);
	    	
	    },
	    onInit: function(slider) {
	    	var activeSlide = $("#tabs-video-wrapper a.active").attr("href").replace("#", "");
	    	var $activeSlide = $(slider.slides[activeSlide]);

	    	var heightContent = $("#video").height();
		    $("#video-slider, #video-slider .swiper-wrapper, #video-slider .swiper-slide").css("height", heightContent);
	    },
	    onSlideChangeStart: function(slider){
	    	$("#video-tabs a").removeClass("active");
	    	$("#video-tabs a[href=#"+slider.activeIndex+"]").addClass("active"); 
	    	
	    }, 
	    onSlideChangeEnd: function(slider) {
	    	var $activeSlide = $(slider.slides[slider.activeIndex]); 
	    	var type = slider.activeIndex;	

	    	$("#noconnect").hide();

	    	//при переключении слайдов подгрузка видео если они еще не загружались!
	    	if (type == 1) {
				if ( !$("#video-review .video").length ) {
					loadTabVideo(1, "");
				}
			} else if (type == 2) {
				if (!$("#video-moments .video").length) {
					loadTabVideo(2, "");
				}
			} else if (type == 3) {
				if (!$("#video-story .video").length) {
					loadTabVideo(3, "");
				}
			} else {
				if (!$("#video-best .video").length) {
					loadTabVideo(0, "");
				}
			}

		}

	});

	//при клике на видео открываем его в стандартном браузере устройства
	$("#video-slider").on("tap", ".video", function(){
		//window.open($(this).attr("href"), "_system");
		
		var urlVideo = $(this).attr("href");
		var idVideo = $(this).data("idfile");
	    //YoutubeVideoPlayer.openVideo(idVideo);
	    VideoPlayer.play(urlVideo);
	    
		return false;
	});

	//подгрузка видео в активный слайд при скроле!
	$("#video-slider .swiper-slide").scroll(function() {
		if ( ($(this).scrollTop()+$(this).height() >= $(this).find(".video-list").height()-200) && !$(this).find(".preloader").length && !$(this).find(".noconnect-small").length ) {

			//получаем активную вкладку
			var activeVideoTab = $("#tabs-video-wrapper a.active").attr("href").replace("#","")*1+1;
				
			//получаем блок в который будем добавлять прелоадер и видео
			if (activeVideoTab == 1) {
				var videoPageLoad = videoBestPageLoad;
				var type = 0;
			}
			if (activeVideoTab == 2) {
				var videoPageLoad = videoReviewPageLoad;
				var type = 1;
			}
			if (activeVideoTab == 3) {
				var videoPageLoad = videoMomentsPageLoad;
				var type = 2;
			}
			if (activeVideoTab == 4) {
				var videoPageLoad = videoStoryPageLoad;
				var type = 3;
			}

			loadTabVideo(type, videoPageLoad);		
		}
	});

/*******************************
*******СТРАНИЦА ФОТО*******
*******************************/
	//переход на просмотр содержимого фотоальбома
	$("#photoalbum-list").on("tap", ".photo", function(){
		$("#photo").data("scroll", $("#photo").scrollTop());
		var id = $(this).data("id");
		photoLoad(id, "");
		pageInner("#photo-inner", "#photo");

		return false;
	});

	//подгрузка фото при скроле
	$("#photo").scroll(function() {
		if($(this).scrollTop() + $(this).height() >= $("#photoalbum-list").height()-200 && photoalbumLoadFlag == 0 ) {
			getPhoto(photoalbumPageLoad);		
		}//END проверка скрола
	});	

	//подгрузка фотографий фотоальбома при скроле
	$("#photo-inner").scroll(function(){
		if($(this).scrollTop() + $(this).height() >= $("#photo-list").height()-200 && photoalbumPhotoLoadFlag == 0 ) {
			var id = $("#photo-inner").data("id");
			photoLoad(id, photoalbumPhotoPageLoad);
		}
	});

/*******************************
*******СТРАНИЦА КЛУБЫ*******
*******************************/
	var tabsWidth = 0;

	$("#tabs-clubs-wrapper").on("scroll", function(){
		//$("#debug").html($(this).scrollLeft());
	});

	//переход к просмотру информации о клубе
	$("#clubs-nav").on("tap", ".clubs a", function(){
		//если клуб на который заходили в последний раз
		//просто скрываем меню и показываем блок с контентом
		//иначе подставляем данные по новому клубу
		//и делаем запросы
		/*
		if ($(this).hasClass("active")) {
			$("#clubs-nav").hide();
			$("#club-header").addClass("show");
			$("#title-page").addClass("clubs");


			$("#tabs-clubs-wrapper").removeClass("load");

		} else {*/
			$(this).addClass("active").siblings().removeClass("active");
			var id = $(this).attr("href").replace("#", "");
			var name = $(this).find("p").text();
			var stadium = $(this).data("stadium");
			var town = $(this).data("town");
			var year = $(this).data("year");
			//подставляем основные данные в шапку клуба
			$("#club-header .logo img").attr("src", "./img/logos/big/"+id+".png");
			$("#club-header .name").html(name);
			$("#club-header .town").html(town);
			$("#club-header .year").html(year+" г.");
			$("#club-header .stadium").html(stadium);

			$("#clubs, #clubs .clubs-slide, #tabs-clubs-wrapper").addClass("load");

			$("#clubs-nav").hide();
			$("#club-header").addClass("show");
			$("#title-page").addClass("clubs");

			//сбрасываем счетчик номеров страниц для подгрузке при скроле!
			clubPhotoPageLoad = 1;
			clubVideoPageLoad = 1;
			clubNewsPageLoad = 1;
			//очищаем вкладки клубов
			$("#club-match-calendar, #club-news, #club-video .video-list, #club-photoalbum .photo-list, #club-forwards, #club-middfielders, #club-defenders, #club-goalkeepers, #direction-staff, #main-staff, #youth-staff, #club-awards").html("");

			
			$("#tabs-clubs-wrapper .tabs a").removeClass("active");
			$("#tabs-clubs-wrapper .tabs a[href=#0]").addClass("active");
			$("#tabs-clubs-wrapper").scrollLeft(0);

			//отправляем запрос на получение календаря игр клуба
			var idClub = $(this).attr("href").replace("#","");
			getClubCalendar(idClub);
			

		//}
		return false;
	});

	//показ/скрытие меню выбора клубов при клике на тайтл
	$("#title-page").on("tap", function(){
		viewMainNav("disable");
		if ($(this).hasClass("clubs")) {
			if ($("#clubs-nav:visible").length) {
				$("#club-header").addClass("show");
				$("#clubs-nav").hide();
				
			} else {
				$("#club-header").removeClass("show");
				$("#clubs-nav").show();
			}
		}
		return false;
	});

	//слайдер вкладок на странице клуба
  	$clubTabsSlider = new Swiper("#club-tabs-slider",{
	    slidesPerView: 1,
	    createPagination: false,
	    loop: false,
	    calculateHeight: false,
	    autoplay: false,
	   	speed: 300,
	   	touchRatio: 1,
	   	noSwiping: true,
	   	simulateTouch: false,
	   	resizeReInit: true,
	   	followFinger: false,
	   	onInit: function(slider) {
	   		
	   		var activeSlide = $("#tabs-clubs-wrapper .tabs a.active").attr("href").replace("#", "");
	    	var $activeSlide = $(slider.slides[activeSlide]);
	
	    	var heightContent = $("#clubs").height();
		    $("#club-tabs-slider, #club-tabs-slider .swiper-wrapper, #club-tabs-slider .swiper-slide").css("height", heightContent);
	    	
	    	if (activeSlide > 3) {
	    		$("#tabs-clubs-wrapper").scrollLeft(1000);
	    	} else {
	    		$("#tabs-clubs-wrapper").scrollLeft(0);
	    	}
	   	},
	    onSwiperCreated: function(slider){
	    	var activeSlide = $("#tabs-clubs-wrapper .tabs a.active").attr("href").replace("#", "");
	    	var $activeSlide = $(slider.slides[activeSlide]);
	    	
	    	var heightContent = $("#clubs").height();
		    $("#club-tabs-slider, #club-tabs-slider .swiper-wrapper, #club-tabs-slider .swiper-slide").css("height", heightContent);
	    },
	    onSlideChangeStart: function(slider){
	    	$("#tabs-clubs-wrapper .tabs a").removeClass("active");
	    	$("#tabs-clubs-wrapper .tabs a[href=#"+slider.activeIndex+"]").addClass("active"); 
	    	
	    	if (slider.activeIndex > 3) {
	    		$("#tabs-clubs-wrapper").scrollLeft(1000);
	    	} else {
	    		$("#tabs-clubs-wrapper").scrollLeft(0);
	    	}

	    }, 
	    onSlideChangeEnd: function(slider) {
	    	var id = $("#clubs-nav .clubs a.active").attr("href").replace("#","");

	    	$("#noconnect").hide();

	    	//подгружаем вкладки клуба в зависимости от типа и если в них не было загрузки
	    	var type = slider.activeIndex;
	    	if (type == 1 && !$("#club-news .news").length) {
	    		getClubNews(id, "");
	    	}
	    	if (type == 2 && !$("#club-video .video").length) {
	    		getClubVideo(id, "");
	    	}
	    	if (type == 3 && !$("#club-photo .photo").length) {
	    		getClubPhoto(id, "");
	    	}
	    	if (type == 4 && !$("#club-players .player").length) {
	    		getClubPlayers(id);
	    	}
	    	if (type == 5 && !$("#club-staff .people").length) {
	    		getClubStaff(id);
	    	}
			if (type == 6 && !$("#club-awards .awards").length) {
	    		getClubAwards(id);
	    	}			
			
	    }
	});
	
	
	$("#club-match-calendar").on("tap", ".match", function(){
		//показ/скрытие блока с доп инфой о матче
		if ($(this).hasClass("active")) {
			$(this).removeClass("active");
		} else {
			$(this).addClass("active").siblings().removeClass("active");
		}
		$clubTabsSlider.resizeFix();

		return false;
	}).on("tap", ".match .links span", function(){
		//при переходе на страницу матча получаем 
		//ID матча, вкладки, активную вкладку и html код матча для подставновки в header
		var activeTab = $(this).data("tab");
		var $linksBlock = $(this).parent();
		var idMatch = $linksBlock.data("id");
		//проверочные переменные для вкладок
		var checkSquads = $linksBlock.data("squads")*1;
		var checkReport = $linksBlock.data("report")*1;
		var checkVideo = $linksBlock.data("video")*1;
		var checkPhoto = $linksBlock.data("photo")*1;

		//html код протокола в матче
		var $activeMatch = $("#club-match-calendar .match.active");
		var classMatch = $activeMatch.attr("class").replace("active", "");
		var matchHtml = $activeMatch.find(".main").html();
		$("#report-match-header .tour-wrapper").html('<div class="'+classMatch+'"><div class="main">'+matchHtml+'</div></div>');
		//запускаем фукнцию получения данных по матчу
		//и формирования html кода слайдов
		//передается ID матча и активная вкладка
		//и проверочные переменные для формирования html кода вкладов
		$("#tabs-report-wrapper").addClass("load");
		getInfoMatch(idMatch, activeTab, checkSquads, checkReport, checkVideo, checkPhoto);
		//$reportMatchSlider.appendSlide('<img src="'+val.img_big+'"><span data-id="'+val.id+'" class="desc">'+val.name+'</span>', 'swiper-slide media news');
		pageInner("#report-match", "#clubs");
		return false;
	});

	$("#club-video").on("tap", ".video", function(){
		//открытие видео в стандартном браузере
		//window.open($(this).attr("href"), "_system");
		var idVideo = $(this).data("idfile");
		var urlVideo = $(this).attr("href");
	    //YoutubeVideoPlayer.openVideo(idVideo);
	    VideoPlayer.play(urlVideo);
		

		return false;
	});
	$("#club-photoalbum").on("tap", ".photo", function(){
		$("#club-photoalbum").parent().data("scroll", $("#club-photoalbum").parent().scrollTop());
		//переход на внутреннюю страницу фотоальбома
		var id = $(this).data("id");
		photoLoad(id, "");
		pageInner("#photo-inner", "#clubs");
		return false;
	});
	$("#club-news").on("tap", ".news", function(){
		$("#club-news").parent().data("scroll", $("#club-news").parent().scrollTop());
		//переход на просмотр полной новости
		var id = $(this).data("id");
		//получение новости по ID
		fullNewsLoad(id);
		pageInner("#news-inner", "#clubs");
		return false;
	});
	$("#club-players").on("tap", ".player", function(){
		pageInner("#players-inner", "#clubs");
  		//получение информации об игроке по ID
  		getPlayersInfo($(this).attr("href").replace("#",""));
  		return false;
	});


  	$("#tabs-clubs-wrapper .tabs a").on("tap", function(){
  		if (!$(this).hasClass("active")) {
  			$(this).addClass("active").siblings().removeClass("active");
  			var num = $(this).attr("href").replace("#", "");
  			$clubTabsSlider.swipeTo(num);
  		}
  		return false;
  	});

  	//подгрузка контента вкладок клуба при скроле блоков на странице клуба!
	$("#club-tabs-slider .swiper-slide").scroll(function() {
		//$("#debug").html("st="+$(this).scrollTop()+" | "+"wH="+$(this).height()+" | sH="+ $(this).find(".clubs-slide").height());
		if ( $(this).scrollTop() + $(this).height() >= $(this).find(".clubs-slide").height()-100 ) {
			//$("#debug").html($("#club-match-calendar .match").length);
			//получаем активную вкладку
			var activeClubTab = $("#tabs-clubs-wrapper a.active").attr("href").replace("#","")*1+1;

			var idClub = $("#clubs-nav .clubs a.active").attr("href").replace("#","");

			if (activeClubTab == "2" && clubNewsPageLoadFlag == 0) {
				getClubNews(idClub, clubNewsPageLoad);	
			}
			if (activeClubTab == "3" && clubVideoPageLoadFlag == 0) {
				getClubVideo(idClub, clubVideoPageLoad);
			}
			if (activeClubTab == "4" && clubPhotoPageLoadFlag == 0) {
				getClubPhoto(idClub, clubPhotoPageLoad);
			}
				
		}//END проверка скрола
		
	});





/*******************************
*******СТРАНИЦА ПРОТОКОЛ МАТЧА*******
*******************************/
	
  	//слайдер протокола матча
  	$reportMatchSlider = new Swiper('#report-match-slider',{
	    slidesPerView: 1,
	    createPagination: false,
	    loop: false,
	    calculateHeight: false,
	    autoplay: false,
	   	speed: 300,
	   	touchRatio: 1,
	   	noSwiping: true,
	   	simulateTouch: false,
	   	resizeReInit: false,
	   	followFinger: false,
	    onSwiperCreated: function(slider){
	    	var heightContent = $("#report-match").height();
		    $("#report-match-slider, #report-match-slider .swiper-wrapper, #report-match-slider .swiper-slide").css("height", heightContent);
	    },
	    onInit: function(slider) {
	    	
	    	//$("#debug").append("1");
	    	if ($("#tabs-report-wrapper a.active").length) {
			    var activeSlide = $("#tabs-report-wrapper a.active").attr("href").replace("#", "")*1;
			    if (slider.activeSlide != activeSlide) {
			    	slider.swipeTo(activeSlide, 0, false);	
			    	//$("#debug").append("|2");
			    }
			}
			
			var heightContent = $("#report-match").height();
		    $("#report-match-slider, #report-match-slider .swiper-wrapper, #report-match-slider .swiper-slide").css("height", heightContent);
		    //var heightContent = $("#report-match").height();
		    //$("#report-match-slider, #report-match-slider .swiper-wrapper, #report-match-slider .swiper-slide").css("height", heightContent);
	    },
	    onSlideChangeStart: function(slider){
	    	$("#tabs-report-wrapper a").removeClass("active");
	    	$("#tabs-report-wrapper a[href=#"+slider.activeIndex+"]").addClass("active"); 
	    	var $activeSlide = $(slider.slides[slider.activeIndex]);
	    }, 
	    onSlideChangeEnd: function(slider) {
	    	$("#tabs-report-wrapper a").removeClass("active");
	    	$("#tabs-report-wrapper a[href=#"+slider.activeIndex+"]").addClass("active"); 

	    	var $activeSlide = $(slider.slides[slider.activeIndex]);	

	    	$("#noconnect").hide();

	    	//ID матча
	    	var id = $("#report-match").data("id");

	    	//подгрузка вкладок при переключении слайдов протокола матча!
	    	if ( ($("#tabs-report-wrapper .eventstabreport.active").length || $("#tabs-report-wrapper .squadstabreport.active").length) && !$("#tabs-report-wrapper .eventstabreport").hasClass("full") ) {
				$("#tabs-report-wrapper .eventstabreport, #tabs-report-wrapper .squadstabreport").addClass("full");
				getDataJSON('temp.json', 'matches.php', {"action":"getMatchesInfo", "matches": id}, getReportSquadsMatch, 1);
			}
	    	//если есть вкладка видео и в нее нет загрузки
			if ($("#tabs-report-wrapper .videotabreport.active").length && !$("#tabs-report-wrapper .videotabreport").hasClass("full")) {
				$("#tabs-report-wrapper .videotabreport").addClass("full");
				getDataJSON('temp.json', 'video.php', {"action":"getVideoMatch", "matches": id}, getVideoMatch, 1);
			}
			//если есть вкладка фото и в нее нет загрузки
			if ($("#tabs-report-wrapper .phototabreport.active").length && !$("#tabs-report-wrapper .phototabreport").hasClass("full")) {
				$("#tabs-report-wrapper .phototabreport").addClass("full");
				getDataJSON('temp.json', 'photo.php', {"action":"getPhotoMatch", "matches": id}, getPhotoMatch, 1);
			}

	    }
	});
	

  	//переключение вкладов по клику на ссылки
  	$("#tabs-report-wrapper").on("tap", ".tabs a", function(){
  		if (!$(this).hasClass("active")) {
  			$(this).addClass("active").siblings().removeClass("active");
  			var num = $(this).attr("href").replace("#", "");
  			$reportMatchSlider.swipeTo(num);
  		}
  		return false;
  	});

	//переход к фотоальбому матча и видео
	$("#report-match-slider")
		.on("tap", ".video", function(){
			var urlVideo = $(this).attr("href");
	    	var idVideo = $(this).data("idfile");
	    	//YoutubeVideoPlayer.openVideo(idVideo);
			VideoPlayer.play(urlVideo);
			/*
			var urlVideo = $(this).attr("href");
		    $("#videoplayer").html("");
		   	var scriptHTML = '<script type="text/javascript">jwplayer("videoplayer").setup({file: "'+urlVideo+'",width: "100%",autostart: "true"});</script>';
		    $("#scriptvideo").html(scriptHTML);

		    pageInner("#video-inner", "#report-match");
		    */
			return false;
		})
		.on("tap", ".photo", function(){
			var id = $(this).data("id");
			pageInner("#photo-inner", "#report-match");
			photoLoad(id, "");
			return false;
		});


/*******************************
*******СТРАНИЦА ПРОФИЛЯ ИГРОКА*******
*******************************/

	//переход к новости игрока
	$("#profile-news").on("tap", ".news", function(){
		//переход на внутреннюю страницу новости
		pageInner("#news-inner", "#players-inner");
		//получение новости по ID
		var id = $(this).data("id");
		//фукнция загрузки и показа новости по ID
		fullNewsLoad(id);
		return false;
	});

	//открытие видео игрока
	$("#profile-video").on("tap", ".video", function(){
		//window.open($(this).attr("href"), "_system");
		var idVideo = $(this).data("idfile");
		var urlVideo = $(this).attr("href");
	    //YoutubeVideoPlayer.openVideo(idVideo);
	    VideoPlayer.play(urlVideo);
	    /*
	    var urlVideo = $(this).attr("href");
		$("#videoplayer").html("");
		var scriptHTML = '<script type="text/javascript">jwplayer("videoplayer").setup({file: "'+urlVideo+'",width: "100%",autostart: "true"});</script>';
		$("#scriptvideo").html(scriptHTML);

		pageInner("#video-inner", "#players-inner");*/
		return false;
	});

	//клик по ссылке ВСЕ НОВОСТИ ИГРОКА
	$("#all-news-player-link").on("tap", function(){
		$("#news-players").addClass("load");
		pageInner("#news-players", "#players-inner");
		//alert($(this).attr("class"));
		//передаем страницу
		getPlayersNewsAll("");
		return false;
	});
	//переход к просмотру полной новости игрока
	$("#news-list-players").on("tap", ".news", function(){
		//переход на внутреннюю страницу новости
		pageInner("#news-inner", "#news-players");

		//получение новости по ID
		var id = $(this).data("id");
		//фукнция загрузки и показа новости по ID
		fullNewsLoad(id);

		return false;
	});
	//подгрузка новостей игрока при скроле!
	$("#news-players").scroll(function() {
		//playersNewsAllPageLoad = 1,
		//playersNewsAllLoadFlag = 0;
		if($(this).scrollTop() + $(this).height() >= $("#news-list-players").height()-200 && playersNewsAllLoadFlag == 0 && playersNewsAllLoadFlag == 0 && !$(this).find(".noconnect-small").length && !$(this).find(".preloader").length ) {
			getPlayersNewsAll(playersNewsAllPageLoad);
		}
	});

	//клик по ссылке ВСЕ ВИДЕО ИГРОКА
	$("#all-video-player-link").on("tap", function(){
		//alert("asdasd");
		$("#video-players").addClass("load");
		//передаем страницу
		getPlayersVideoAll("");
		pageInner("#video-players", "#players-inner");
		return false;
	});
	//открытие видео игрока
	$("#video-list-players").on("tap", ".video", function(){
		//window.open($(this).attr("href"), "_system");
		var idVideo = $(this).data("idfile");
		var urlVideo = $(this).attr("href");	
	    //YoutubeVideoPlayer.openVideo(idVideo);
	    VideoPlayer.play(urlVideo);
		/*
		var urlVideo = $(this).attr("href");
		$("#videoplayer").html("");
		var scriptHTML = '<script type="text/javascript">jwplayer("videoplayer").setup({file: "'+urlVideo+'",width: "100%",autostart: "true"});</script>';
		$("#scriptvideo").html(scriptHTML);

		pageInner("#video-inner", "#video-players");*/

		return false;
	});
	//подгрузка видео игрока при скроле!
	$("#video-players").scroll(function() {
		if($(this).scrollTop() + $(this).height() >= $("#video-list-players").height()-200 && playersVideoAllLoadFlag == 0 && playersVideoAllLoadFlag == 0 && !$(this).find(".noconnect-small").length && !$(this).find(".preloader").length ) {
			getPlayersVideoAll(playersVideoAllPageLoad);
		}
	});
	//клик по ссылке ВСЕ ФОТО ИГРОКА
	$("#all-photo-player-link").on("tap", function(){
		//alert("asdasd");
		$("#photo-players").addClass("load");
		//передаем страницу
		getPlayersPhotoAll("");
		pageInner("#photo-players", "#players-inner");

		return false;
	});
	//подгрузка фото игрока при скроле!
	$("#photo-players").scroll(function() {
		if($(this).scrollTop() + $(this).height() >= $("#photo-list-players").height()-200 && playersPhotoAllLoadFlag == 0 && playersPhotoAllLoadFlag == 0 && !$(this).find(".noconnect-small").length && !$(this).find(".preloader").length ) {
			getPlayersPhotoAll(playersPhotoAllPageLoad);
		}
	});
	

	/*******************************
	*******СТРАНИЦА РФПЛ*******
	*******************************/

	//открытие всех ссылок в контенте раздела в стандартном браузере
	$("#rfpl").on("tap", "a:not(.submit)", function(){
		var url = $(this).attr("href");
		window.open(url, "_system");

		return false;
	});


	$("#feedback_name, #feedback_email, #feedback_message").on("focus", function(){
		var heightMargin = $("#rfpl").height()/2;

		$(".feedback").append('<div id="margin_form" style="height:'+heightMargin+'px;" class="margin"></div>');
		$("#rfpl").scrollTop($(this).offset().top);
		
	}).on("blur", function(){
		$("#margin_form").remove();
		//alert("blur");
	});

	var regEmail = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,4}$/i;
	$('#feedback_submit').on("click", function(){
		var error = 0;
		var f_name = $("#feedback_name").val();
		var f_email = $("#feedback_email").val();
		var f_text = $("#feedback_message").val();
		if(f_name == ""){
			$("#feedback_name").parent().addClass("error");
			error++;
		}else{
			$("#feedback_name").parent().removeClass("error");
		};

		if (f_email == "" || f_email.search(regEmail) == -1 || f_email.length > 40){
			$("#feedback_email").parent().addClass("error");
			error++;
		}else{
			$("#feedback_email").parent().removeClass("error");
		};

		if(f_text == ""){
			$("#feedback_message").parent().addClass("error");
			error++;
		}else{
			$("#feedback_message").parent().removeClass("error");
		};
		if(error==0){
			$(".feedback").addClass("load");
			$(".feedback .form").hide();
			$("#feedback_name").val("");
			$("#feedback_email").val("");
			$("#feedback_message").val("");
			$.ajax({
				url:'http://rfpl.org/netcat/add.php',
				type: 'POST',
				data: 'catalogue=1&cc=204&sub=242&posting=1&curPos=0&f_Checked=1&f_name='+f_name+'&f_email='+f_email+'&f_text='+f_text,
				success: function(res){
					$(".feedback").removeClass("load");
					$(".feedback .success").show();
				}
			});
		};

		return false;
	});

	/*******************************
	******* ЛИГА СТАВОК *******
	*******************************/

	//слайдер лидеров
	//setTimeout(function(){

	$lsSlider = new Swiper('#ls-slider', {

	   	slidesPerView: 1,
	    createPagination: false,
	    loop: false,
	    calculateHeight: false,
	    autoplay: false,
	   	speed: 300,
	   	touchRatio: 1,
	   	simulateTouch: false,
	   	touchMoveStopPropagation: false,
	   	resistance: true,
	   	resistanceRatio: 0,
	    onSwiperCreated: function(slider) {
	    	var heightContent = $("#ls").height();
	    	//$("#ls-header .liga-stavok").html(heightContent);
	    	//alert(heightContent);
		    $("#ls .swiper-wrapper, #ls .swiper-slide").css("height", heightContent);
		    //$("#ls-header .liga-stavok").html(heightContent+" | "+$("#ls .swiper-wrapper").height()+" | "+$("#ls .swiper-slide").height());
		    
	    },
	    onInit: function(slider) {
	    	var heightContent = $("#ls").height();
	    	//$("#ls-header .liga-stavok").html(heightContent);
		    $("#ls .swiper-wrapper, #ls .swiper-slide").css("height", heightContent);
		    //$("#ls-header .liga-stavok").html(heightContent+" | "+$("#ls .swiper-wrapper").height()+" | "+$("#ls .swiper-slide").height());
		    
	    },
	    onSlideChangeStart: function(slider){
	    	$("#ls-tabs a").removeClass("active");
	    	$("#ls-tabs a[href=#"+slider.activeIndex+"]").addClass("active"); 

	    	$("#ls-subtitle").html($("#ls-tabs a[href=#"+slider.activeIndex+"]").data("title"));
	    },
	    onSlideChangeEnd: function(slider){
	    	//подгружаем вкладки клуба в зависимости от типа и если в них не было загрузки
	    	$("#noconnect").hide();

	    	var type = slider.activeIndex;
	    	if (type == 1 && !$("#ls-video .video").length) {
	    		getLSVideo("");
	    	}
	    	if (type == 2 && !$("#ls-news .news").length) {
	    		getLSNews("");
	    	}
	    	if (type == 3 && !$("#ls-official h1").length) {
	    		getLSofficial();
	    	}

	    }
	});

  	$("#ls-tabs a").on("tap", function(){
  		if (!$(this).hasClass("active")) {
  			var num = $(this).attr("href").replace("#", "");
  			$("#ls-tabs a").removeClass("active");
	    	$("#ls-tabs a[href=#"+num+"]").addClass("active"); 
  			$lsSlider.swipeTo(num);
  			if (num == 3) {
	    		getLSofficial();
	    		$("#ls-subtitle").html($("#ls-tabs a.active").data("title"));
	    	}
  		}
  		return false;
  	});	

	//}, 100);
	
	

	//загрузка новостей и показ видео
	$("#ls")
		.on("tap", ".news", function(){
			var id = $(this).data("id");
			//фукнция загрузки и показа новости по ID
			pageInner("#news-inner", "#ls");
			$("#ls-news-scroll").data("scroll", $("#ls-news-scroll").scrollTop());
			fullNewsLoad(id);
			return false;
		}).on("tap", ".video", function(){
			//открытие видео в стандартном браузере
			//window.open($(this).attr("href"), "_system");
			var idVideo = $(this).data("idfile");
			var urlVideo = $(this).attr("href");
		    //YoutubeVideoPlayer.openVideo(idVideo);
		    VideoPlayer.play(urlVideo);
			

			return false;
		});
	
	$("#ls-video-scroll").scroll(function() {
		if ($(this).scrollTop() + $(this).height() >= $("#ls-video").height()-100 &&  !$("#ls-video").find(".preloader").length && lsVideoPageLoadFlag == 0 ) {
			getLSVideo(lsVideoPageLoad);
		}
	});
	$("#ls-news-scroll").scroll(function() {
		if ($(this).scrollTop() + $(this).height() >= $("#ls-news").height()-100 &&  !$("#ls-news").find(".preloader").length && lsNewsPageLoadFlag == 0 ) {
			getLSNews(lsNewsPageLoad);
		}
	});
	
	/*
	$("#news").scroll(function() {
		if ($(this).scrollTop() + $(this).height() >= $("#news-list").height()-100 && newsLoadFlag == 0 ) {
			getNews(newsPageLoad);
		}
	});
	*/

	//alert("1");
	//получение линии
	function parseLSline(res) {
		
		if (res != "noconnect") {
			//setTimeout(function(){
			$("#ls-line").html(res.rate).removeClass("load");
			$lsSlider.reInit();
			//lsLineScroll.refresh();

			//добавляем заголовки в шапку и вкладки
			$("#ls-header p").html(res.text.header);
			
			$("#ls-header a[href=#0]").data("title", res.text.tab1);
			$("#ls-header a[href=#1]").data("title", res.text.tab2);
			$("#ls-header a[href=#2]").data("title", res.text.tab3);
			$("#ls-header a[href=#3]").data("title", res.text.tab4);

			$("#ls-subtitle").html($("#ls-tabs a.active").data("title"));

			//}, 1000);
		} else {
			//если интернета нет
			$("#noconnect").show();
			$("#refresh-repeat").data("action", "lsline");
		}
		
	}
	function getLSline() {
		//добавляем прелоадер
		$("#ls-line").addClass("load").html("");
		getDataJSON('temp.json', 'ls.php', {"action":"getRate"}, parseLSline, 1);
		//parseLSline(res);
	}

	//для видео лиги ставок переменная страницы загрузки и флаг загрузки!
	var lsVideoPageLoad = 1,
		lsVideoPageLoadFlag = 0;
	//фукцнии разбора видео лиги ставок при загрузке и подгрузке
	function parseLSVideo(res) {

		if (res != "noconnect") {
			var videoHTML = '';
			if (res.video != null) {
				
				$.each(res.video, function(key,val){
					videoHTML += '<a class="video" href="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" target="_blank"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.time+'<i></i> </span></span><span class="bg"></span></a>';
				});
				//setTimeout(function(){
					$("#ls-video").html(videoHTML);
					$("#ls-video").removeClass("load");
					//lsVideoScroll.refresh();
				//}, 1000);
				
			} else {

			}

		} else {
			//если интернета нет
			$("#noconnect").show();
			$("#refresh-repeat").data("action", "lsvideo");
		}	
					
	}
	function parseLSScrollVideo(res) {

		if (res != "noconnect") {

			var videoHTML = '';
			if (res.video != null) {

				$.each(res.video, function(key,val){
					videoHTML += '<a class="video" href="'+val.file+'" data-idfile="'+val.idfile+'" data-id="'+val.id+'" target="_blank"><img src="'+val.bigimg+'"><span class="desc"><span class="name">'+val.name+'</span><span class="time">'+val.time+'<i></i> </span></span><span class="bg"></span></a>';
				});
				//videoHTML = '<a class="video" href="#" data-idfile="#" data-id="id" target="_blank"><img src="http://rfpl.org/netcat_files/multifile/2329/preview_332288_1700x1100.jpg"><span class="desc"><span class="name">P='+lsVideoPageLoad+' Тестовое видео</span><span class="time">02:34<i></i> </span></span><span class="bg"></span></a>';
				//setTimeout(function(){
					$("#ls-video").find(".preloader").hide("fade", 250, function(){ $(this).remove(); });
					$("#ls-video").append(videoHTML);
					lsVideoPageLoad++;
					lsVideoPageLoadFlag = 0;
					//lsVideoScroll.refresh();
				//}, 1000);
				
			} else {	
				setTimeout(function(){
					$("#ls-video").find(".preloader").hide();//.remove();
					lsVideoPageLoadFlag = 0;
					//clubVideoScroll.refresh();
				}, 250);
			}

		} else {
			//если интернета нет
			//удаляем прелоадер
			$("#ls-video").find(".preloader").remove();
			//добавляем сообщение об отсутствии интернета
			$("#ls-video").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="lsvideoscroll" href="#"><i></i> Повторить попытку</a></div>');		
			//lsVideoScroll.refresh();
		}
	}
	function getLSVideo(page) {
		//http://rfpl.org/api/video.php?action=getVideoClubs&club=1&page=0		
		if (page != "") { 
			lsVideoPageLoadFlag = 1;
			$("#ls-video").find(".noconnect-small").remove();
			$("#ls-video").append('<div class="preloader"></div>');
			//lsVideoScroll.refresh();
			getDataJSON('temp.json', 'video.php', {"action":"getVideoLS", "page" : page}, parseLSScrollVideo, 1);
			//parseLSScrollVideo();
		} else {
			//обнуляем страницу загрузки
			lsVideoPageLoad = 1;
			getDataJSON('temp.json', 'video.php', {"action":"getVideoLS"}, parseLSVideo, 1);
			//parseLSVideo();
		}	
	}

	//для новостей лиги ставок переменная страницы загрузки и флаг загрузки!
	var lsNewsPageLoad = 1,
		lsNewsPageLoadFlag = 0;
	//фукцнии разбора видео лиги ставок при загрузке и подгрузке
	function parseLSNews(res) {

		if (res != "noconnect") {
			var newsHTML = '';
			$.each(res.news, function(key, val){
				var descHtml = "";
				if (val.short_s != "") {
					descHtml = '<p class="desc">'+val.short_s+'</p>';
				}
				var rfplHtml = "";
				if (val.type == "18") {
					rfplHtml = '<span class="rfpl">РФПЛ</span>';
				}
				newsHTML += '<a href="#" class="news" data-id="'+val.id+'"><figure><img src="'+val.img+'"></figure><p><span class="date">'+val.date+'</span>'+rfplHtml+'</p><p class="name">'+val.name+'</p>'+descHtml+'</a>';

			});
			$("#ls-news").html(newsHTML);
			$("#ls-news").removeClass("load");
			//if (res.video != null) {
				/*
				newsHTML = '<a href="#" class="news" data-id="16434"><figure><img src="http://rfpl.org/netcat_files/236/198/30tur.png"></figure><p><span class="date">28.05.2015</span></p><p class="name">ИНТРИГУЮЩИЙ ФИНАЛ!</p><p class="desc">Разбор тридцатого тура РФПЛ от главного аналитика БК «Лига Ставок»</p></a>';
				setTimeout(function(){
					$("#ls-news").html(newsHTML+newsHTML+newsHTML+newsHTML+newsHTML+newsHTML+newsHTML+newsHTML+newsHTML);
					$("#ls-news").removeClass("load");
					//lsNewsScroll.refresh();
				}, 1000);
				*/
				
			//} else {

			//}

		} else {
			//если интернета нет
			$("#noconnect").show();
			//$("#club-video").removeClass("load");
			$("#refresh-repeat").data("action", "lsnews");
		}	
					
	}
	function parseLSScrollNews(res) {

		//если есть подключение к интернету
		if (res != "noconnect") {
			var newsHTML = '';
			//подгрузка новостей при скроле!
			//первоначальная загрузка полсдених новостей в local storage	
			if (res.news != null) {
				$.each(res.news, function(key, val){
							
					var descHtml = "";
					if (val.short_s != "") {
						descHtml = '<p class="desc">'+val.short_s+'</p>';
					}
					var rfplHtml = "";
					if (val.type == "18") {
						rfplHtml = '<span class="rfpl">РФПЛ</span>';
					}
					newsHTML += '<a href="#" class="news" data-id="'+val.id+'"><figure><img src="'+val.img+'"></figure><p><span class="date">'+val.date+'</span>'+rfplHtml+'</p><p class="name">'+val.name+'</p>'+descHtml+'</a>';

							
				});
				$("#ls-news").find(".preloader").hide("fade", 250, function(){
					$(this).remove();
				});
				$("#ls-news").append(newsHTML);
				lsNewsPageLoad++;
				lsNewsPageLoadFlag = 0;
			} else {
				setTimeout(function(){
					$("#ls-news").find(".preloader").hide();
					lsNewsPageLoadFlag = 0;
				}, 250);
			}
			

		} else {
			//если интернета нет
			//удаляем прелоадер
			$("#ls-news").find(".preloader").remove();
			//добавляем сообщение об отсутствии интернета
			$("#ls-news").append('<div class="noconnect-small"><p class="text">Подключение к интернету отсутствует</p><a class="refresh-repeat" data-action="lsnewsscroll" href="#"><i></i> Повторить попытку</a></div>');		
			//lsNewsScroll.refresh();		

		}

	}
	function getLSNews(page) {
		//http://rfpl.org/api/video.php?action=getVideoClubs&club=1&page=0		
		if (page != "") { 
			lsNewsPageLoadFlag = 1;
			$("#ls-news").find(".noconnect-small").remove();
			$("#ls-news").append('<div class="preloader"></div>');

			//news.php?action=getNewsLS
			getDataJSON('temp.json', 'news.php', {"action":"getNewsLS", "page" : page}, parseLSScrollNews, 1);
			//parseLSScrollNews();
		} else {
			//обнуляем страницу загрузки
			lsNewsPageLoad = 1;
			getDataJSON('temp.json', 'news.php', {"action":"getNewsLS"}, parseLSNews, 1);
		}	
	}


	//получение линии
	function parseLSofficial(res) {
		
		if (res != "noconnect") {
			//setTimeout(function(){
				$("#ls-official").html(res.text).removeClass("load");
				$lsSlider.reInit();
				//lsOfficialScroll.refresh();
			//}, 1000);
		} else {
			//если интернета нет
			$("#noconnect").show();
			$("#refresh-repeat").data("action", "lsofficial");
		}
		
	}
	function getLSofficial() {
		//добавляем прелоадер
		$("#ls-official").addClass("load").html("");
		//text.php?action=LigaStavok
		getDataJSON('temp.json', 'text.php', {"action":"LigaStavok"}, parseLSofficial, 1);
		//var res = '<h1>БК «Лига Ставок»</h1><p>Российская букмекерская компания, оказывающая услуги на основании лицензии № 6 ФНС РФ, выданной 8 июля 2009 года.</p><p>Деятельность компании направлена на развитие букмекерского бизнеса в России, поддержку отечественного спорта и создание новой сферы развлечений. </p><p>Такая стратегия работы позволила компании стать лидером на рынке букмекерских услуг в России. Сегодня БК «Лига Ставок» - это 500 клубов более чем в 120 городах Российской Федерации.</p><p>С 2010 года БК «Лига Ставок» и Российская футбольная Премьер-Лига сотрудничают по различным направлениям в области развития и популяризации (пропаганды) футбола в России.</p><p class="big">В спортивных сезонах 2014-2015 гг. и 2015-2016 гг. <br> <b>БК «Лига Ставок» - официальная букмекерская компания РФПЛ.</b></p><div class="img"><a href="http://www.ligastavok.ru/" class="link">www.ligastavok.ru</a><img src="./img/ls-img.jpg"></div>';
		//parseLSofficial(res);
	}

	//открытие всех ссылок в контенте новости в стандартном браузере
	$("#ls-official").on("tap", "a", function(){
		var url = $(this).attr("href");
		window.open(url, "_system");

		return false;
	});

	
	/*******************************
	******* END ЛИГА СТАВОК *******
	*******************************/
	
	
	$("#wrapper").on("click",".banner-ea", function(){
		//alert($(this).data("url"));
		var url = $(this).data("url");
		window.open(url, "_system");
		return false;
	});


	$("body").bind("orientationchange", function(event) {
		return false;
	});

	//обработка события кнопки назад
	document.addEventListener("backbutton", backKeyDown, true); 

	function backKeyDown() { 

		//если меню открыто - тогда скрываем меню
		if ( $("#main-nav").hasClass("open") ) {
			viewMainNav("hide");
		} else {
			//если были переходы
		    if (backArrayGlobal.length > 0) {
		    	var $fromPage = $("#pages .current");
		    	var toPage = backArrayGlobal[backArrayGlobal.length-1];
				var $toPage = $("#"+toPage);

		    	//if ($("#back:visible").length) {
		    	var $checkLink = $("#main-nav a."+toPage);
		    	if ($checkLink.length) {
		    		$("#main-nav a").removeClass("active");
		    		$checkLink.addClass("active");
		    	}

		    	animateInner("back", $toPage, $fromPage);
		    	//}

		    } else {
		    	navigator.app.exitApp();
		    }
		}

		
	}

	//обработка события кнопки назад
	/*
	document.addEventListener("menubutton", onMenuKeyDown, false);

	function onMenuKeyDown() {
	  	if (!$("#back:visible").length && !$("#main-nav").hasClass("open")) {
			viewMainNav("show");
		} else {
			viewMainNav("hide");
		}
	}
	*/


	function checkConnection() {
	    var networkState = navigator.connection.type;

	    var states = {};
	    states[Connection.UNKNOWN]  = "UNKNOWN";
	    states[Connection.ETHERNET] = "Ethernet connection";
	    states[Connection.WIFI]     = "WiFi connection";
	    states[Connection.CELL_2G]  = "Cell 2G connection";
	    states[Connection.CELL_3G]  = "Cell 3G connection";
	    states[Connection.CELL_4G]  = "Cell 4G connection";
	    states[Connection.CELL]     = "Cell generic connection";
	    states[Connection.NONE]     = "NONE";
	    //alert('Connection type: ' + states[networkState]);
	    //alert(states[networkState]);
	    if (states[networkState] == "NONE" || states[networkState] == "UNKNOWN") {
	    	return false;
	    } else {
	    	return true;
	    }
	    
	    //return true;
	}

	checkConnection();




}, false);


function onDeviceReady() {  
	//alert("onDeviceReady");
    
}








$(document).ready(function() {




	
});
